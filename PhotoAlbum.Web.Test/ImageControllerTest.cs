﻿using System;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Mvc;
using Moq;
using PhotoAlbum.Controllers;
using PhotoAlbum.Services.Interfaces;
using Xunit;

namespace PhotoAlbum.Web.Test
{
    /// <summary>
    ///     Contains unit test for the <see cref="ImageController"/> class.
    /// </summary>
    public static class ImageControllerTest 
    {
        [Fact]
        internal static async Task GetImageAsync_SuccessfullyCompletedAsync()
        {
            var mockFileStorage = new Mock<IFileStorage>(); 
            var mockImageAccessingAuthorizationService = new Mock<IImageAccessAuthorizationService>();
            var imageController = new ImageController(
                mockFileStorage.Object,
                mockImageAccessingAuthorizationService.Object);
            var imageId = Guid.NewGuid();

            mockImageAccessingAuthorizationService.Setup(
                service => service.CheckPermissions(imageId, It.IsAny<IPrincipal>()));
            mockFileStorage.Setup(
                fileStorage => fileStorage.GetFileStringAsync(imageId))
                .ReturnsAsync(Guid.NewGuid().ToByteArray);

            var actualResult = await imageController.GetImage(imageId);

            mockImageAccessingAuthorizationService.Verify( 
                service => service.CheckPermissions(imageId, It.IsAny<IPrincipal>()),
                Times.Once());
            mockFileStorage.Verify(
                fileStorage => fileStorage.GetFileStringAsync(imageId),
                Times.Once);

            Assert.IsType<FileContentResult>(actualResult);
        }
    }
} 