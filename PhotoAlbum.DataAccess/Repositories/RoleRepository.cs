﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PhotoAlbum.DataAccess.Interfaces;

namespace PhotoAlbum.DataAccess.Repositories
{
    /// <summary>
    ///     Represents role repository.
    /// </summary>
    public sealed class RoleRepository : RoleManager<IdentityRole>, IRoleRepository
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RoleRepository" /> class.
        /// </summary>
        /// <param name="store">Role store.</param>
        public RoleRepository(IRoleStore<IdentityRole, string> store) : base(store)
        {
        }

        /// <inheritdoc />
        public Task<IEnumerable<IdentityRole>> GetRolesAsync()
        {
            return Task.FromResult<IEnumerable<IdentityRole>>(Roles.ToList());
        }
    }
}