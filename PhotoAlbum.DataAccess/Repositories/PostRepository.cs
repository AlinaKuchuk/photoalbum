﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.Enums;
using PhotoAlbum.DataAccess.Models.QueryParameters;

namespace PhotoAlbum.DataAccess.Repositories
{
    /// <summary>
    ///     Represents post repository.
    /// </summary>
    public sealed class PostRepository : GenericRepository<Post, int>, IPostRepository
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PostRepository" /> class.
        /// </summary>
        /// <param name="photoAlbumContext">Photo album context.</param>
        public PostRepository(IPhotoAlbumContext photoAlbumContext) : base(photoAlbumContext)
        {
        }

        /// <inheritdoc />
        public async Task<FullPostAggregatedModel> GetFullPostAsync(
            int postId,
            string requestingUserId,
            DbPaginationQueryParameters commentsDbPaginationQueryParameters,
            string[] requestingUserRoles)
        {
            var isRequestingUserAdministrator = requestingUserRoles.Contains(nameof(Roles.Administrator));

            var amountToSkip = commentsDbPaginationQueryParameters.PageSize *
                               commentsDbPaginationQueryParameters.PageNumber;
            var selectionResult = await PhotoAlbumContext.Set<Post>()
                .Where(post =>
                    !post.IsDeleted
                    && post.Id == postId
                    && (post.Album.OwnerId == requestingUserId ||
                        isRequestingUserAdministrator
                        || (!post.Album.IsPrivate
                        && !post.Album.Owner.IsPrivate)))
                .Select(post =>
                    new
                    {
                        Post = post,
                        post.Album,
                        AlbumOwner = post.Album.Owner,
                        post.Photos,
                        LikesCount = post.Likes.Count(like => !like.IsDeleted),
                        IsLiked = post.Likes.Any(like =>
                            like.UserId == requestingUserId && !like.IsDeleted && like.PostId == post.Id),
                        TotalComments = post.Comments.Count(),
                        Comments = post.Comments
                            .OrderByDescending(c => c.CreatedDateTime)
                            .Skip(amountToSkip)
                            .Take(commentsDbPaginationQueryParameters.PageSize)
                            .Select(comment => new
                            {
                                Comment = comment,
                                comment.Author
                            })
                    })
                .FirstOrDefaultAsync();

            if (selectionResult == null)
            {
                return null;
            }

            selectionResult.Post.Views++;
            PhotoAlbumContext.Entry(selectionResult.Post).State = EntityState.Modified;
            await PhotoAlbumContext.SaveChangesAsync();

            var aggregatedPost = new FullPostAggregatedModel
            {
                Post = selectionResult.Post,
                Comments = new DbPaginationData<Comment>
                {
                    Items = selectionResult.Comments.Select(c => c.Comment),
                    TotalPages =
                        (int) Math.Ceiling((double) selectionResult.TotalComments /
                                           commentsDbPaginationQueryParameters.PageSize),
                    CurrentPage = commentsDbPaginationQueryParameters.PageNumber
                },
                LikesCount = selectionResult.LikesCount
            };

            aggregatedPost.Post.Album = selectionResult.Album;
            aggregatedPost.Post.Photos = selectionResult.Photos;
            aggregatedPost.Post.Album.Owner = selectionResult.AlbumOwner;
            aggregatedPost.IsLiked = selectionResult.IsLiked;

            return aggregatedPost;
        }

        /// <inheritdoc />
        public async Task<DbPaginationData<PreviewPostAggregatedModel>> GetFollowingUpdatesAsync(
            string searchText,
            string requestingUserId,
            DbPaginationQueryParameters paginationQueryParameters,
            string sortingField,
            SortOrder sortingOrder)
        {
            var query = PhotoAlbumContext.Set<User>()
                .Where(user => user.Id == requestingUserId)
                .SelectMany(user => user.Following)
                .SelectMany(user => user.Albums)
                .Where(album => !album.IsPrivate && !album.IsDeleted)
                .SelectMany(album => album.Posts)
                .Select(post => new
                {
                    Post = post,
                    post.CreatedDateTime,
                    Author = post.Album.Owner,
                    post.Album,
                    post.Photos,
                    IsLiked = post.Likes.Any(like =>
                        like.UserId == requestingUserId && !like.IsDeleted && like.PostId == post.Id),
                    LikesCount = post.Likes.Count(like => !like.IsDeleted),
                    Comments = post.Comments.OrderByDescending(comment => comment.CreatedDateTime)
                        .Take(2)
                        .Select(comment => new
                        {
                            Comment = comment,
                            comment.Author
                        }) // Just assumption select only last two comments
                });

            query = (searchText == null) ? query
                : query.Where(post =>
                    post.Post.Description.Contains(searchText)
                    || post.Album.Title.Contains(searchText)
                    || post.Post.Location.Contains(searchText));

            return new DbPaginationData<PreviewPostAggregatedModel>
            {
                Items =
                    await (sortingOrder == SortOrder.Asc
                            ? query.OrderBy(post => post.Post.CreatedDateTime)
                            : query.OrderByDescending(post => post.Post.CreatedDateTime))
                        .Select(post => new PreviewPostAggregatedModel
                        {
                            Post = post.Post,
                            Album = post.Album,
                            Author = post.Author,
                            Photos = post.Photos,
                            LikesCount = post.LikesCount,
                            Comments = post.Comments.Select(comment => comment.Comment),
                            IsLiked = post.IsLiked
                        })
                        .Skip(paginationQueryParameters.PageSize * paginationQueryParameters.PageNumber)
                        .Take(paginationQueryParameters.PageSize)
                        .ToListAsync(),
                CurrentPage = paginationQueryParameters.PageNumber,
                TotalPages = (int)Math.Ceiling((double)
                    query.Count() / paginationQueryParameters.PageSize)
            };
        }

        /// <inheritdoc />
        public async Task<PostsStatistic> GetPostsStatisticAsync(string requestingUserId)
        {
            var selectionResult = await PhotoAlbumContext.Set<User>()
                .Where(user => user.Id == requestingUserId)
                .Select(user => new
                {
                    ViewsCount = user.Albums.SelectMany(album => album.Posts).Sum(post => post.Views),
                    LikesCount = user.Albums.SelectMany(album => album.Posts).Sum(post => post.Likes.Count)
                })
                .FirstOrDefaultAsync();

            if (selectionResult == null)
            {
                return null;
            }

            return new PostsStatistic
            {
                ViewsCount = selectionResult.ViewsCount,
                LikesCount = selectionResult.LikesCount
            };
        }

        /// <inheritdoc />
        public Task<DbPaginationData<PreviewPostAggregatedModel>> GetExplorePostsAsync(
            string searchText,
            string requestingUserId,
            DbPaginationQueryParameters paginationQueryParameters,
            string sortField,
            SortOrder sortingOrder)
        {
            var query = PhotoAlbumContext.Set<User>()
                .Where(user => !user.IsPrivate)
                .SelectMany(user => user.Albums)
                .Where(album => !album.IsPrivate && !album.IsDeleted)
                .SelectMany(album => album.Posts);

            query = (searchText == null) ? query 
                : query.Where(post => 
                    post.Description.Contains(searchText)
                    || post.Album.Title.Contains(searchText)
                    || post.Location.Contains(searchText));

            query = sortingOrder == SortOrder.Asc
                ? query.OrderBy(d => d.CreatedDateTime)
                : query.OrderByDescending(d => d.CreatedDateTime);

            var selectionResult = query
                .Select(post => new
                {
                    Post = post,
                    post.CreatedDateTime,
                    Author = post.Album.Owner,
                    post.Album,
                    post.Photos,
                    IsLiked = post.Likes.Any(like =>
                        like.UserId == requestingUserId && !like.IsDeleted && like.PostId == post.Id),
                    LikesCount = post.Likes.Count(like => !like.IsDeleted),
                    Comments = post.Comments.OrderByDescending(comment => comment.CreatedDateTime)
                        .Take(2)
                        .Select(comment => new
                        {
                            Comment = comment,
                            comment.Author
                        }) // Just assumption select only last two comments
                });

            return Task.FromResult(new DbPaginationData<PreviewPostAggregatedModel>
            {
                Items = selectionResult
                    .Select(post => new PreviewPostAggregatedModel
                    {
                        Post = post.Post,
                        Album = post.Album,
                        Author = post.Author,
                        Photos = post.Photos,
                        LikesCount = post.LikesCount,
                        Comments = post.Comments.Select(comment => comment.Comment),
                        IsLiked = post.IsLiked
                    })
                    .Skip(paginationQueryParameters.PageSize * paginationQueryParameters.PageNumber)
                    .Take(paginationQueryParameters.PageSize)
                    .ToList(),
                CurrentPage = paginationQueryParameters.PageNumber,
                TotalPages = (int)Math.Ceiling((double) 
                    selectionResult.Count() / paginationQueryParameters.PageSize)
            });
        }
    }
}