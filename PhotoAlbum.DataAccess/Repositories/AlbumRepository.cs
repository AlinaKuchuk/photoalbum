﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.Enums;
using PhotoAlbum.DataAccess.Models.QueryParameters;

namespace PhotoAlbum.DataAccess.Repositories
{
    /// <summary>
    ///     Represents album repository.
    /// </summary>
    public sealed class AlbumRepository : GenericRepository<Album, int>, IAlbumRepository
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AlbumRepository" /> class.
        /// </summary>
        /// <param name="photoAlbumContext">Photo album context.</param>
        public AlbumRepository(IPhotoAlbumContext photoAlbumContext) : base(photoAlbumContext)
        {
        }

        /// <inheritdoc />
        public async Task<UserAlbumsAggregatedModel> GetUserAlbumsAsync(
            DbGetUserAlbumsQueryParameters queryParameters)
        {
            var amountToSkip = queryParameters.AlbumsPaginationQueryParameters.PageSize *
                               queryParameters.AlbumsPaginationQueryParameters.PageNumber;
            var selectionResult = PhotoAlbumContext.Set<Album>()
                .Where(album => !album.IsDeleted && queryParameters.UserId == album.OwnerId &&
                                (album.OwnerId == queryParameters.RequestingUserId ||
                                 queryParameters.RequestingUserRoles.Any(role => role == nameof(Roles.Administrator)) ||
                                 !album.IsPrivate))
                .OrderByDescending(album => album.CreatedDateTime)
                .Skip(amountToSkip)
                .Take(queryParameters.AlbumsPaginationQueryParameters.PageSize)
                .Select(album =>
                    new
                    {
                        Album = album,
                        AlbumOwner = album.Owner
                    });

            var aggregatedModel = new UserAlbumsAggregatedModel
            {
                Owner = await selectionResult
                    .Select(album => album.AlbumOwner)
                    .FirstOrDefaultAsync(),
                Albums = new DbPaginationData<PreviewAlbumAggregatedModel>
                {
                    Items = selectionResult
                        .Select(album => new PreviewAlbumAggregatedModel
                        {
                            Album = album.Album,
                            AlbumPreviewPhotoId = album.Album.Posts.SelectMany(post => post.Photos)
                                .OrderByDescending(photo => photo.CreatedDateTime)
                                .FirstOrDefault()
                                .FileId
                        }),
                    TotalPages =
                        (int) Math.Ceiling((double) selectionResult.Count() /
                                           queryParameters.AlbumsPaginationQueryParameters.PageSize),
                    CurrentPage = queryParameters.AlbumsPaginationQueryParameters.PageNumber
                }
            };

            return aggregatedModel;
        }

        /// <inheritdoc />
        public async Task<FullAlbumAggregatedModel> GetAlbumPhotosAsync(DbGetAlbumPhotosQueryParameters queryParameters)
        {
            var amountPhotoToSkip = queryParameters.PhotosPaginationQueryParameters.PageSize *
                                    queryParameters.PhotosPaginationQueryParameters.PageNumber;

            var albumPhotos = await PhotoAlbumContext.Set<Album>()
                .Where(album => !album.IsDeleted && queryParameters.AlbumId == album.Id)
                .Select(album =>
                    new
                    {
                        Album = album,
                        album.Owner,
                        Photos = album.Posts
                            .SelectMany(a => a.Photos)
                            .OrderByDescending(photo => photo.CreatedDateTime)
                            .Skip(amountPhotoToSkip)
                            .Take(queryParameters.PhotosPaginationQueryParameters.PageSize),
                        PhotosCount = album.Posts
                            .SelectMany(post => post.Photos)
                            .Count(),
                        PostCount = album.Posts
                            .Count()
                    })
                .FirstOrDefaultAsync();

            if (albumPhotos == null)
            {
                return null;
            }

            return new FullAlbumAggregatedModel
            {
                Album = albumPhotos.Album,
                Photos = new DbPaginationData<PhotoMetadata>
                {
                    Items = albumPhotos.Photos,
                    CurrentPage = queryParameters.PhotosPaginationQueryParameters.PageNumber,
                    TotalPages =
                        (int) Math.Ceiling((double) albumPhotos.PhotosCount /
                                           queryParameters.PhotosPaginationQueryParameters.PageSize)
                },
                PhotosCount = albumPhotos.PhotosCount,
                PostCount = albumPhotos.PostCount
            };
        }
    }
}