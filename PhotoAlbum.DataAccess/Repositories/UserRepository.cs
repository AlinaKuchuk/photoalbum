﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.Enums;
using PhotoAlbum.DataAccess.Models.QueryParameters;

namespace PhotoAlbum.DataAccess.Repositories
{
    /// <summary>
    ///     Represents user repository.
    /// </summary>
    public sealed class UserRepository : UserManager<User>, IUserRepository
    {
        private readonly IPhotoAlbumContext _photoAlbumContext;

        /// <summary>
        ///     Initializes a new instance of the <see cref="UserRepository" /> class.
        /// </summary>
        /// <param name="store">User store.</param>
        /// <param name="photoAlbumContext">Photo album context.</param>
        public UserRepository(IUserStore<User> store, IPhotoAlbumContext photoAlbumContext)
            : base(store)
        {
            _photoAlbumContext = photoAlbumContext;
        }

        /// <inheritdoc />
        public Task<IEnumerable<UserWithRolesAggregatedModel>> GetFilteredAsync(
            Expression<Func<User, bool>> filterExpression)
        {
            var selectionResult = _photoAlbumContext.Set<User>()
                .Where(filterExpression)
                .Select(user => new
                {
                    User = user,
                    Roles = _photoAlbumContext.Roles
                        .Where(role =>
                            user.Roles.Any(userRole => userRole.RoleId == role.Id))
                })
                .ToList();

            return Task.FromResult(
                selectionResult
                    .Select(selectionItem =>
                        new UserWithRolesAggregatedModel
                        {
                            User = selectionItem.User,
                            Roles = selectionItem.Roles
                        }));
        }

        /// <inheritdoc cref="UserManager{TUser}" />
        public override async Task<ClaimsIdentity> CreateIdentityAsync(User user, string authenticationType)
        {
            var identity = await base.CreateIdentityAsync(user, authenticationType);

            identity.AddClaim(new Claim(ClaimNamesConstants.FullNameClaimName, $"{user.FirstName} {user.LastName}"));
            identity.AddClaim(new Claim(ClaimNamesConstants.AvatarClaimName, user.ProfilePhotoId.ToString()));
            identity.AddClaim(new Claim(ClaimNamesConstants.IsPrivateClaimName, user.IsPrivate.ToString()));

            return identity;
        }

        /// <inheritdoc />
        public async Task<FullUserAggregatedModel> GetFullUserAsync(DbGetFullUserQueryParameters queryParameters)
        {
            var amountPostsToSkip = queryParameters.PhotosPaginationQueryParameters.PageSize *
                                    queryParameters.PhotosPaginationQueryParameters.PageNumber;

            var detailedUser = await Users
                .Where(user => user.Id == queryParameters.UserId)
                .Select(user => new
                {
                    User = user,
                    IsFollowed = user.Followers.Any(follower => follower.Id == queryParameters.RequestingUserId),
                    FollowersCount = user.Followers.Count,
                    FollowingCount = user.Following.Count,
                    Albums = user.Albums.Select(
                            album => new PreviewAlbumAggregatedModel
                            {
                                Album = album,
                                AlbumPreviewPhotoId = album.Posts.SelectMany(post => post.Photos)
                                    .OrderByDescending(photo => photo.CreatedDateTime)
                                    .FirstOrDefault()
                                    .FileId
                            })
                        .Where(album => !album.Album.IsDeleted && (user.Id == queryParameters.RequestingUserId ||
                                                             queryParameters.RequestingUserRoles.Any(role =>
                                                                 role == nameof(Roles.Administrator)) ||
                                                             !album.Album.IsPrivate))
                        .OrderByDescending(album => album.Album.CreatedDateTime)
                        .Take(queryParameters.MaximumAlbumsCount),
                    Photos = user
                        .Albums
                        .Where(album =>
                            !album.IsDeleted && user.Id == queryParameters.RequestingUserId || !album.IsPrivate)
                        .SelectMany(album => album.Posts)
                        .SelectMany(post => post.Photos)
                        .OrderBy(post => post.CreatedDateTime)
                        .Skip(amountPostsToSkip)
                        .Take(queryParameters.PhotosPaginationQueryParameters.PageSize),
                    PhotosCount = user
                        .Albums
                        .Where(album => !album.IsPrivate && !album.IsDeleted)
                        .SelectMany(album => album.Posts)
                        .SelectMany(post => post.Photos)
                        .Count(),
                    PostCounts = user.Albums.SelectMany(album => album.Posts).Count()
                })
                .FirstOrDefaultAsync();

            if (detailedUser == null)
            {
                return null;
            }

            return new FullUserAggregatedModel
            {
                User = detailedUser.User,
                IsFollowed = detailedUser.IsFollowed,
                FollowersCount = detailedUser.FollowersCount,
                FollowingCount = detailedUser.FollowingCount,
                Photos = new DbPaginationData<PhotoMetadata>
                {
                    Items = detailedUser.Photos,
                    CurrentPage = queryParameters.PhotosPaginationQueryParameters.PageNumber,
                    TotalPages =
                        (int) Math.Ceiling((double) detailedUser.PhotosCount /
                                           queryParameters.PhotosPaginationQueryParameters.PageSize)
                },
                Albums = detailedUser.Albums,
                PostsCount = detailedUser.PostCounts
            };
        }

        /// <inheritdoc />
        public Task<User> FindByIdWithIncludesAsync(string id, params Expression<Func<User, object>>[] includes)
        {
            return includes.Aggregate(
                    Users,
                    (query, nextIncludeExpression) => query.Include(nextIncludeExpression))
                .FirstOrDefaultAsync(user => user.Id == id);
        }

        /// <inheritdoc />
        public async Task<FollowersOrFollowingAggregatedModel> GetFollowersAsync(
            DbGetFollowersOrFollowingQueryParameters queryParameters)
        {
            var amountToSkip = queryParameters.PaginationQueryParameters.PageSize *
                               queryParameters.PaginationQueryParameters.PageNumber;

            var selectionResult = await _photoAlbumContext.Set<User>()
                .Where(user => user.Id == queryParameters.UserId)
                .Select(user => new
                {
                    User = user,
                    Followers = user.Followers.OrderByDescending(f => f.Email)
                        .Skip(amountToSkip)
                        .Take(queryParameters.PaginationQueryParameters.PageSize),
                    TotalCount = user.Followers.Count()
                })
                .FirstOrDefaultAsync();

            if (selectionResult == null)
            {
                return null;
            }

            return new FollowersOrFollowingAggregatedModel
            {
                Users = new DbPaginationData<User>
                {
                    Items = selectionResult.Followers,
                    TotalPages = (int) Math.Ceiling((double) selectionResult.TotalCount /
                                                    queryParameters.PaginationQueryParameters.PageSize),
                    CurrentPage = queryParameters.PaginationQueryParameters.PageNumber
                },
                User = selectionResult.User
            };
        }

        /// <inheritdoc />
        public async Task<FollowersOrFollowingAggregatedModel> GetFollowingAsync(
            DbGetFollowersOrFollowingQueryParameters queryParameters)
        {
            var amountToSkip = queryParameters.PaginationQueryParameters.PageSize *
                               queryParameters.PaginationQueryParameters.PageNumber;

            var selectionResult = await _photoAlbumContext.Set<User>()
                .Where(user => user.Id == queryParameters.UserId)
                .Select(user => new
                {
                    User = user,
                    Users = user.Following
                        .OrderByDescending(f => f.Email)
                        .Skip(amountToSkip)
                        .Take(queryParameters.PaginationQueryParameters.PageSize),
                    TotalCount = user.Followers.Count()
                })
                .FirstOrDefaultAsync();

            if (selectionResult == null)
            {
                return null;
            }

            return new FollowersOrFollowingAggregatedModel
            {
                Users = new DbPaginationData<User>
                {
                    Items = selectionResult.Users,
                    TotalPages = (int)Math.Ceiling((double)selectionResult.TotalCount /
                                                   queryParameters.PaginationQueryParameters.PageSize),
                    CurrentPage = queryParameters.PaginationQueryParameters.PageNumber
                },
                User = selectionResult.User
            };
        }
    }
}