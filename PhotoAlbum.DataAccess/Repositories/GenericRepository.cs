﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;

namespace PhotoAlbum.DataAccess.Repositories
{
    /// <summary>
    ///     Represents generic repository.
    /// </summary>
    /// <typeparam name="TEntity">Type of entity.</typeparam>
    /// <typeparam name="TKey">Type of entity key.</typeparam>
    public class GenericRepository<TEntity, TKey> : IGenericRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
        where TKey : IComparable<TKey>, new()
    {
        protected readonly IPhotoAlbumContext PhotoAlbumContext;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GenericRepository{TEntity,TKey}" /> class.
        /// </summary>
        /// <param name="photoAlbumContext">Photo album context.</param>
        public GenericRepository(IPhotoAlbumContext photoAlbumContext)
        {
            PhotoAlbumContext = photoAlbumContext;
        }

        /// <inheritdoc />
        public TEntity Add(TEntity entity)
        {
            entity.CreatedDateTime = DateTime.UtcNow;
            entity.LastModifiedDateTime = entity.CreatedDateTime;
            return PhotoAlbumContext.Set<TEntity>().Add(entity);
        }

        /// <inheritdoc />
        public TEntity Delete(TEntity entity)
        {
            return PhotoAlbumContext.Set<TEntity>().Remove(entity);
        }

        /// <inheritdoc />
        public void Update(TEntity entity)
        {
            PhotoAlbumContext.Entry(entity).State = EntityState.Modified;
            entity.LastModifiedDateTime = DateTime.UtcNow;
        }

        /// <inheritdoc />
        public Task<TEntity> GetByIdAsync(TKey id, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = PhotoAlbumContext.Set<TEntity>();

            query = includes.Aggregate(query, (current, expression) => current.Include(expression));

            return query
                .FirstOrDefaultAsync(entity => !entity.IsDeleted && entity.Id.CompareTo(id) == 0);
        }

        /// <inheritdoc />
        public IEnumerable<TEntity> GetFiltered(Expression<Func<TEntity, bool>> filter, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = PhotoAlbumContext.Set<TEntity>();

            query = includes.Aggregate(query, (current, expression) => current.Include(expression));

            return query
                .Where(filter)
                .ToList();
        }
    }
}