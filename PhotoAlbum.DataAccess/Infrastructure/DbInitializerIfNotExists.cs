﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Represents database initializer, runs if database does not exists.
    /// </summary>
    public sealed class DbInitializerIfNotExists : CreateDatabaseIfNotExists<PhotoAlbumContext>
    {
        /// <inheritdoc />
        protected override void Seed(PhotoAlbumContext context)
        {
            var usersDynamics = ReadDataFromDataFile("UsersData.json");
            var albumsDynamics = ReadDataFromDataFile("AlbumsData.json");
            var rolesDynamics = ReadDataFromDataFile("RolesData.json");
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<User>(new UserStore<User>(context));

            // Seed roles
            foreach (var role in rolesDynamics)
            {
                var roleEntity = new IdentityRole
                {
                    Name = role.name
                };

                roleManager.Create(roleEntity);
            }

            // Seed users
            foreach (var user in usersDynamics)
            {
                var userEntity = new User
                {
                    Email = user.email,
                    UserName = user.email,
                    FirstName = user.firstName,
                    LastName = user.lastName,
                    Biography = user.biography,
                    ProfilePhotoId = user.photoId 
                };

                userManager.Create(userEntity, "user123");

                foreach (var userRole in user.roles)
                {
                    userManager.AddToRole(userEntity.Id, (string)userRole);
                }
            }

            // Seed Albums 
            foreach (var album in albumsDynamics)
            {
                var createdModifiedDateTime = DateTime.UtcNow;
                var albumEntity = new Album
                {
                    CreatedDateTime = createdModifiedDateTime,
                    LastModifiedDateTime = createdModifiedDateTime,
                    Description = album.description,
                    Title = album.title,
                    OwnerId = userManager.FindByEmail((string)album.ownerEmail).Id,
                    IsPrivate = album.isPrivate 
                };

                context.Albums.Add(albumEntity);
            }
            context.SaveChanges();
        }

        private static IEnumerable<dynamic> ReadDataFromDataFile(string fileName)
        {
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data", fileName);
            var jsonData = File.ReadAllText(filePath);

            return JsonConvert.DeserializeObject<dynamic[]>(jsonData);
        } 
    }
}