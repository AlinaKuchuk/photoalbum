﻿namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Contains model constants.
    /// </summary>
    internal static class ModelConstants
    {
        /// <summary>
        ///     Gets date-time column type.
        /// </summary>
        internal const string DateTimeColumnType = "datetime2";

        /// <summary>
        ///     Contains constants for the <see cref="Post" /> entity.
        /// </summary>
        internal static class Post
        {
            /// <summary>
            ///     Gets description maximum length.
            /// </summary>
            internal const int DescriptionMaximumLength = 1000;

            /// <summary>
            ///     Gets location maximum length.
            /// </summary>
            internal const int LocationMaximumLength = 300;
        }

        /// <summary>
        ///     Contains constants for the <see cref="User" /> entity.
        /// </summary>
        internal static class User
        {
            /// <summary>
            ///     Gets first name maximum length.
            /// </summary>
            internal const int FirstNameMaximumLength = 300;

            /// <summary>
            ///     Gets last name maximum length.
            /// </summary>
            internal const int LastNameMaximumLength = 300;

            /// <summary>
            ///     Gets biography maximum length.
            /// </summary>
            internal const int BiographyMaximumLength = 600;
        }

        /// <summary>
        ///     Contains constants for the <see cref="Comment" /> entity.
        /// </summary>
        internal static class Comment
        {
            /// <summary>
            ///     Gets comment content maximum length.
            /// </summary>
            internal const int ContentMaximumLength = 500;
        }

        /// <summary>
        ///     Contains constant for the <see cref="Album" /> entity.
        /// </summary>
        internal static class Album
        {
            /// <summary>
            ///     Gets title maximum length.
            /// </summary>
            internal const int TitleMaximumLength = 300;

            /// <summary>
            ///     Gets description maximum length.
            /// </summary>
            internal const int DescriptionMaximumLength = 1000;
        }
    }
}