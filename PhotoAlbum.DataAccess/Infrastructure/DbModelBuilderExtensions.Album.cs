﻿using System.Data.Entity;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Contains extensions methods to the <see cref="DbModelBuilder" /> class.
    /// </summary>
    internal static partial class DbModelBuilderExtensions
    {
        /// <summary>
        ///     Configures the <see cref="Album" /> entity.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        /// <returns>Configured album entity.</returns>
        internal static DbModelBuilder ConfigureAlbumEntity(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Album>()
                .HasRequired(album => album.Owner)
                .WithMany(user => user.Albums)
                .HasForeignKey(album => album.OwnerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Album>()
                .Property(album => album.Description)
                .IsRequired()
                .HasMaxLength(ModelConstants.Album.DescriptionMaximumLength);

            modelBuilder.Entity<Album>()
                .Property(album => album.Title)
                .IsRequired()
                .HasMaxLength(ModelConstants.Album.TitleMaximumLength);

            return modelBuilder.ConfigureDateTimeColumn<Album, int>();
        }
    }
}