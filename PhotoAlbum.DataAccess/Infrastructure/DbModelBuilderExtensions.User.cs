﻿using System.Data.Entity;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Contains extensions method to the <see cref="DbModelBuilder" /> class.
    /// </summary>
    internal static partial class DbModelBuilderExtensions
    {
        /// <summary>
        ///     Configures the <see cref="User" /> entity.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        /// <returns>Configured model builder.</returns>
        internal static DbModelBuilder ConfigureUserEntity(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(user => user.Followers)
                .WithMany(follower => follower.Following)
                .Map(relation =>
                {
                    relation.MapLeftKey("UserId");
                    relation.MapRightKey("FollowerId");
                    relation.ToTable("UserFollowers");
                });

            modelBuilder.Entity<User>()
                .Property(user => user.FirstName)
                .IsRequired()
                .HasMaxLength(ModelConstants.User.FirstNameMaximumLength);

            modelBuilder.Entity<User>()
                .Property(user => user.LastName)
                .IsRequired()
                .HasMaxLength(ModelConstants.User.LastNameMaximumLength);

            modelBuilder.Entity<User>()
                .Property(user => user.Biography)
                .HasMaxLength(ModelConstants.User.BiographyMaximumLength);

            return modelBuilder;
        }
    }
}