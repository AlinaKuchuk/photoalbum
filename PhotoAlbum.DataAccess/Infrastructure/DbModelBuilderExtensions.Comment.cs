﻿using System.Data.Entity;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Contains extensions methods to the <see cref="DbModelBuilder" /> class.
    /// </summary>
    internal static partial class DbModelBuilderExtensions
    {
        /// <summary>
        ///     Configures the <see cref="Comment" /> entity.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        /// <returns>Configured model builder.</returns>
        internal static DbModelBuilder ConfigureCommentEntity(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .HasRequired(comment => comment.Author)
                .WithMany(user => user.Comments)
                .HasForeignKey(comment => comment.AuthorId);

            modelBuilder.Entity<Comment>()
                .HasRequired(comment => comment.Post)
                .WithMany(post => post.Comments)
                .HasForeignKey(comment => comment.PostId);

            modelBuilder.Entity<Comment>()
                .Property(comment => comment.Content)
                .IsRequired()
                .HasMaxLength(ModelConstants.Comment.ContentMaximumLength);

            return modelBuilder.ConfigureDateTimeColumn<Comment, int>();
        }
    }
}