﻿using System.Data.Entity;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Contains extensions methods to the <see cref="DbModelBuilder" /> class.
    /// </summary>
    internal static partial class DbModelBuilderExtensions
    {
        /// <summary>
        ///     Configured model builder for the <see cref="Post" /> entity.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        /// <returns>Configured model builder.</returns>
        internal static DbModelBuilder ConfigurePostEntity(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>()
                .HasRequired(post => post.Album)
                .WithMany(album => album.Posts)
                .HasForeignKey(post => post.AlbumId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Post>()
                .Property(post => post.Description)
                .HasMaxLength(ModelConstants.Post.DescriptionMaximumLength);

            modelBuilder.Entity<Post>()
                .Property(post => post.Location)
                .HasMaxLength(ModelConstants.Post.LocationMaximumLength);

            return modelBuilder.ConfigureDateTimeColumn<Post, int>();
        }
    }
}