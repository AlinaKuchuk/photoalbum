﻿using System.Data.Entity;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Contains extensions methods to the <see cref="DbModelBuilder" /> class.
    /// </summary>
    internal static partial class DbModelBuilderExtensions
    {
        /// <summary>
        ///     Configures model builder for the <see cref="PhotoMetadata" /> entity.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        /// <returns>Configured model builder.</returns>
        internal static DbModelBuilder ConfigurePhotoMetadataEntity(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PhotoMetadata>()
                .HasRequired(photo => photo.Owner)
                .WithMany(user => user.Photos)
                .HasForeignKey(photo => photo.OwnerId);

            modelBuilder.Entity<PhotoMetadata>()
                .HasRequired(photo => photo.Post)
                .WithMany(post => post.Photos)
                .HasForeignKey(photo => photo.PostId);

            modelBuilder.Entity<PhotoMetadata>()
                .Property(photo => photo.FileId)
                .IsRequired();

            return modelBuilder.ConfigureDateTimeColumn<PhotoMetadata, int>();
        }
    }
}