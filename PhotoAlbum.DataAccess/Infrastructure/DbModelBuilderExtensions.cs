﻿using System;
using System.Data.Entity;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Contains extensions method to the <see cref="DbModelBuilder" /> class.
    /// </summary>
    internal static partial class DbModelBuilderExtensions
    {
        /// <summary>
        ///     Configures date-time for column type.
        /// </summary>
        /// <typeparam name="TEntity">Type of entity.</typeparam>
        /// <typeparam name="TKey">Type of entity key.</typeparam>
        /// <param name="modelBuilder">Model builder.</param>
        /// <returns>Configured model builder.</returns>
        internal static DbModelBuilder ConfigureDateTimeColumn<TEntity, TKey>(this DbModelBuilder modelBuilder)
            where TEntity : BaseEntity<TKey>
            where TKey : IComparable<TKey>, new()
        {
            modelBuilder.Entity<TEntity>()
                .Property(entity => entity.CreatedDateTime)
                .IsRequired()
                .HasColumnType(ModelConstants.DateTimeColumnType);

            modelBuilder.Entity<TEntity>()
                .Property(entity => entity.LastModifiedDateTime)
                .IsRequired()
                .HasColumnType(ModelConstants.DateTimeColumnType);

            return modelBuilder;
        }
    }
}