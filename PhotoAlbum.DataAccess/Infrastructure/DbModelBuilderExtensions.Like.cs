﻿using System.Data.Entity;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Infrastructure
{
    /// <summary>
    ///     Contains extensions methods for the <see cref="DbModelBuilder" /> class.
    /// </summary>
    internal static partial class DbModelBuilderExtensions
    {
        /// <summary>
        ///     Configures model builder for the <see cref="Like" /> entity.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        /// <returns>Configured model builder.</returns>
        internal static DbModelBuilder ConfigureLikeEntity(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Like>()
                .HasRequired(like => like.User)
                .WithMany(user => user.Likes)
                .HasForeignKey(like => like.UserId);

            modelBuilder.Entity<Like>()
                .HasRequired(like => like.Post)
                .WithMany(post => post.Likes)
                .HasForeignKey(post => post.PostId);

            return modelBuilder.ConfigureDateTimeColumn<Like, int>();
        }
    }
}