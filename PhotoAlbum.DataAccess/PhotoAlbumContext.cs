﻿using System.Data.Entity;
using System.Diagnostics;
using Microsoft.AspNet.Identity.EntityFramework;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Infrastructure;
using PhotoAlbum.DataAccess.Interfaces;

namespace PhotoAlbum.DataAccess
{
    /// <summary>
    ///     Represents photo album context.
    /// </summary>
    public sealed class PhotoAlbumContext : IdentityDbContext<User>, IPhotoAlbumContext
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PhotoAlbumContext" /> class.
        /// </summary>
        public PhotoAlbumContext() : base("PhotoAlbumConnectionString")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DbInitializerIfNotExists());
        }

        /// <summary>
        ///     Gets or sets exceptions database set.
        /// </summary>
        public IDbSet<ExceptionData> Exceptions { get; set; }

        /// <summary>
        ///     Gets or sets photo metadata database set.
        /// </summary>
        public IDbSet<PhotoMetadata> PhotoMetadata { get; set; }

        /// <summary>
        ///     Gets or sets albums database set.
        /// </summary>
        public IDbSet<Album> Albums { get; set; }

        /// <summary>
        ///     Gets or sets comments database set.
        /// </summary>
        public IDbSet<Comment> Comments { get; set; }

        /// <summary>
        ///     Gets or sets likes database set.
        /// </summary>
        public IDbSet<Like> Likes { get; set; }

        /// <summary>
        ///     Gets or sets posts.
        /// </summary>
        public IDbSet<Post> Posts { get; set; }

        /// <summary>
        ///     Maps table names, and sets up relationships between the various user entities.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .ConfigurePhotoMetadataEntity()
                .ConfigureLikeEntity()
                .ConfigurePostEntity()
                .ConfigureUserEntity()
                .ConfigureCommentEntity();
        }
    }
}