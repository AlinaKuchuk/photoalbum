﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents role.
    /// </summary>
    public class Role : IdentityRole
    {
    }
}