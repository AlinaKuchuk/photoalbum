﻿using System;

namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents photo metadata.
    /// </summary>
    public class PhotoMetadata : BaseEntity<int>
    {
        /// <summary>
        ///     Gets or sets file name.
        /// </summary>
        public Guid FileId { get; set; }

        /// <summary>
        ///     Gets or sets file extension.
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        ///     Gets or sets owner identifier.
        /// </summary>
        public string OwnerId { get; set; }

        /// <summary>
        ///     Gets or sets owner.
        /// </summary>
        public User Owner { get; set; }

        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        public int PostId { get; set; }

        /// <summary>
        ///     Gets or sets post.
        /// </summary>
        public Post Post { get; set; }
    }
}