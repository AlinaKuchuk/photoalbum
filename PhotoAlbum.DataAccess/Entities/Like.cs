﻿namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents post like.
    /// </summary>
    public class Like : BaseEntity<int>
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets user.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        public int PostId { get; set; }

        /// <summary>
        ///     Gets or sets post.
        /// </summary>
        public Post Post { get; set; }
    }
}