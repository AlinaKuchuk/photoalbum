﻿namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents exception data.
    /// </summary>
    public sealed class ExceptionData : BaseEntity<int>
    {
        /// <summary>
        ///     Gets or sets user principal identifier.
        /// </summary>
        public string UserPrincipalId { get; set; }

        /// <summary>
        ///     Gets or sets url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        ///     Gets or sets stack trace.
        /// </summary>
        public string StackTrace { get; set; }
    }
}