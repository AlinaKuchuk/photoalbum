﻿using System.Collections.Generic;

namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents album.
    /// </summary>
    public class Album : BaseEntity<int>
    {
        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Gets or sets owner identifier.
        /// </summary>
        public string OwnerId { get; set; }

        /// <summary>
        ///     Gets or sets owner.
        /// </summary>
        public User Owner { get; set; }

        /// <summary>
        ///     Gets or sets posts.
        /// </summary>
        public ICollection<Post> Posts { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is album private or not.
        /// </summary>
        public bool IsPrivate { get; set; }    }
}