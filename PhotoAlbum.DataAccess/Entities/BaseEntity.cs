﻿using System;

namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents base entity. Contains shared properties for all entities.
    /// </summary>
    /// <typeparam name="TKey">Type of identifier.</typeparam>
    public abstract class BaseEntity<TKey>
        where TKey : IComparable<TKey>, new()
    {
        /// <summary>
        ///     Gets or sets identifier.
        /// </summary>
        public TKey Id { get; set; }

        /// <summary>
        ///     Gets or sets a value indicates is entity deleted or not.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        ///     Gets or sets created date-time in UTC.
        /// </summary>
        public DateTime CreatedDateTime { get; set; }

        /// <summary>
        ///     Gets or sets last modified date-time int UTC.
        /// </summary>
        public DateTime LastModifiedDateTime { get; set; }
    }
}