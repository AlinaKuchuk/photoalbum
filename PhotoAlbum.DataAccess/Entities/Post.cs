﻿using System.Collections.Generic;

namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents single post.
    /// </summary>
    public class Post : BaseEntity<int>
    {
        /// <summary>
        ///     Gets or sets likes.
        /// </summary>
        public ICollection<Like> Likes { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public ICollection<PhotoMetadata> Photos { get; set; }

        /// <summary>
        ///     Gets or sets comments.
        /// </summary>
        public ICollection<Comment> Comments { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        ///     Gets or sets album identifier.
        /// </summary>
        public int AlbumId { get; set; }

        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        public Album Album { get; set; }

        /// <summary>
        ///     Gets or sets count of views.
        /// </summary>
        public int Views { get; set; }
    }
}