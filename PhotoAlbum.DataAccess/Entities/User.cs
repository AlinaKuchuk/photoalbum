﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents user.
    /// </summary>
    public class User : IdentityUser
    {
        /// <summary>
        ///     Gets or sets first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is user account private or not.
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        ///     Gets or sets last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets biography.
        /// </summary>
        public string Biography { get; set; }

        /// <summary>
        ///     Gets or sets profile photo identifier.
        /// </summary>
        public Guid? ProfilePhotoId { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public ICollection<PhotoMetadata> Photos { get; set; }

        /// <summary>
        ///     Gets or sets comments.
        /// </summary>
        public ICollection<Comment> Comments { get; set; }

        /// <summary>
        ///     Gets or sets likes.
        /// </summary>
        public ICollection<Like> Likes { get; set; }

        /// <summary>
        ///     Gets or sets albums.
        /// </summary>
        public ICollection<Album> Albums { get; set; }

        /// <summary>
        ///     Gets or sets followers.
        /// </summary>
        public ICollection<User> Followers { get; set; }

        /// <summary>
        ///     Gets or sets following.
        /// </summary>
        public ICollection<User> Following { get; set; }
    }
}