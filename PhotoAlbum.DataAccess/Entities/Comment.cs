﻿namespace PhotoAlbum.DataAccess.Entities
{
    /// <summary>
    ///     Represents post comment.
    /// </summary>
    public class Comment : BaseEntity<int>
    {
        /// <summary>
        ///     Gets or sets content.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        ///     Gets or sets author identifier.
        /// </summary>
        public string AuthorId { get; set; }

        /// <summary>
        ///     Gets or sets author.
        /// </summary>
        public User Author { get; set; }

        /// <summary>
        /// Gets or sets post identifier.
        /// </summary>
        public int PostId { get; set; }

        /// <summary>
        /// Gets or sets post.
        /// </summary>
        public Post Post { get; set; }
    }
}