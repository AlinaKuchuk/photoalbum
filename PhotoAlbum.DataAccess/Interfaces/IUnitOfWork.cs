﻿using System.Threading;
using System.Threading.Tasks;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Interfaces
{
    /// <summary>
    ///     Provides an interface for unit of work.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        ///     Gets exception data repository.
        /// </summary>
        IGenericRepository<ExceptionData, int> ExceptionDataRepository { get; }

        /// <summary>
        ///     Gets album repository.
        /// </summary>
        IAlbumRepository AlbumRepository { get; }

        /// <summary>
        ///     Gets comment repository.
        /// </summary>
        IGenericRepository<Comment, int> CommentRepository { get; }

        /// <summary>
        ///     Gets like repository.
        /// </summary>
        IGenericRepository<Like, int> LikeRepository { get; }

        /// <summary>
        ///     Gets photo metadata repository.
        /// </summary>
        IGenericRepository<PhotoMetadata, int> PhotoMetadataRepository { get; }

        /// <summary>
        ///     Get post repository.
        /// </summary>
        IPostRepository PostRepository { get; }

        /// <summary>
        ///     Gets user repository.
        /// </summary>
        IUserRepository UserRepository { get; }

        /// <summary>
        ///     Gets role repository.
        /// </summary>
        IRoleRepository RoleRepository { get; }

        /// <summary>
        ///     Performs saving all changes applied during session.
        /// </summary>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task CommitAsync(CancellationToken cancellationToken = default);
    }
}