﻿using System.Threading.Tasks;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.QueryParameters;

namespace PhotoAlbum.DataAccess.Interfaces
{
    /// <summary>
    ///     Provides interface for album repository.
    /// </summary>
    public interface IAlbumRepository : IGenericRepository<Album, int>
    {
        /// <summary>
        ///     Gets all user albums.
        /// </summary>
        /// <param name="queryParameters">Albums pagination query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous saving operation.</returns>
        Task<UserAlbumsAggregatedModel> GetUserAlbumsAsync(DbGetUserAlbumsQueryParameters queryParameters);

        /// <summary>
        ///     Gets all photo in the album.
        /// </summary>
        /// <param name="queryParameters">Photos query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous saving operation.</returns>
        Task<FullAlbumAggregatedModel> GetAlbumPhotosAsync(DbGetAlbumPhotosQueryParameters queryParameters);
    }
}