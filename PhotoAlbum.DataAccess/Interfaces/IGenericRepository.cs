﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Interfaces
{
    /// <summary>
    ///     Provides an interface for generic repository.
    /// </summary>
    /// <typeparam name="TEntity">Type of entity.</typeparam>
    /// <typeparam name="TKey">Type of key.</typeparam>
    public interface IGenericRepository<TEntity, in TKey>
        where TEntity : BaseEntity<TKey>
        where TKey :  IComparable<TKey>, new()
    {
        /// <summary>
        ///     Retrieves entity by identifier.
        /// </summary>
        /// <param name="id">Entity identifier.</param>
        /// <param name="includes">A list of included properties.</param>
        /// <returns>Entity.</returns>
        Task<TEntity> GetByIdAsync(TKey id, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        ///     Removes entity.
        /// </summary>
        /// <param name="entity">Entity for removing.</param>
        /// <returns>Removed entity.</returns>
        TEntity Delete(TEntity entity);

        /// <summary>
        ///     Update entity.
        /// </summary>
        /// <param name="entity">Entity to update.</param>
        void Update(TEntity entity);

        /// <summary>
        ///     Adds entity.
        /// </summary>
        /// <param name="entity">Entity for adding.</param>
        /// <returns>Added entity.</returns>
        TEntity Add(TEntity entity);

        /// <summary>
        ///     Retrieves entities by filter.
        /// </summary>
        /// <param name="filter">Expression representing filter.</param>
        /// <param name="includes">A list of include expressions.</param>
        /// <returns>Filtered entities.</returns>
        IEnumerable<TEntity> GetFiltered(Expression<Func<TEntity, bool>> filter, params Expression<Func<TEntity, object>>[] includes);
    }
}