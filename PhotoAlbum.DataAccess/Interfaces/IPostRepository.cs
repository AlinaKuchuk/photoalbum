﻿using System.Threading.Tasks;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.QueryParameters;
using PhotoAlbum.DataAccess.Models.Enums;

namespace PhotoAlbum.DataAccess.Interfaces
{
    /// <summary>
    ///     Provides an interface for the post repository.
    /// </summary>
    public interface IPostRepository : IGenericRepository<Post, int>
    {
        /// <summary>
        ///     Gets full information about the post.
        /// </summary>
        /// <param name="id">Post identifier.</param>
        /// <param name="requestingUserId"></param>
        /// <param name="commentsDbPaginationQueryParameters">Pagination information for comments.</param>
        /// <param name="requestingUserRoles"></param>
        /// <returns>A <see cref="Task" /> representing asynchronous saving operation.</returns>
        Task<FullPostAggregatedModel> GetFullPostAsync(int id, string requestingUserId,
            DbPaginationQueryParameters commentsDbPaginationQueryParameters, string[] requestingUserRoles);

        /// <summary>
        ///     Gets following updates posts.
        /// </summary>
        /// <param name="searchText">Search test.</param>
        /// <param name="requestingUserId">User identifier.</param>
        /// <param name="paginationQueryParameters">Pagination following updates posts.</param>
        /// <param name="sortingField">Sorting field.</param>
        /// <param name="sortingOrder">Sorting order.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous getting operation.</returns>
        Task<DbPaginationData<PreviewPostAggregatedModel>> GetExplorePostsAsync(
            string searchText,
            string requestingUserId,
            DbPaginationQueryParameters paginationQueryParameters,
            string sortingField,
            SortOrder sortingOrder);

        /// <summary>
        ///     Gets following updates posts.
        /// </summary>
        /// <param name="searchText">Search test.</param>
        /// <param name="requestingUserId">User identifier.</param>
        /// <param name="paginationQueryParameters">Pagination following updates posts.</param>
        /// <param name="sortingField"></param>
        /// <param name="sortingOrder"></param>
        /// <returns>A <see cref="Task" /> representing asynchronous getting operation.</returns>
        Task<DbPaginationData<PreviewPostAggregatedModel>> GetFollowingUpdatesAsync(
            string searchText,
            string requestingUserId,
            DbPaginationQueryParameters paginationQueryParameters, 
            string sortingField, 
            SortOrder sortingOrder);

        /// <summary>
        ///     Gets post statistics.
        /// </summary>
        /// <param name="requestingUserId">User identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<PostsStatistic> GetPostsStatisticAsync(string requestingUserId);
    }
}