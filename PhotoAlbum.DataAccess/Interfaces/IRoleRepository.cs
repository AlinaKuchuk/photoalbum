﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PhotoAlbum.DataAccess.Interfaces
{
    /// <summary>
    ///     Provides an interface for the role repository.
    /// </summary>
    public interface IRoleRepository
    {
        /// <summary>
        ///     Creates role.
        /// </summary>
        /// <param name="role">Role.</param>
        /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
        Task<IdentityResult> CreateAsync(IdentityRole role);

        /// <summary>
        ///     Updates role.
        /// </summary>
        /// <param name="role">Role to update.</param>
        /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
        Task<IdentityResult> UpdateAsync(IdentityRole role);

        /// <summary>
        ///     Deletes role.
        /// </summary>
        /// <param name="role">Role to delete.</param>
        /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
        Task<IdentityResult> DeleteAsync(IdentityRole role);

        /// <summary>
        ///     Checks if role exists or not.
        /// </summary>
        /// <param name="roleName">Role name.</param>
        /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
        /// <returns></returns>
        Task<bool> RoleExistsAsync(string roleName);

        /// <summary>
        ///     Finds role by identifier.
        /// </summary>
        /// <param name="roleId">Role identifier.</param>
        /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
        Task<IdentityRole> FindByIdAsync(string roleId);

        /// <summary>
        ///     Finds role by name.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
        Task<IdentityRole> FindByNameAsync(string roleName);

        /// <summary>
        /// Retrieves roles.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
        Task<IEnumerable<IdentityRole>> GetRolesAsync();
    }
}