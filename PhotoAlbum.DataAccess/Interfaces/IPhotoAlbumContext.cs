﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PhotoAlbum.DataAccess.Interfaces
{
    /// <summary>
    ///     Provides generic interface for the photo album context.
    /// </summary>
    public interface IPhotoAlbumContext
    {
        /// <summary>
        ///     Gets roles database set.
        /// </summary>
        IDbSet<IdentityRole> Roles { get; }

        /// <summary>
        ///     Generates instance of the <see cref="DbSet{TEntity}" /> class.
        /// </summary>
        /// <typeparam name="TEntity">Type of entity.</typeparam>
        /// <returns>Database set.</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        ///     Saves changes.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous saving operation.</returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        ///     Save changes.
        /// </summary>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous saving operation.</returns>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        /// <summary>
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}