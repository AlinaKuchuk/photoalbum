﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.QueryParameters;

namespace PhotoAlbum.DataAccess.Interfaces
{
    /// <summary>
    ///     Provide an interface for the user repository.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        ///     Creates user identity.
        /// </summary>
        /// <param name="user">User.</param>
        /// <param name="authenticationType">Authentication type.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<ClaimsIdentity> CreateIdentityAsync(User user, string authenticationType);

        /// <summary>
        ///     Creates user.
        /// </summary>
        /// <param name="user">User to create.</param>
        /// <param name="password">User password.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<IdentityResult> CreateAsync(User user, string password);

        /// <summary>
        ///     Updates user.
        /// </summary>
        /// <param name="user">User name.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<IdentityResult> UpdateAsync(User user);

        /// <summary>
        ///     Deletes user.
        /// </summary>
        /// <param name="user">User to delete.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<IdentityResult> DeleteAsync(User user);

        /// <summary>
        ///     Finds user by identifier.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<User> FindByIdAsync(string userId);

        /// <summary>
        ///     Finds user by name.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<User> FindByNameAsync(string userName);

        /// <summary>
        ///     Find user by email.
        /// </summary>
        /// <param name="email">Email for finding.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<User> FindByEmailAsync(string email);

        /// <summary>
        ///     Adds user to the role.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="role">Role.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<IdentityResult> AddToRoleAsync(string userId, string role);

        /// <summary>
        ///     Retrieves user by email and password.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        /// <returns>A <see cref="Task" /> represents asynchronous operation.</returns>
        Task<User> FindAsync(string userName, string password);

        /// <summary>
        ///     Retrieves users by predicate.
        /// </summary>
        /// <param name="filterExpression"></param>
        /// <returns></returns>
        Task<IEnumerable<UserWithRolesAggregatedModel>> GetFilteredAsync(Expression<Func<User, bool>> filterExpression);

        /// <summary>
        ///     Retrieves user with detailed fields.
        /// </summary>
        /// <param name="queryParameters">Get full user query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation result.</returns>
        Task<FullUserAggregatedModel> GetFullUserAsync(DbGetFullUserQueryParameters queryParameters);

        /// <summary>
        ///     Finds user by identifier with set of include expressions.
        /// </summary>
        /// <param name="id">User identifier.</param>
        /// <param name="includes">A set of include expressions.</param>
        /// <returns></returns>
        Task<User> FindByIdWithIncludesAsync(string id, params Expression<Func<User, object>>[] includes);

        /// <summary>
        ///     Retrieves user followers.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation result.</returns>
        Task<FollowersOrFollowingAggregatedModel> GetFollowersAsync(DbGetFollowersOrFollowingQueryParameters queryParameters);

        /// <summary>
        ///     Retrieves user following.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation result.</returns>
        Task<FollowersOrFollowingAggregatedModel> GetFollowingAsync(DbGetFollowersOrFollowingQueryParameters queryParameters);
    }
}