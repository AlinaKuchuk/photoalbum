﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NameRelatioProperly : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UserUsers", newName: "UserFollowers");
            RenameColumn(table: "dbo.UserFollowers", name: "User_Id", newName: "UserId");
            RenameColumn(table: "dbo.UserFollowers", name: "User_Id1", newName: "FollowerId");
            RenameIndex(table: "dbo.UserFollowers", name: "IX_User_Id", newName: "IX_UserId");
            RenameIndex(table: "dbo.UserFollowers", name: "IX_User_Id1", newName: "IX_FollowerId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.UserFollowers", name: "IX_FollowerId", newName: "IX_User_Id1");
            RenameIndex(table: "dbo.UserFollowers", name: "IX_UserId", newName: "IX_User_Id");
            RenameColumn(table: "dbo.UserFollowers", name: "FollowerId", newName: "User_Id1");
            RenameColumn(table: "dbo.UserFollowers", name: "UserId", newName: "User_Id");
            RenameTable(name: "dbo.UserFollowers", newName: "UserUsers");
        }
    }
}
