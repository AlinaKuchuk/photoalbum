﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddStatisticFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsPrivate", c => c.Boolean(nullable: false));
            AddColumn("dbo.Posts", "Views", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "Views");
            DropColumn("dbo.AspNetUsers", "IsPrivate");
        }
    }
}
