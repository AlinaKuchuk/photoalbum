﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPrivateFlagToAlbum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Albums", "IsPrivate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Albums", "IsPrivate");
        }
    }
}
