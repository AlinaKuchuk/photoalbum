﻿using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<PhotoAlbumContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}