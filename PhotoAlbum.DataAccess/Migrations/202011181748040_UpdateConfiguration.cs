﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateConfiguration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Location", c => c.String(maxLength: 300));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Location", c => c.String());
        }
    }
}
