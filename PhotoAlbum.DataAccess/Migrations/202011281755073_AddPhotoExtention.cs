﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhotoExtention : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhotoMetadatas", "Extension", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PhotoMetadatas", "Extension");
        }
    }
}
