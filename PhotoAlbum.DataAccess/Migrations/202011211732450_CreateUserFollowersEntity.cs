﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateUserFollowersEntity : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UserFollowers", newName: "UserUsers");
            RenameColumn(table: "dbo.UserUsers", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.UserUsers", name: "FollowerId", newName: "User_Id1");
            RenameIndex(table: "dbo.UserUsers", name: "IX_UserId", newName: "IX_User_Id");
            RenameIndex(table: "dbo.UserUsers", name: "IX_FollowerId", newName: "IX_User_Id1");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.UserUsers", name: "IX_User_Id1", newName: "IX_FollowerId");
            RenameIndex(table: "dbo.UserUsers", name: "IX_User_Id", newName: "IX_UserId");
            RenameColumn(table: "dbo.UserUsers", name: "User_Id1", newName: "FollowerId");
            RenameColumn(table: "dbo.UserUsers", name: "User_Id", newName: "UserId");
            RenameTable(name: "dbo.UserUsers", newName: "UserFollowers");
        }
    }
}
