﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeBioOptional : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "Biography", c => c.String(maxLength: 600));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "Biography", c => c.String(nullable: false, maxLength: 600));
        }
    }
}
