﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConfigureDateTimeForPhotoMetadata : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PhotoMetadatas", "CreatedDateTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PhotoMetadatas", "LastModifiedDateTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PhotoMetadatas", "LastModifiedDateTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PhotoMetadatas", "CreatedDateTime", c => c.DateTime(nullable: false));
        }
    }
}
