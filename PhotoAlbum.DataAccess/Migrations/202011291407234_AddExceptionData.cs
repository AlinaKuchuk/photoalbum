﻿namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExceptionData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExceptionDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserPrincipalId = c.String(),
                        Url = c.String(),
                        StackTrace = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ExceptionDatas");
        }
    }
}
