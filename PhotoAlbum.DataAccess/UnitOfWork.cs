﻿using System.Threading;
using System.Threading.Tasks;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;

namespace PhotoAlbum.DataAccess
{
    /// <summary>
    ///     Represents unit of work.
    /// </summary>
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly IPhotoAlbumContext _photoAlbumContext;

        /// <summary>
        ///     Initializes a new instance of the <see cref="UnitOfWork" /> class.
        /// </summary>
        /// <param name="postRepository">Post repository.</param>
        /// <param name="photoMetadataRepository">Photo metadata repository.</param>
        /// <param name="likeRepository">Like repository.</param>
        /// <param name="commentRepository">Comment repository.</param>
        /// <param name="albumRepository">Album repository.</param>
        /// <param name="userRepository">User repository.</param>
        /// <param name="roleRepository">Role repository.</param>
        /// <param name="photoAlbumContext">Photo album context.</param>
        /// <param name="exceptionDataRepository">Exception data repository.</param>
        public UnitOfWork(
            IPostRepository postRepository,
            IGenericRepository<PhotoMetadata, int> photoMetadataRepository,
            IGenericRepository<Like, int> likeRepository,
            IGenericRepository<Comment, int> commentRepository,
            IAlbumRepository albumRepository,
            IUserRepository userRepository,
            IRoleRepository roleRepository,
            IPhotoAlbumContext photoAlbumContext,
            IGenericRepository<ExceptionData, int> exceptionDataRepository)
        {
            PostRepository = postRepository;
            PhotoMetadataRepository = photoMetadataRepository;
            LikeRepository = likeRepository;
            CommentRepository = commentRepository;
            AlbumRepository = albumRepository;
            _photoAlbumContext = photoAlbumContext;
            ExceptionDataRepository = exceptionDataRepository;
            UserRepository = userRepository;
            RoleRepository = roleRepository;
        }

        /// <inheritdoc />
        public IGenericRepository<ExceptionData, int> ExceptionDataRepository { get; }

        /// <inheritdoc />
        public IAlbumRepository AlbumRepository { get; }

        /// <inheritdoc />
        public IGenericRepository<Comment, int> CommentRepository { get; }

        /// <inheritdoc />
        public IGenericRepository<Like, int> LikeRepository { get; }

        /// <inheritdoc />
        public IGenericRepository<PhotoMetadata, int> PhotoMetadataRepository { get; }

        /// <inheritdoc />
        public IPostRepository PostRepository { get; }

        /// <inheritdoc />
        public IUserRepository UserRepository { get; }

        /// <inheritdoc />
        public IRoleRepository RoleRepository { get; }

        /// <inheritdoc />
        public Task CommitAsync(CancellationToken cancellationToken = default)
        {
            return _photoAlbumContext.SaveChangesAsync(cancellationToken);
        }
    }
}