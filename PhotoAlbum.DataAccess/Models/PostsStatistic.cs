﻿namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents post statistic.
    /// </summary>
    public sealed class PostsStatistic
    {
        /// <summary>
        ///     Gets or sets view count.
        /// </summary>
        public int ViewsCount { get; set; }

        /// <summary>
        ///     Gets or sets likes.
        /// </summary>
        public int LikesCount { get; set; }
    }
}