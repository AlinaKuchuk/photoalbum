﻿namespace PhotoAlbum.DataAccess.Models.Enums
{
    /// <summary>
    /// Represents sorting order.
    /// </summary>
    public enum SortOrder
    {
        /// <summary>
        /// Ascending sort.
        /// </summary>
        Asc,

        /// <summary>
        /// Descending sort.
        /// </summary>
        Desc
    }
}