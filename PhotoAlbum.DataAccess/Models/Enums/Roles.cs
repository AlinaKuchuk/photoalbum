﻿namespace PhotoAlbum.DataAccess.Models.Enums
{
    /// <summary>
    /// User roles.
    /// </summary>
    public enum Roles
    {
        /// <summary>
        /// Common user role.
        /// </summary>
        User,

        /// <summary>
        /// Business account role.
        /// </summary>
        Business,

        /// <summary>
        /// Administrator role.
        /// </summary>
        Administrator
    }
}