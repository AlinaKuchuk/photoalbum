﻿using System.Collections.Generic;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents pagination data.
    /// </summary>
    /// <typeparam name="TEntity">Type of entity.</typeparam>
    public sealed class DbPaginationData<TEntity>
        where TEntity : class
    {
        /// <summary>
        ///     Gets or sets items.
        /// </summary>
        public IEnumerable<TEntity> Items { get; set; }

        /// <summary>
        ///     Gets or sets number of total pages.
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        ///     Gets or sets current page.
        /// </summary>
        public int CurrentPage { get; set; }
    }
}