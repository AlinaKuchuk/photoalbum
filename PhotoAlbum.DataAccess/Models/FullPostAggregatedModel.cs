﻿using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents full post aggregated model.
    /// </summary>
    public sealed class FullPostAggregatedModel
    {
        /// <summary>
        ///     Gets or sets post.
        /// </summary>
        public Post Post { get; set; }

        /// <summary>
        ///     Gets or sets paginated comments.
        /// </summary>
        public DbPaginationData<Comment> Comments { get; set; }

        /// <summary>
        ///     Gets or sets number of likes.
        /// </summary>
        public int LikesCount { get; set; }

        /// <summary>
        ///     Gets or sets a valued indicating is post liked by requesting user or not.
        /// </summary>
        public bool IsLiked { get; set; }
    }
}