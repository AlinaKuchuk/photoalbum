﻿namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Contains custom claim names.
    /// </summary>
    public static class ClaimNamesConstants
    {
        /// <summary>
        ///     Gets or sets full name claim name.
        /// </summary>
        public const string FullNameClaimName = "FullName";

        /// <summary>
        ///     Gets or sets avatar claim name.
        /// </summary>
        public const string AvatarClaimName = "Avatar";

        /// <summary>
        ///     Gets or sets is private claim name.
        /// </summary>
        public const string IsPrivateClaimName = "IsPrivate";
    }
}