﻿using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents followers or following aggregated model.
    /// </summary>
    public sealed class FollowersOrFollowingAggregatedModel
    {
        /// <summary>
        ///     Gets or sets users.
        /// </summary>
        public DbPaginationData<User> Users { get; set; }

        /// <summary>
        ///     Gets or sets user.
        /// </summary>
        public User User { get; set; }
    }
}