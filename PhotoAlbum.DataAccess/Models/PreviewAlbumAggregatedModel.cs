﻿using System;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents preview album aggregated model.
    /// </summary>
    public sealed class PreviewAlbumAggregatedModel
    {
        /// <summary>
        ///     Gets or sets preview album photo identifier.
        /// </summary>
        public Guid? AlbumPreviewPhotoId { get; set; }

        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        public Album Album { get; set; }
    }
}
