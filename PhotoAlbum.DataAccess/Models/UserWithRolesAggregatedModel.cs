﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents user with model aggregation model.
    /// </summary>
    public sealed class UserWithRolesAggregatedModel
    {
        /// <summary>
        ///     Get or sets user.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        ///     Gets or sets roles.
        /// </summary>
        public IEnumerable<IdentityRole> Roles { get; set; }
    }
}