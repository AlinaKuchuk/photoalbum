﻿using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents album photos aggregated model.
    /// </summary>
    public sealed class FullAlbumAggregatedModel
    {
        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        public Album Album { get; set; }

        /// <summary>
        ///     Gets or sets number of posts in the album.
        /// </summary>
        public int PostCount { get; set; }

        /// <summary>
        ///     Gets or sets number of photos in the album.
        /// </summary>
        public int PhotosCount { get; set; }

        /// <summary>
        ///     Gets or sets photos and pagination data.
        /// </summary>
        public DbPaginationData<PhotoMetadata> Photos { get; set; }
    }
}
