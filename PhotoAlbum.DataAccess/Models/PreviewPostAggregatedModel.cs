﻿using System.Collections.Generic;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents preview aggregate post model.
    /// </summary>
    public sealed class PreviewPostAggregatedModel
    {
        /// <summary>
        ///     Gets or sets post.
        /// </summary>
        public Post Post { get; set; }

        /// <summary>
        ///     Gets or sets author.
        /// </summary>
        public User Author { get; set; }

        /// <summary>
        ///     Gets or sets albums.
        /// </summary>
        public Album Album { get; set; }

        /// <summary>
        ///     Gets or sets number of likes.
        /// </summary>
        public int LikesCount { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public IEnumerable<PhotoMetadata> Photos { get; set; }

        /// <summary>
        ///     Gets or sets comments.
        /// </summary>
        public IEnumerable<Comment> Comments { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is post liked by requesting user or not.
        /// </summary>
        public bool IsLiked { get; set; }
    }
}