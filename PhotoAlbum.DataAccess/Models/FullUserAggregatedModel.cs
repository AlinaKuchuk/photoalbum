﻿using System.Collections.Generic;
using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents full user aggregated model.
    /// </summary>
    public sealed class FullUserAggregatedModel
    {
        /// <summary>
        ///     Gets or sets user.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is requesting user follows requested user.
        /// </summary>
        public bool IsFollowed { get; set; }

        /// <summary>
        ///     Gets or sets followers.
        /// </summary>
        public int FollowersCount { get; set; }

        /// <summary>
        ///     Gets or sets following.
        /// </summary>
        public int FollowingCount { get; set; }

        /// <summary>
        ///     Gets or sets posts count.
        /// </summary>
        public int PostsCount { get; set; }

        /// <summary>
        ///     Gets or sets albums.
        /// </summary>
        public IEnumerable<PreviewAlbumAggregatedModel> Albums { get; set; }

        /// <summary>
        ///     Gets or sets posts.
        /// </summary>
        public DbPaginationData<PhotoMetadata> Photos { get; set; }
    }
}