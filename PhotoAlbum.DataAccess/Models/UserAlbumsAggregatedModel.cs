﻿using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Models
{
    /// <summary>
    ///     Represents user album aggregated model.
    /// </summary>
    public sealed class UserAlbumsAggregatedModel
    {
        /// <summary>
        ///     Gets or sets owner album.
        /// </summary>
        public User Owner { get; set; }

        /// <summary>
        ///     Gets or sets paginated albums.
        /// </summary>
        public DbPaginationData<PreviewAlbumAggregatedModel> Albums { get; set; }
    }
}
