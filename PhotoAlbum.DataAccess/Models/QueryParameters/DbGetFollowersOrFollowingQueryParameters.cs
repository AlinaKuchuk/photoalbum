﻿using PhotoAlbum.DataAccess.Models.QueryParameters;

namespace PhotoAlbum.DataAccess.Models.QueryParameters
{
    /// <summary>
    ///     Represents database get followers or following query parameters.
    /// </summary>
    public sealed class DbGetFollowersOrFollowingQueryParameters : BaseQueryParameters
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets pagination query parameters.
        /// </summary>
        public DbPaginationQueryParameters PaginationQueryParameters { get; set; }
    }
}