﻿namespace PhotoAlbum.DataAccess.Models.QueryParameters
{
    /// <summary>
    ///     Represents album photos query parameters.
    /// </summary>
    public sealed class DbGetAlbumPhotosQueryParameters : BaseQueryParameters
    {
        /// <summary>
        ///     Gets or sets album identifier..
        /// </summary>
        public int AlbumId { get; set; }

        /// <summary>
        ///     Gets or sets requesting user roles.
        /// </summary>
        public string[] RequestingUserRoles { get; set; }

        /// <summary>
        ///     Gets or sets photos pagination query parameters.
        /// </summary>
        public DbPaginationQueryParameters PhotosPaginationQueryParameters { get; set; }
    }
}
