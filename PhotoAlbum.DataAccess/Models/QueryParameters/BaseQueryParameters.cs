﻿namespace PhotoAlbum.DataAccess.Models.QueryParameters
{
    /// <summary>
    ///     Represents base query parameters.
    /// </summary>
    public abstract class BaseQueryParameters
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        public string RequestingUserId { get; set; }
    }
}