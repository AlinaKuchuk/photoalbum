﻿namespace PhotoAlbum.DataAccess.Models.QueryParameters
{
    /// <summary>
    ///     Represents get full user query parameters.
    /// </summary>
    public sealed class DbGetFullUserQueryParameters : BaseQueryParameters
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets requesting user roles.
        /// </summary>
        public string[] RequestingUserRoles { get; set; }

        /// <summary>
        ///     Gets or sets photos pagination query parameters.
        /// </summary>
        public DbPaginationQueryParameters PhotosPaginationQueryParameters { get; set; }

        /// <summary>
        ///     Gets or sets maximum count of albums.
        /// </summary>
        public int MaximumAlbumsCount { get; set; } = 4;
    }
}