﻿namespace PhotoAlbum.DataAccess.Models.QueryParameters
{
    /// <summary>
    ///     Represents get user albums query parameters.
    /// </summary>
    public class DbGetUserAlbumsQueryParameters : BaseQueryParameters
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets requesting user roles.
        /// </summary>
        public string[] RequestingUserRoles { get; set; }

        /// <summary>
        ///     Gets or sets albums pagination query parameters.
        /// </summary>
        public DbPaginationQueryParameters AlbumsPaginationQueryParameters { get; set; }
    }
}
