﻿namespace PhotoAlbum.DataAccess.Models.QueryParameters
{
    /// <summary>
    ///     Represents pagination query parameters.
    /// </summary>
    public sealed class DbPaginationQueryParameters
    {
        /// <summary>
        ///     Gets or sets page number.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        ///     Gets or sets page size.
        /// </summary>
        public int PageSize { get; set; }
    }
}