﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.Services.Models.Album;
using PhotoAlbum.Services.Services;
using PhotoAlbum.Services.Test.TestData;
using Xunit;

namespace PhotoAlbum.Services.Test.Services
{
    /// <summary>
    ///     Contains unit test for the <see cref="AlbumService"/> class.
    /// </summary>
    public static class AlbumServiceTest
    {
        [Theory]
        [MemberData(
            nameof(AlbumServiceTestData.CreateAlbumCorrectData),
            MemberType = typeof(AlbumServiceTestData))]
        public static async Task CreateAlbumAsync_SuccessfullyCreatesAlbumAsync(
            CreateAlbumDto createAlbumDto,
            Album albumEntity,
            string requestingUserId)

        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var albumService = new AlbumService(
                mockUnitOfWork.Object,
                mockMapper.Object);

            mockUnitOfWork.Setup(uow => uow.AlbumRepository.Add(
                It.Is<Album>(album => album.Title == createAlbumDto.Title
                                      && album.Description == createAlbumDto.Description)));
            mockMapper.Setup(mapper => mapper.Map<Album>(It.IsAny<CreateAlbumDto>()))
                .Returns(albumEntity);
            mockMapper.Setup(mapper => mapper.Map<AlbumDto>(It.IsAny<Album>()))
                .Returns(new AlbumDto());

            await albumService.CreateAlbumAsync(createAlbumDto, requestingUserId);

            mockUnitOfWork.Verify(
                uow => uow.AlbumRepository.Add(It.IsAny<Album>()),
                Times.Once);
            mockMapper.Verify(
                mapper => mapper.Map<AlbumDto>(It.IsAny<Album>()),
                Times.Once);
            mockMapper.Verify(
                mapper => mapper.Map<Album>(It.IsAny<CreateAlbumDto>()),
                Times.Once);
        }

        [Theory]
        [MemberData(
            nameof(AlbumServiceTestData.GetAlbumCorrectData),
            MemberType = typeof(AlbumServiceTestData))]
        public static async Task GetAlbumAsync_SuccessfullyReturnsAlbumAsync(
            int albumId,
            Album returnedAlbum)
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var albumService = new AlbumService(
                mockUnitOfWork.Object,
                mockMapper.Object);

            mockUnitOfWork.Setup(
                    uow => uow.AlbumRepository.GetFiltered(
                        It.IsAny<Expression<Func<Album, bool>>>()))
                .Returns(new[] {returnedAlbum});
            mockMapper.Setup(
                mapper => mapper.Map<AlbumDto>(It.IsAny<Album>()));

            await albumService.GetAlbumAsync(albumId);

            mockUnitOfWork.Verify(
                uow => uow.AlbumRepository.GetFiltered(
                    It.IsAny<Expression<Func<Album, bool>>>()),
                Times.Once);
            mockMapper.Verify(
                mapper => mapper.Map<AlbumDto>(It.IsAny<Album>()),
                Times.Once);
        }
    }
}