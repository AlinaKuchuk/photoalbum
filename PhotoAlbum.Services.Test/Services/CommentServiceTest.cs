﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.Services.Exceptions;
using PhotoAlbum.Services.Models.Comments;
using PhotoAlbum.Services.Services;
using Xunit;

namespace PhotoAlbum.Services.Test.Services
{
    public static class CommentServiceTest
    {
        [Fact]
        public static Task CreateCommentAsync_PassesNotExistingPostId_ThrowsNotFoundExceptionAsync()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var commentService = new CommentService(
                mockUnitOfWork.Object,
                mockMapper.Object);

            mockUnitOfWork.Setup(
                    uow => uow.PostRepository.GetByIdAsync(
                        It.IsAny<int>(),
                        It.IsAny<Expression<Func<Post, object>>[]>()))
                .ReturnsAsync(() => null);

            return Assert.ThrowsAsync<NotFoundException>(() => commentService.CreateCommentAsync(
                new CreateCommentDto
                {
                    PostId = 1
                }, 
                Guid.NewGuid().ToString()));
        }
    }
}