﻿using System.Threading.Tasks;
using AutoMapper;
using Moq;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Album;
using PhotoAlbum.Services.Services;
using PhotoAlbum.Services.Test.TestData;
using Xunit;

namespace PhotoAlbum.Services.Test.Services
{
    public static class ExceptionDataServiceTest
    {
        [Theory]
        [MemberData(
            nameof(ExceptionDataServiceTestData.SaveExceptionDataCorrectData),
            MemberType = typeof(ExceptionDataServiceTestData))]
        public static async Task SaveExceptionDataAsync_SuccessfullySavesAsync(
            CreateExceptionDataDto createExceptionData)
        {
            var mockMapper = new Mock<IMapper>();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var exceptionService = new ExceptionDataService(
                mockMapper.Object,
                mockUnitOfWork.Object);

            mockUnitOfWork.Setup(
                uow => uow.ExceptionDataRepository.Add(It.IsAny<ExceptionData>()));
            mockMapper.Setup(
                mapper => mapper.Map<ExceptionData>(It.IsAny<CreateAlbumDto>()));

            await exceptionService.SaveExceptionDataAsync(createExceptionData);

            mockUnitOfWork.Verify(
                uow => uow.ExceptionDataRepository.Add(It.IsAny<ExceptionData>()), Times.Once);
            mockMapper.Verify(
                mapper => mapper.Map<ExceptionData>(It.IsAny<CreateExceptionDataDto>()),
                Times.Once);
        }
    }
}