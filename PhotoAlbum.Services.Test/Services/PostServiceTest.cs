﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.Enums;
using PhotoAlbum.DataAccess.Models.QueryParameters;
using PhotoAlbum.Services.Exceptions;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Post;
using PhotoAlbum.Services.Services;
using PhotoAlbum.Services.Test.TestData;
using Xunit;

namespace PhotoAlbum.Services.Test.Services
{
    /// <summary>
    ///     Contains unit test for the <see cref="PostService"/> class.
    /// </summary>
    public static class PostServiceTest
    {
        [Theory]
        [MemberData(
            nameof(PostServiceTestData.GetFollowingUpdatesAsyncData),
            MemberType = typeof(PostServiceTestData))]
        internal static async Task GetFollowingUpdatesAsync_SuccessfullyReturnsPostsAsync(
            GetFollowingPostsQueryParametersDto getFollowingPostsQueryParameters,
            DbPaginationData<PreviewPostAggregatedModel> paginatedPreviewPostAggregatedModels,
            PaginatedDataDto<PreviewPostAggregatedDto> paginatedPreviewPostAggregatedDtoModels)
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var mockFileStorage = new Mock<IFileStorage>();
            var postService = new PostService(mockFileStorage.Object, mockUnitOfWork.Object, mockMapper.Object);

            mockUnitOfWork.Setup(
                uow => uow.PostRepository.GetFollowingUpdatesAsync(
                    getFollowingPostsQueryParameters.SearchText,
                    getFollowingPostsQueryParameters.RequestingUserId,
                    It.IsAny<DbPaginationQueryParameters>(),
                    It.IsAny<string>(),
                    It.IsAny<SortOrder>()))
                .ReturnsAsync(paginatedPreviewPostAggregatedModels);

            mockMapper.Setup(mapper =>
                    mapper.Map<PaginatedDataDto<PreviewPostAggregatedDto>>(
                        It.IsAny<DbPaginationData<PreviewPostAggregatedModel>>()))
                .Returns(paginatedPreviewPostAggregatedDtoModels);

            var returnedPosts = await postService.GetFollowingUpdatesAsync(
                getFollowingPostsQueryParameters);

            mockUnitOfWork.Verify(
                uow => uow.PostRepository.GetFollowingUpdatesAsync(
                    getFollowingPostsQueryParameters.SearchText,
                    getFollowingPostsQueryParameters.RequestingUserId,
                    It.IsAny<DbPaginationQueryParameters>(),
                    It.IsAny<string>(),
                    It.IsAny<SortOrder>()),
                Times.Once);

            mockMapper.Verify(mapper =>
                mapper.Map<PaginatedDataDto<PreviewPostAggregatedDto>>(
                    It.IsAny<DbPaginationData<PreviewPostAggregatedModel>>()),
                Times.Once);

            Assert.Equal(paginatedPreviewPostAggregatedModels.Items.Count(), returnedPosts.Items.Count());
            Assert.Equal(paginatedPreviewPostAggregatedModels.CurrentPage, returnedPosts.CurrentPage);
            Assert.Equal(paginatedPreviewPostAggregatedModels.TotalPages, returnedPosts.TotalPages);
        }

        [Theory]
        [MemberData(
            nameof(PostServiceTestData.GetExploreUpdatesData),
            MemberType = typeof(PostServiceTestData))]
        internal static async Task GetExplorePostAsync_SuccessfullyReturnsPostsAsync (
            GetFollowingPostsQueryParametersDto getFollowingPostsQueryParameters,
            DbPaginationData<PreviewPostAggregatedModel> paginatedPreviewPostAggregatedModels,
            PaginatedDataDto<PreviewPostAggregatedDto> paginatedPreviewPostAggregatedDtoModels)
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var mockFileStorage = new Mock<IFileStorage>();
            var postService = new PostService(mockFileStorage.Object, mockUnitOfWork.Object, mockMapper.Object);

            mockUnitOfWork.Setup(
                uow => uow.PostRepository.GetExplorePostsAsync(
                    getFollowingPostsQueryParameters.SearchText,
                    getFollowingPostsQueryParameters.RequestingUserId,
                    It.IsAny<DbPaginationQueryParameters>(),
                    It.IsAny<string>(),
                    It.IsAny<SortOrder>()))
                .ReturnsAsync(paginatedPreviewPostAggregatedModels);

            mockMapper.Setup(mapper =>
                    mapper.Map<PaginatedDataDto<PreviewPostAggregatedDto>>(
                        It.IsAny<DbPaginationData<PreviewPostAggregatedModel>>()))
                .Returns(paginatedPreviewPostAggregatedDtoModels);

            var returnedPosts = await postService.GetExplorePostsAsync(
                getFollowingPostsQueryParameters);

            mockUnitOfWork.Verify(
                uow => uow.PostRepository.GetExplorePostsAsync(
                    getFollowingPostsQueryParameters.SearchText,
                    getFollowingPostsQueryParameters.RequestingUserId,
                    It.IsAny<DbPaginationQueryParameters>(),
                    It.IsAny<string>(),
                    It.IsAny<SortOrder>()),
                Times.Once);

            mockMapper.Verify(mapper =>
                mapper.Map<PaginatedDataDto<PreviewPostAggregatedDto>>(
                    It.IsAny<DbPaginationData<PreviewPostAggregatedModel>>()),
                Times.Once);

            Assert.Equal(paginatedPreviewPostAggregatedModels.Items.Count(), returnedPosts.Items.Count());
            Assert.Equal(paginatedPreviewPostAggregatedModels.CurrentPage, returnedPosts.CurrentPage);
            Assert.Equal(paginatedPreviewPostAggregatedModels.TotalPages, returnedPosts.TotalPages);
        }

        [Fact]
        internal static Task LikePostByUserAsync_ThrowsNotFoundExceptionAsync()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var mockFileStorage = new Mock<IFileStorage>();
            var postService = new PostService(mockFileStorage.Object, mockUnitOfWork.Object, mockMapper.Object);
            const int postId = 1;
            var userId = Guid.NewGuid().ToString();

            mockUnitOfWork.Setup(
                uow => uow.PostRepository.GetByIdAsync(
                    postId,
                    It.IsAny<Expression<Func<Post, object>>>(),
                    It.IsAny<Expression<Func<Post, object>>>()))
                .ReturnsAsync(() => null);

            return  Assert.ThrowsAsync<NotFoundException>(() => postService.LikePostByUserAsync(userId, postId));
        }

        [Fact]
        internal static async Task LikePostByUserAsync_ThrowsPrivateResourceAccessingExceptionWhenAlbumPrivateAsync()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var mockFileStorage = new Mock<IFileStorage>();
            var postService = new PostService(mockFileStorage.Object, mockUnitOfWork.Object, mockMapper.Object);
            const int postId = 1;
            var userId = Guid.NewGuid().ToString();
            var privateAlbumPost = new Post
            {
                Id = postId,
                Album = new Album
                {
                    IsPrivate = true
                }
            };

            mockUnitOfWork.Setup(
                uow => uow.PostRepository.GetByIdAsync(
                    postId,
                    It.IsAny<Expression<Func<Post, object>>[]>())) 
                .ReturnsAsync(privateAlbumPost);

            await Assert.ThrowsAsync<PrivateResourceAccessingException>(() => postService.LikePostByUserAsync(userId, postId));

            mockUnitOfWork.Verify(
                uow => uow.PostRepository.GetByIdAsync(
                    postId,
                    It.IsAny<Expression<Func<Post, object>>[]>()));
        }

        [Fact]
        internal static async Task LikePostByUserAsync_ThrowsPrivateResourceAccessingExceptionWhenOwnerPrivateAsync()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var mockFileStorage = new Mock<IFileStorage>();
            var postService = new PostService(mockFileStorage.Object, mockUnitOfWork.Object, mockMapper.Object);
            const int postId = 1;
            var userId = Guid.NewGuid().ToString();
            var privateAlbumPost = new Post
            {
                Id = postId,
                Album = new Album
                {
                    Owner = new User
                    {
                        IsPrivate = true
                    }
                }
            };

            mockUnitOfWork.Setup(
                uow => uow.PostRepository.GetByIdAsync(
                    postId,
                    It.IsAny<Expression<Func<Post, object>>[]>())) 
                .ReturnsAsync(privateAlbumPost);

            await Assert.ThrowsAsync<PrivateResourceAccessingException>(() => postService.LikePostByUserAsync(userId, postId));

            mockUnitOfWork.Verify(
                uow => uow.PostRepository.GetByIdAsync(
                    postId,
                    It.IsAny<Expression<Func<Post, object>>[]>()), 
                Times.Once);
        }

        [Fact]
        internal static async Task LikePostByUserAsync_SuccessfullyUpdatesAsync ()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            var mockFileStorage = new Mock<IFileStorage>();
            var postService = new PostService(mockFileStorage.Object, mockUnitOfWork.Object, mockMapper.Object);
            const int postId = 1;
            var userId = Guid.NewGuid().ToString();
            var privateAlbumPost = new Post
            {
                Id = postId,
                Album = new Album
                {
                    Owner = new User
                    {
                        IsPrivate = false 
                    }
                }
            };

            mockUnitOfWork.Setup(
                uow => uow.PostRepository.GetByIdAsync(
                    postId,
                    It.IsAny<Expression<Func<Post, object>>[]>())) 
                .ReturnsAsync(privateAlbumPost);
            
            mockUnitOfWork.Setup(
                uow => uow.LikeRepository.Add(
                    It.IsAny<Like>()));

            await postService.LikePostByUserAsync(userId, postId);

            mockUnitOfWork.Verify(
                uow => uow.PostRepository.GetByIdAsync(
                    postId,
                    It.IsAny<Expression<Func<Post, object>>[]>()), 
                Times.Once);

            mockUnitOfWork.Verify(
                uow => uow.LikeRepository.Add(
                    It.IsAny<Like>()),
                Times.Once);
        }
    }
}