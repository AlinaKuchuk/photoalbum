﻿using System.Collections.Generic;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.Services.Models.Album;
using PhotoAlbum.Services.Test.Services;

namespace PhotoAlbum.Services.Test.TestData
{
    /// <summary>
    ///     Contains  unit tests for the <see cref="AlbumServiceTest" /> class.
    /// </summary>
    public static class AlbumServiceTestData
    {
        /// <summary>
        ///     Gets test data for the create album method.
        /// </summary>
        public static IEnumerable<object[]> CreateAlbumCorrectData = new[]
        {
            new object[]
            {
                new CreateAlbumDto
                {
                    Description = TestDataHelper.GenerateBase64String(),
                    Title = TestDataHelper.GenerateBase64String(),
                    IsPrivate = false
                },
                new Album
                {
                    OwnerId = "d552d870-ca55-44e2-bf67-f513a80ae6dd"
                },
                "d552d870-ca55-44e2-bf67-f513a80ae6dd"
            }
        };

        public static IEnumerable<object[]> GetAlbumCorrectData =
            new[]
            {
                new object[]
                {
                    1,
                    new Album
                    {
                        Id = 1
                    }
                }
            };
    }
}