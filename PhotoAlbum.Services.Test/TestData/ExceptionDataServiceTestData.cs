﻿using System.Collections.Generic;
using PhotoAlbum.Services.Models;

namespace PhotoAlbum.Services.Test.TestData
{
    public static class ExceptionDataServiceTestData
    {
        public static IEnumerable<object[]>  SaveExceptionDataCorrectData =
            new[]
            {
                new object[]
                {
                    new CreateExceptionDataDto
                    {
                        UserPrincipalId = TestDataHelper.GenerateBase64String(),
                        Url = TestDataHelper.GenerateBase64String(),
                        StackTrace = TestDataHelper.GenerateBase64String() 
                    }
                }
            };
    }
}