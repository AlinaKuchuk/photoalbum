﻿using System.Collections.Generic;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Post;
using PhotoAlbum.Services.Test.Services;

namespace PhotoAlbum.Services.Test.TestData
{
    /// <summary>
    ///     Contains test data for the <see cref="PostServiceTest" /> class.
    /// </summary>
    public static class PostServiceTestData
    {
        public static IEnumerable<object[]> GetFollowingUpdatesAsyncData =
            new[]
            {
                new object[]
                {
                    new GetFollowingPostsQueryParametersDto
                    {
                        SearchText = TestDataHelper.GenerateBase64String(),
                        PaginationQueryParameters = new GetPaginationQueryParametersDto
                        {
                            PageNumber = 2,
                            PageSize = 1
                        },
                        Sort = "date:asc"
                    },
                    new DbPaginationData<PreviewPostAggregatedModel>
                    {
                        Items = new[]
                        {
                            new PreviewPostAggregatedModel()
                        },
                        CurrentPage = 2,
                        TotalPages = 10
                    },
                    new PaginatedDataDto<PreviewPostAggregatedDto>
                    {
                        CurrentPage = 2,
                        TotalPages = 10,
                        Items = new[]
                        {
                            new PreviewPostAggregatedDto()
                        }
                    }
                }
            };

        public static IEnumerable<object[]> GetExploreUpdatesData =
            new[]
            {
                new object[]
                {
                    new GetFollowingPostsQueryParametersDto
                    {
                        SearchText = TestDataHelper.GenerateBase64String(),
                        PaginationQueryParameters = new GetPaginationQueryParametersDto
                        {
                            PageNumber = 2,
                            PageSize = 1
                        },
                        Sort = "date:asc"
                    },
                    new DbPaginationData<PreviewPostAggregatedModel>
                    {
                        Items = new[]
                        {
                            new PreviewPostAggregatedModel()
                        },
                        CurrentPage = 2,
                        TotalPages = 10
                    },
                    new PaginatedDataDto<PreviewPostAggregatedDto>
                    {
                        CurrentPage = 2,
                        TotalPages = 10,
                        Items = new[]
                        {
                            new PreviewPostAggregatedDto()
                        }
                    }
                }
            };
    }
}