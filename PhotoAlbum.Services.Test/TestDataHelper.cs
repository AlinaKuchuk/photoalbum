﻿using System;

namespace PhotoAlbum.Services.Test
{
    /// <summary>
    ///     Contains method for test data generating.
    /// </summary>
    public static class TestDataHelper
    {
        /// <summary>
        /// Generates random base64 string.
        /// </summary>
        /// <returns></returns>
        public static string GenerateBase64String()
        {
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray());
        }
    }
}