﻿using System.Threading.Tasks;
using Moq;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Repositories;
using Xunit;

namespace PhotoAlbum.DataAccess.Tests.Repositories
{
    /// <summary>
    /// Contains unit tests for the <see cref="GenericRepository{TEntity,TKey}"/> class.
    /// </summary>
    public static class GenericRepositoryTests
    {
        [Fact]
        internal static void Add_VerifyContextAddMethodIsCalled()
        {
            var mockPhotoAlbumContext = new Mock<IPhotoAlbumContext>();
            var genericRepository = new GenericRepository<TestEntity, int>(
                mockPhotoAlbumContext.Object);

            mockPhotoAlbumContext.Setup(
                    context => context.Set<TestEntity>().Add(It.IsNotNull<TestEntity>()))
                .Returns<TestEntity>(entity => entity);

            genericRepository.Add(new TestEntity());

            mockPhotoAlbumContext.Verify(
                context => context.Set<TestEntity>().Add(It.IsNotNull<TestEntity>()),
                Times.Once);
        }

        [Fact]
        internal static void Delete_VerifyContextRemoveMethodIsCalled()
        {
            var mockPhotoAlbumContext = new Mock<IPhotoAlbumContext>();
            var genericRepository = new GenericRepository<TestEntity, int>(
                mockPhotoAlbumContext.Object);

            mockPhotoAlbumContext.Setup(
                    context => context.Set<TestEntity>().Remove(It.IsNotNull<TestEntity>()))
                .Returns<TestEntity>(entity => entity);

            genericRepository.Delete(new TestEntity());

            mockPhotoAlbumContext.Verify(
                context => context.Set<TestEntity>().Remove(It.IsNotNull<TestEntity>()),
                Times.Once);
        }
    }
}