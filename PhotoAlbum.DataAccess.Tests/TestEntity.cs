﻿using PhotoAlbum.DataAccess.Entities;

namespace PhotoAlbum.DataAccess.Tests
{
    /// <summary>
    /// Represents entity for testing.
    /// </summary>
    public class TestEntity : BaseEntity<int>
    {
    }
}