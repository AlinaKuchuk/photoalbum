﻿using System;

namespace PhotoAlbum.Models
{
    /// <summary>
    ///     Represents successfully completed action view model.
    /// </summary>
    public sealed class SuccessfullyCompletedViewModel
    {
        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets return URI.
        /// </summary>
        public string ReturnUri { get; set; }
    }
}