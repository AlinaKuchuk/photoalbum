﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Represents full post query parameters.
    /// </summary>
    public sealed class GetFullPostQueryParameters
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(PostDetailedResources),
            ErrorMessageResourceName = "PostIdRequired")]
        public int? PostId { get; set; }

        /// <summary>
        ///     Gets or sets page number.
        /// </summary>
        [Range(0, int.MaxValue,
            ErrorMessageResourceType = typeof(PostDetailedResources),
            ErrorMessageResourceName = "PageNumberRange")]
        public int? PageNumber { get; set; } = 0;

        /// <summary>
        ///     Gets or sets page size.
        /// </summary>
        [Range(1, 21,
            ErrorMessageResourceType = typeof(PostDetailedResources),
            ErrorMessageResourceName = "PageSizeRange")]
        public int? PageSize { get; set; } = 10;
    }
}