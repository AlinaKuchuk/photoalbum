﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Models.Shared;
using PhotoAlbum.Models.User;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Represents full post information view model.
    /// </summary>
    public sealed class FullPostViewModel
    {
        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        [Display(Name = "Description", ResourceType = typeof(PostDetailedResources))]
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets number of likes.
        /// </summary>
        public int LikeCount { get; set; }

        /// <summary>
        ///     Gets or sets post author.
        /// </summary>
        public AuthorViewModel Author { get; set; }

        /// <summary>
        ///     Gets or sets photos in the post.
        /// </summary>
        public IList<PhotoMetadataPreviewViewModel> Photos { get; set; }

        /// <summary>
        ///     Gets or sets comments.
        /// </summary>
        public PaginatedViewModel<CommentViewModel> Comments { get; set; }

        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        [Display(Name = "Album", ResourceType = typeof(PostDetailedResources))]
        public AlbumPreviewViewModel Album { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is the post liked by requesting user or not.
        /// </summary>
        public bool IsLiked { get; set; }

        /// <summary>
        ///     Gets or sets created date-time.
        /// </summary>
        public DateTime CreateDateTime { get; set; }
    }
}