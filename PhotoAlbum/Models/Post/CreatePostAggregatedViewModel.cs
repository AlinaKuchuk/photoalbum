﻿using PhotoAlbum.Models.User;

namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Represents creating post aggregated view model.
    /// </summary>
    public sealed class CreatePostAggregatedViewModel
    {
        /// <summary>
        ///     Gets or sets post creating form.
        /// </summary>
        public CreatePostViewModel CreatePost { get; set; }

        /// <summary>
        ///     Gets or sets album preview.
        /// </summary>
        public AlbumPreviewViewModel[] Albums { get; set; }
    }
}