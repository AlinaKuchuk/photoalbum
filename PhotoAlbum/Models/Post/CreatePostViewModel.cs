﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using PhotoAlbum.Infrastructure.Attributes;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Represents creating post view model.
    /// </summary>
    public sealed class CreatePostViewModel
    {
        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        [StringLength(
            ValidationConstants.ValidationConstants.CreatePost.DescriptionMaximumLength,
            ErrorMessageResourceType = typeof(CreatePostResources),
            ErrorMessageResourceName = "DescriptionMaximumLength")]
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets location.
        /// </summary>
        [StringLength(
            ValidationConstants.ValidationConstants.CreatePost.LocationMaximumLength,
            ErrorMessageResourceType = typeof(CreatePostResources),
            ErrorMessageResourceName = "LocationMaximumLength")]
        public string Location { get; set; }

        /// <summary>
        ///     Gets or sets album identifier.
        /// </summary>
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(CreatePostResources),
            ErrorMessageResourceName = "AlbumIdRequired")]
        public int? AlbumId { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(CreatePostResources),
            ErrorMessageResourceName = "PhotoRequired")]
        [MaxLength(3)]
        [MinLength(1)]
        [NoNullElements]
        public IEnumerable<HttpPostedFileBase> Photos { get; set; }
    }
}