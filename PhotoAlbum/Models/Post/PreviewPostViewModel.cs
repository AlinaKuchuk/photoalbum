﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhotoAlbum.Models.User;

namespace PhotoAlbum.Models.Post
{
    public class PreviewPostViewModel
    {
        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public IEnumerable<PhotoMetadataPreviewViewModel> Photos { get; set; }

        /// <summary>
        ///     Gets or sets created date-time.
        /// </summary>
        public DateTime CreateDateTime { get; set; }
    }
}