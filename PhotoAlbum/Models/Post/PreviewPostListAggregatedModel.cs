﻿namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Represents preview post list aggregated model.
    /// </summary>
    public class PreviewPostListAggregatedModel
    {
        /// <summary>
        ///     Gets or sets query parameters.
        /// </summary>
        public GetPostsListQueryParameters QueryParameters { get; set; }

        /// <summary>
        ///     Gets or sets 
        /// </summary>
        public PaginatedViewModel<PreviewPostAggregatedViewModel> Posts { get; set; }
    }
}