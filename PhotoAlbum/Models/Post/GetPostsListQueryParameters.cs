﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Represents get explore posts query parameters.
    /// </summary>
    public sealed class GetPostsListQueryParameters
    {
        /// <summary>
        ///     Gets or sets sort order.
        /// </summary>
        [RegularExpression("(date|like):(asc|desc)", 
            ErrorMessageResourceType = typeof(PostListResources),
            ErrorMessageResourceName = "IncorrectSorting")]
        public string SortOrder { get; set; } = "date:desc";

        /// <summary>
        ///     Gets or sets test to search.
        /// </summary>
        [StringLength(
            ValidationConstants.ValidationConstants.FilterPost.SearchTextMaximumLength,
            ErrorMessageResourceType = typeof(PostListResources),
            ErrorMessageResourceName = "SearchTextMaxLenght")]
        public string SearchText { get; set; }

        /// <summary>
        ///     Gets or sets page number.
        /// </summary>
        [Range(0, int.MaxValue,
            ErrorMessageResourceType = typeof(PostListResources),
            ErrorMessageResourceName = "PageNumberRange")]
        public int? PageNumber { get; set; } = 0;

        /// <summary>
        ///     Gets or sets page size.
        /// </summary>
        [Range(1, 21,
            ErrorMessageResourceType = typeof(PostListResources),
            ErrorMessageResourceName = "PageSizeRange")]
        public int? PageSize { get; set; } = 5;
    }
}