﻿namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Represents comment view model.
    /// </summary>
    public sealed class CommentViewModel
    {
        /// <summary>
        ///     Gets or sets comment identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets author identifier.
        /// </summary>
        public string AuthorId { get; set; }

        /// <summary>
        ///     Gets or sets author first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets author last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets content.
        /// </summary>
        public string Content { get; set; }
    }
}