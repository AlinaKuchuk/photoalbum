﻿namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Gets or sets post statistic view model.
    /// </summary>
    public sealed class PostStatisticViewModel
    {
        /// <summary>
        ///     Gets or sets views count.
        /// </summary>
        public int ViewsCount { get; set; }

        /// <summary>
        ///     Gets or sets likes count.
        /// </summary>
        public int LikesCount { get; set; }
    }
}