﻿using System.Collections.Generic;
using PhotoAlbum.Models.User;

namespace PhotoAlbum.Models.Post
{
    /// <summary>
    ///     Represents preview post aggregated data-transfer object.
    /// </summary>
    public sealed class PreviewPostAggregatedViewModel
    {
        /// <summary>
        ///     Gets or sets author.
        /// </summary>
        public UserViewModel Author { get; set; }

        /// <summary>
        ///     Gets or sets post.
        /// </summary>
        public PreviewPostViewModel Post { get; set; }

        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        public AlbumPreviewViewModel Album { get; set; }

        /// <summary>
        ///     Gets or set number of likes.
        /// </summary>
        public int LikesCount { get; set; }

        /// <summary>
        ///     Gets or set comments.
        /// </summary>
        public IEnumerable<CommentViewModel> Comments { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is post liked by requesting user or not.
        /// </summary>
        public bool IsLiked { get; set; }
    }
}