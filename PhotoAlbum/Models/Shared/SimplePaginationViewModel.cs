﻿namespace PhotoAlbum.Models.Shared
{
    /// <summary>
    ///     Represents pagination view model.
    /// </summary>
    public sealed class SimplePaginationViewModel
    {
        /// <summary>
        ///     Gets or sets number of total pages.
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        ///     Gets or sets current pages.
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        ///     Gets or sets next pages url.
        /// </summary>
        public string NextUrl { get; set; }

        /// <summary>
        ///     Gets or sets previous pages url.
        /// </summary>
        public string PreviousUrl { get; set; }
    }
}