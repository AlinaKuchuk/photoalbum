﻿using System;

namespace PhotoAlbum.Models.Shared
{
    /// <summary>
    /// Represents post author view model.
    /// </summary>
    public sealed class AuthorViewModel
    {
        /// <summary>
        /// Gets or sets author identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets profile photo identifier.
        /// </summary>
        public Guid? ProfilePhotoId { get; set; }
    }
}