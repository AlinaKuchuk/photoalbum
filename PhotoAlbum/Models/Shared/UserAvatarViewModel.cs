﻿using System;

namespace PhotoAlbum.Models.Shared
{
    /// <summary>
    ///     Represents user avatar view model.
    /// </summary>
    public sealed class UserAvatarViewModel 
    {
        /// <summary>
        ///     Gets or sets photo identifier.
        /// </summary>
        public Guid? PhotoId { get; set; }
    }
}