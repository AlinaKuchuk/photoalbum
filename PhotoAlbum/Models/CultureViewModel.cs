﻿namespace PhotoAlbum.Models
{
    /// <summary>
    ///     Represents culture view model.
    /// </summary>
    public sealed class CultureViewModel
    {
        /// <summary>
        ///     Gets or sets two-letter name.
        /// </summary>
        public string TwoLetterName { get; set; }

        /// <summary>
        ///     Gets or sets full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        ///     Gets or sets image,
        /// </summary>
        public string Image { get; set; }
    }
}