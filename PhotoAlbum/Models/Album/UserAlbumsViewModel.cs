﻿using PhotoAlbum.Models.Shared;
using PhotoAlbum.Models.User;

namespace PhotoAlbum.Models.Album
{
    /// <summary>
    ///     Represents user albums view model.
    /// </summary>
    public class UserAlbumsViewModel
    {
        /// <summary>
        ///     Gets or sets owner identifier.
        /// </summary>
        public AuthorViewModel Owner { get; set; }

        /// <summary>
        ///     Gets or sets albums.
        /// </summary>
        public PaginatedViewModel<AlbumPreviewViewModel> Albums { get; set; }
    }
}