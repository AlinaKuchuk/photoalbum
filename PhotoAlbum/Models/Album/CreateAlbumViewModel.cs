﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Album
{
    /// <summary>
    ///     Represents creating album view model.
    /// </summary>
    public sealed class CreateAlbumViewModel
    {
        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        [Display(Name = "NewAlbumDescription", ResourceType = typeof(CreateAlbumResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(CreateAlbumResources),
            ErrorMessageResourceName = "DescriptionRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.CreateAlbum.DescriptionMaximumLength,
            ErrorMessageResourceType = typeof(CreateAlbumResources),
            ErrorMessageResourceName = "DescriptionMaximumLength")]
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        [Display(Name = "NewAlbumTitle", ResourceType = typeof(CreateAlbumResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(CreateAlbumResources),
            ErrorMessageResourceName = "TitleRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.CreateAlbum.TitleMaximumLength,
            ErrorMessageResourceType = typeof(CreateAlbumResources),
            ErrorMessageResourceName = "TitleMaximumLength")]
        public string Title { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is album private or not.
        /// </summary>
        [Display(Name = "NewAlbumIsPrivate", ResourceType = typeof(CreateAlbumResources))]
        public bool IsPrivate { get; set; }
    }
}