﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Models.User;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Album
{
    /// <summary>
    ///     Represents album photos view model.
    /// </summary>
    public sealed class FullAlbumViewModel
    {
        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        public AlbumPreviewViewModel Album { get; set; }

        /// <summary>
        ///     Gets or sets number of post in the album.
        /// </summary>
        [Display(ResourceType = typeof(AlbumPhotosResources), Name = "PostCount")]
        public int PostCount { get; set; }

        /// <summary>
        ///     Gets or sets number of photos in the album.
        /// </summary>
        [Display(ResourceType = typeof(AlbumPhotosResources), Name = "PhotosCount")]
        public int PhotosCount { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public PaginatedViewModel<PhotoMetadataPreviewViewModel> Photos { get; set; }
    }
}