﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Album
{
    /// <summary>
    ///     Represents album photos query parameters.
    /// </summary>
    public class GetAlbumPhotosQueryParameters
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(UserAlbumsResources),
            ErrorMessageResourceName = "AlbumIdRequired")]
        public int? AlbumId { get; set; }

        /// <summary>
        ///     Gets or sets page number.
        /// </summary>
        [Range(0, int.MaxValue,
            ErrorMessageResourceType = typeof(UserAlbumsResources),
            ErrorMessageResourceName = "PageNumberRange")]
        public int? PageNumber { get; set; } = 0;

        /// <summary>
        ///     Gets or sets page size.
        /// </summary>
        [Range(1, 51,
            ErrorMessageResourceType = typeof(UserAlbumsResources),
            ErrorMessageResourceName = "PageSizeRange")]
        public int? PageSize { get; set; } = 40;
    }
}