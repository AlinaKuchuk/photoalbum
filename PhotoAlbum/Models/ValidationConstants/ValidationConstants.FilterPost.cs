﻿using PhotoAlbum.Models.Post;

namespace PhotoAlbum.Models.ValidationConstants
{
    /// <summary>
    /// Contains validation constants. 
    /// </summary>
    internal static partial class ValidationConstants
    {
        /// <summary>
        /// Contains validation constants for the <see cref="GetPostsListQueryParameters"/> class.
        /// </summary>
        internal static class FilterPost
        {
            /// <summary>
            /// Gets or sets search text maximum length.
            /// </summary>
            internal const int SearchTextMaximumLength = 30;
        }
    }
}