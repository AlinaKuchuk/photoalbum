﻿using PhotoAlbum.Models.User;
using PhotoAlbum.Services.Models.Enums;
using System;
using System.Collections.Generic;

namespace PhotoAlbum.Models.ValidationConstants
{
    /// <summary>
    /// Contains validation constants. 
    /// </summary>
    internal static partial class ValidationConstants
    {
        /// <summary>
        /// Contains validation constants for the <see cref="UsersFilterViewModel"/> class.
        /// </summary>
        internal static class FilterUser
        {
            /// <summary>
            /// Gets user roles.
            /// </summary>
            public static IEnumerable<string> UserRoles = Enum.GetNames(typeof(RolesDto));

            /// <summary>
            /// Gets user maximum length.
            /// </summary>
            public const int UserNameMaximumLength = 30;
        }
    }
}