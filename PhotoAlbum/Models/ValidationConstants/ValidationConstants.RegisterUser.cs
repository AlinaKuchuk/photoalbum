﻿using PhotoAlbum.Models.Account;

namespace PhotoAlbum.Models.ValidationConstants
{
    /// <summary>
    /// Contains validation constants. 
    /// </summary>
    internal static partial class ValidationConstants
    {
        /// <summary>
        /// Contains validation constants for the <see cref="RegisterUserViewModel"/> class.
        /// </summary>
        internal static class RegisterUser
        {
            /// <summary>
            /// Gets or sets first name maximum length.
            /// </summary>
            internal const int FirstNameMaximumLength = 300;

            /// <summary>
            /// Gets or sets last name maximum length.
            /// </summary>
            internal const int LastNameMaximumLength = 300;
        }
    }
}