﻿using PhotoAlbum.Models.Post;

namespace PhotoAlbum.Models.ValidationConstants
{
    internal static partial class ValidationConstants
    {
        /// <summary>
        /// Contains validation constants for the <see cref="CreatePostViewModel"/> class.
        /// </summary>
        internal static class CreatePost
        {
            /// <summary>
            ///     Gets description maximum length.
            /// </summary>
            internal const int DescriptionMaximumLength = 1000;

            /// <summary>
            ///     Gets Location maximum length.
            /// </summary>
            internal const int LocationMaximumLength = 300;
        }
    }
}