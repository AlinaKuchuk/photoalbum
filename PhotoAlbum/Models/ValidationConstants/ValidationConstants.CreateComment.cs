﻿using PhotoAlbum.Models.Comment;

namespace PhotoAlbum.Models.ValidationConstants
{
    internal static partial class ValidationConstants
    {
        /// <summary>
        ///     Contains validation constants for the <see cref="CreateCommentViewModel"/> class.
        /// </summary>
        internal static class CreateComment
        {
            /// <summary>
            ///     Gets comment content maximum length.
            /// </summary>
            internal const int ContentMaximumLength = 500;
        }
    }
}