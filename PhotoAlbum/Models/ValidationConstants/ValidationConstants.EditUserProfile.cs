﻿using PhotoAlbum.Models.User;

namespace PhotoAlbum.Models.ValidationConstants
{
    /// <summary>
    /// Contains validation constants. 
    /// </summary>
    internal static partial class ValidationConstants
    {
        /// <summary>
        /// Contains validation constants for the <see cref="EditUserProfileViewModel"/> class.
        /// </summary>
        internal static class EditUserProfile
        {
            /// <summary>
            ///     Gets biography maximum length.
            /// </summary>
            internal const int BiographyMaximumLength = 600;

            /// <summary>
            /// Gets or sets first name maximum length.
            /// </summary>
            internal const int FirstNameMaximumLength = 300;

            /// <summary>
            /// Gets or sets last name maximum length.
            /// </summary>
            internal const int LastNameMaximumLength = 300;
        }
    }
}