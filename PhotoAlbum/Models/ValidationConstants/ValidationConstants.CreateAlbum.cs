﻿using PhotoAlbum.Models.Album;

namespace PhotoAlbum.Models.ValidationConstants
{
    /// <summary>
    /// Contains validation constants. 
    /// </summary>
    internal static partial class ValidationConstants
    {
        /// <summary>
        /// Contains validation constants for the <see cref="CreateAlbumViewModel"/> class.
        /// </summary>
        internal static class CreateAlbum
        {
            /// <summary>
            ///     Gets title maximum length.
            /// </summary>
            internal const int TitleMaximumLength = 300;

            /// <summary>
            ///     Gets description maximum length.
            /// </summary>
            internal const int DescriptionMaximumLength = 1000;
        }
    }
}