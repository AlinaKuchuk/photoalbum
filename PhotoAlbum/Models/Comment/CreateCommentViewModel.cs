﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Comment
{
    /// <summary>
    ///     Represents comment view model.
    /// </summary>
    public sealed class CreateCommentViewModel
    {
        /// <summary>
        ///     Gets or sets content.
        /// </summary>
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(PostDetailedResources),
            ErrorMessageResourceName = "CommentContentRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.CreateComment.ContentMaximumLength,
            ErrorMessageResourceType = typeof(PostDetailedResources),
            ErrorMessageResourceName = "CommentContentMaxLength")]
        public string Content { get; set; }

        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(PostDetailedResources),
            ErrorMessageResourceName = "PostIdRequired")]
        public int? PostId { get; set; }
    }
}