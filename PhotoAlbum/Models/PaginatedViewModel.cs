﻿using System.Collections.Generic;

namespace PhotoAlbum.Models
{
    /// <summary>
    ///     Represents pagination view model.
    /// </summary>
    /// <typeparam name="TViewModel">Type of view model.</typeparam>
    public sealed class PaginatedViewModel<TViewModel>
        where TViewModel : class
    {
        /// <summary>
        ///     Gets or sets items.
        /// </summary>
        public IEnumerable<TViewModel> Items { get; set; }

        /// <summary>
        ///     Gets or sets page number.
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        ///     Gets or sets total pages.
        /// </summary>
        public int TotalPages { get; set; }
    }
}