﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Account
{
    /// <summary>
    ///     Represents register user view model.
    /// </summary>
    public sealed class RegisterUserViewModel
    {
        /// <summary>
        ///     Gets or sets email.
        /// </summary>
        [Display(Name = nameof(Email), ResourceType = typeof(RegisterResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "EmailRequired")]
        [EmailAddress(
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "EmailInvalid")]
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets first name.
        /// </summary>
        [Display(Name = nameof(FirstName), ResourceType = typeof(RegisterResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "FirstNameRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.RegisterUser.FirstNameMaximumLength,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "FirstNameLength")]
        public string FirstName { get; set; }


        /// <summary>
        ///     Gets or sets last name.
        /// </summary>
        [Display(Name = nameof(LastName), ResourceType = typeof(RegisterResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "LastNameRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.RegisterUser.LastNameMaximumLength,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "LastNameLength")]
        public string LastName { get; set; }

        /// <summary>
        ///     Get or sets password.
        /// </summary>
        [Display(Name = nameof(Password), ResourceType = typeof(RegisterResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "PasswordRequired")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        ///     Gets or sets password confirmation.
        /// </summary>
        [Display(Name = nameof(PasswordConfirm), ResourceType = typeof(RegisterResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "PasswordConfirmRequired")]
        [Compare(
            nameof(Password),
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "PasswordConfirmWrong")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}