﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.Account
{
    /// <summary>
    ///     Represents login view model.
    /// </summary>
    public sealed class LoginViewModel
    {
        /// <summary>
        ///     Get or sets email.
        /// </summary>
        [Display(Name = "LoginEmail", ResourceType = typeof(LoginResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(LoginResources),
            ErrorMessageResourceName = "LoginEmailRequired")]
        [EmailAddress(
            ErrorMessageResourceType = typeof(LoginResources),
            ErrorMessageResourceName = "EmailInvalid")]
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets password.
        /// </summary>
        [Display(Name = "LoginPassword", ResourceType = typeof(LoginResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(LoginResources),
            ErrorMessageResourceName = "LoginPasswordRequired")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}