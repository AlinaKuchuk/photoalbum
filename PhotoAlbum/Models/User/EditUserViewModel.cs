﻿using PhotoAlbum.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    /// Represents edit user view model.
    /// </summary>
    public sealed class EditUserViewModel
    {
        /// <summary>
        /// Gets or sets identifier.
        /// </summary>
        [HiddenInput]
        [Required(AllowEmptyStrings = false)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        [Display(Name = "Email", ResourceType = typeof(EditUserProfileResources))]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets first name.
        /// </summary>
        [Display(Name = "FirstName", ResourceType = typeof(EditUserProfileResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(EditUserProfileResources),
            ErrorMessageResourceName = "FirstNameRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.EditUserProfile.FirstNameMaximumLength,
            ErrorMessageResourceType = typeof(EditUserProfileResources),
            ErrorMessageResourceName = "FirstNameLength")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets last name.
        /// </summary>
        [Display(Name = "LastName", ResourceType = typeof(RegisterResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "LastNameRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.RegisterUser.LastNameMaximumLength,
            ErrorMessageResourceType = typeof(RegisterResources),
            ErrorMessageResourceName = "LastNameLength")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets selected roles.
        /// </summary>
        [Display(Name = "UserRoles", ResourceType = typeof(EditUserResources))]
        [MinLength(
            1,
            ErrorMessageResourceType = typeof(EditUserResources),
            ErrorMessageResourceName = "UserRolesMinLength")]
        public ICollection<string> SelectedRoles { get; set; }
    }
}