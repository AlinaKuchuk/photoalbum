﻿using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represents full user query parameter.
    /// </summary>
    public sealed class GetFullUserQueryParameters
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(UserDetailedResources),
            ErrorMessageResourceName = "UserIdRequired")]
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets page number.
        /// </summary>
        [Range(0, int.MaxValue, 
            ErrorMessageResourceType = typeof(UserDetailedResources),
            ErrorMessageResourceName = "PageNumberRange")]
        public int? PageNumber { get; set; } = 0;

        /// <summary>
        ///     Gets or sets page size.
        /// </summary>
        [Range(1, 51,
            ErrorMessageResourceType = typeof(UserDetailedResources),
            ErrorMessageResourceName = "PageSizeRange")]
        public int? PageSize { get; set; } = 21;
    }
}