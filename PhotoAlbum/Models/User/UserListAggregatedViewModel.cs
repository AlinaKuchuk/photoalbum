﻿using System.Collections.Generic;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represents user list aggregated view model.
    /// </summary>
    public class UserListAggregatedViewModel
    {
        /// <summary>
        /// Gets or sets user filter.
        /// </summary>
        public UsersFilterViewModel Filter { get; set; }

        /// <summary>
        /// Gets or sets users.
        /// </summary>
        public IEnumerable<UserViewModel> Users { get; set; }
    }
}