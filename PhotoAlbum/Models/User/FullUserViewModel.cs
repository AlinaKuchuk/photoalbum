﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represents details user view model.
    /// </summary>
    public sealed class FullUserViewModel
    {
        /// <summary>
        ///     Gets or sets identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating does requesting user follows requested user.
        /// </summary>
        public bool IsFollowed { get; set; }

        /// <summary>
        ///     Gets or sets email.
        /// </summary>
        [Display(Name = "UserEmail", ResourceType = typeof(UserDetailedResources))]
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets followers count.
        /// </summary>
        [Display(Name = "UserFollowers", ResourceType = typeof(UserDetailedResources))]
        public int FollowersCount { get; set; }

        /// <summary>
        ///     Gets or sets following count.
        /// </summary>
        [Display(Name = "UserFollowing", ResourceType = typeof(UserDetailedResources))]
        public int FollowingCount { get; set; }

        /// <summary>
        ///     Gets or sets posts count.
        /// </summary>
        [Display(Name = "UserPostsCount", ResourceType = typeof(UserDetailedResources))]
        public int PostsCount { get; set; }

        /// <summary>
        ///     Gets or sets biography.
        /// </summary>
        public string Biography { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is user account private or not.
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        ///     Gets or sets profile photo identifier.
        /// </summary>
        public Guid? ProfilePhotoId { get; set; }

        /// <summary>
        ///     Gets or sets albums.
        /// </summary>
        public IEnumerable<AlbumPreviewViewModel> Albums { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public PaginatedViewModel<PhotoMetadataPreviewViewModel> Photos { get; set; }
    }
}