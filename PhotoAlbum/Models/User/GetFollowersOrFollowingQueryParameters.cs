﻿using PhotoAlbum.Resources;
using System.ComponentModel.DataAnnotations;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represents get followers or following query parameters.
    /// </summary>
    public sealed class GetFollowersOrFollowingQueryParameters
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(UserDetailedResources),
            ErrorMessageResourceName = "UserIdRequired")]
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets page number.
        /// </summary>
        [Range(0, int.MaxValue,
            ErrorMessageResourceType = typeof(UserDetailedResources),
            ErrorMessageResourceName = "PageNumberRange")]
        public int? PageNumber { get; set; } = 0;

        /// <summary>
        ///     Gets or sets page size.
        /// </summary>
        [Range(1, 51,
            ErrorMessageResourceType = typeof(UserDetailedResources),
            ErrorMessageResourceName = "PageSizeRange")]
        public int? PageSize { get; set; } = 50;
    }
}