﻿using System;
using System.Collections.Generic;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represents post preview view model.
    /// </summary>
    public sealed class PostPreviewViewModel
    {
        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public IEnumerable<PhotoMetadataPreviewViewModel> Photos { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        ///     Gets or sets created date-time.
        /// </summary>
        public DateTime CreateDateTime { get; set; }
    }
}