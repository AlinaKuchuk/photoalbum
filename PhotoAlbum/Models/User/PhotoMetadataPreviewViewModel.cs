﻿using System;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    /// Represents photo metadata preview view model.
    /// </summary>
    public sealed class PhotoMetadataPreviewViewModel
    {
        /// <summary>
        ///     Gets or sets file identifier.
        /// </summary>
        public Guid FileId { get; set; }

        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        public int PostId { get; set; }
    }
}