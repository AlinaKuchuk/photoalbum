﻿using PhotoAlbum.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represents edit profile form data view model.
    /// </summary>
    public sealed class EditUserProfileFormViewModel
    {
        /// <summary>
        /// Gets or sets identifier.
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets first name.
        /// </summary>
        [Display(Name = nameof(FirstName), ResourceType = typeof(EditUserProfileResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(EditUserProfileResources),
            ErrorMessageResourceName = "FirstNameRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.EditUserProfile.FirstNameMaximumLength,
            ErrorMessageResourceType = typeof(EditUserProfileResources),
            ErrorMessageResourceName = "FirstNameLength")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets last name.
        /// </summary>
        [Display(Name = nameof(LastName), ResourceType = typeof(EditUserProfileResources))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(EditUserProfileResources),
            ErrorMessageResourceName = "LastNameRequired")]
        [StringLength(
            ValidationConstants.ValidationConstants.EditUserProfile.LastNameMaximumLength,
            ErrorMessageResourceType = typeof(EditUserProfileResources),
            ErrorMessageResourceName = "LastNameLength")]
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets user biography.
        /// </summary>
        [Display(Name = nameof(Biography), ResourceType = typeof(EditUserProfileResources))]
        [StringLength(
            ValidationConstants.ValidationConstants.EditUserProfile.BiographyMaximumLength,
            ErrorMessageResourceType = typeof(EditUserProfileResources),
            ErrorMessageResourceName = "BiographyLength")]
        public string Biography { get; set; }

        /// <summary>
        ///     Gets or sets user profile photo identifier.
        /// </summary>
        public Guid? ProfilePhotoId { get; set; }
    }
}