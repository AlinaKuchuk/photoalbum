﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    /// Represents edit user aggregated view model.
    /// </summary>
    public sealed class EditUserAggregatedViewModel
    {
        /// <summary>
        /// Gets or sets edit user view model.
        /// </summary>
        public EditUserViewModel EditUserModel { get; set; }

        /// <summary>
        /// Represents available user roles.
        /// </summary>
        public IList<SelectListItem> AvailableRoles { get; set; }
    }
}