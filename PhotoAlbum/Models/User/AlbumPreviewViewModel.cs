﻿using System;
using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represent album preview view model.
    /// </summary>
    public sealed class AlbumPreviewViewModel
    {
        /// <summary>
        ///     Gets or sets identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Get or sets a value indicating is album private or not.
        /// </summary>
        [Display(ResourceType = typeof(AlbumPhotosResources), Name = "IsPrivate")]
        public bool IsPrivate { get; set; }

        /// <summary>
        ///     Gets or sets preview photo identifier.
        /// </summary>
        public Guid? PreviewPhotoId { get; set; }

        /// <summary>
        ///     Gets or sets owner identifier.
        /// </summary>
        public string OwnerId { get; set; }
    }
}