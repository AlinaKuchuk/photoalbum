﻿using System;
using System.ComponentModel.DataAnnotations;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represents user view model.
    /// </summary>
    public sealed class UserViewModel
    {
        /// <summary>
        ///     Gets or sets identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets first name.
        /// </summary>
        [Display(Name = "UserListUserFirstName", ResourceType = typeof(UserListResources))]
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets last name.
        /// </summary>
        [Display(Name = "UserListUserLastName", ResourceType = typeof(UserListResources))]
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets email.
        /// </summary>
        [Display(Name = "UserListUserEmail", ResourceType = typeof(UserListResources))]
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is user account private or not.
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        ///     Gets or sets roles.
        /// </summary>
        public string Roles { get; set; }

        /// <summary>
        ///     Gets or sets profile photo identifier.
        /// </summary>
        public Guid? ProfilePhotoId { get; set; }
    }
}