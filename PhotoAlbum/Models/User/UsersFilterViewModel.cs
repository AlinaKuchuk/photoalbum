﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models.User
{
    /// <summary>
    ///     Represents user view model.
    /// </summary>
    public sealed class UsersFilterViewModel
    {
        /// <summary>
        ///     Gets or sets user name.
        /// </summary>
        [Display(Name = "UserFilteredName", ResourceType = typeof(UserListResources))]
        [StringLength(
            ValidationConstants.ValidationConstants.FilterUser.UserNameMaximumLength,
            ErrorMessageResourceType = typeof(UserListResources),
            ErrorMessageResourceName = "UserNameMaxLength")]
        public string UserName { get; set; }

        /// <summary>
        ///     Gets or sets role.
        /// </summary>
        [Display(Name = "UserFilteredRole", ResourceType = typeof(UserListResources))]
        public string Role { get; set; }

        /// <summary>
        ///     Gets or sets roles.
        /// </summary>
        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}