﻿namespace PhotoAlbum.Models
{
    /// <summary>
    ///     Contains custom claim names.
    /// </summary>
    public static class ClaimNamesConstants
    {
        /// <summary>
        ///     Gets full name claim name.
        /// </summary
        public const string FullNameClaimName = "FullName";

        /// <summary>
        ///     Gets avatar claim name.
        /// </summary>
        public const string AvatarClaimName = "Avatar";

        /// <summary>
        ///     Gets is private claim name.
        /// </summary>
        public const string IsPrivateClaimName = "IsPrivate";

        /// <summary>
        ///     Gets roles claim name.
        /// </summary>
        public const string RolesClaimName = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
    }
}