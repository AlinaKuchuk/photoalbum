﻿using System;
using System.Collections.Generic;
using PhotoAlbum.Services.Models.Enums;

namespace PhotoAlbum.Models
{
    /// <summary>
    ///     Contains model constants for edit user.
    /// </summary>
    public static class ModelConstants
    {
        /// <summary>
        ///     Gets user roles.
        /// </summary>
        public static IEnumerable<string> UserRoles = Enum.GetNames(typeof(RolesDto));
    }
}