﻿using System.Web.Mvc;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Models
{
    /// <summary>
    ///     Represents error view model.
    /// </summary>
    public sealed class ErrorViewModel
    {
        /// <summary>
        ///     Gets or sets error message.
        /// </summary>
        public string ErrorMessage { get; set; } = ErrorMessagesResources.UnknowError;

        /// <summary>
        ///     Gets or sets redirect URI.
        /// </summary>
        public string RedirectUri { get; set; }
    }
}