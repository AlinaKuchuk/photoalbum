﻿using PhotoAlbum.Models.User;

namespace PhotoAlbum.Models
{
    /// <summary>
    ///     Represents user list view model.
    /// </summary>
    public sealed class UserListViewModel
    {
        /// <summary>
        ///     Gets or sets users.
        /// </summary>
        public PaginatedViewModel<UserViewModel> Users { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is action available or not.
        /// </summary>
        public bool IsActionAvailable { get; set; }
    }
}