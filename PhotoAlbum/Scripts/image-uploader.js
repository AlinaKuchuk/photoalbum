﻿
function preview(event, targetId) {
    var imagePreview = document.getElementById("imagePreview");
    var copyImagePreview = imagePreview.cloneNode(true);
    copyImagePreview.classList.replace("d-none", "d-flex");

    var reader = new FileReader();
    reader.onload = function() {
        copyImagePreview.firstElementChild.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);

    var target = document.getElementById(targetId);
    target.appendChild(copyImagePreview);
    target.firstElementChild.remove();
}