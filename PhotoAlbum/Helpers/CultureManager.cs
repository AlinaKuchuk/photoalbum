﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using PhotoAlbum.Models;

namespace PhotoAlbum.Helpers
{
    /// <summary>
    ///     Represents culture manager.
    /// </summary>
    public static class CultureManager
    {
        /// <summary>
        ///     Gets culture cookie name.
        /// </summary>
        public const string CultureCookieName = "_culture";

        /// <summary>
        ///     Gets all supported cultures.
        /// </summary>
        public static readonly List<CultureViewModel> SupportedCultures = new List<CultureViewModel>
        {
            new CultureViewModel
            {
                TwoLetterName = "en",
                FullName = "English",
                Image = "eng.png"
            },
            new CultureViewModel
            {
                TwoLetterName = "ru",
                FullName = "Russian",
                Image = "rus.png"
            }
        };

        /// <summary>
        ///     Gets current culture information.
        /// </summary>
        public static CultureViewModel CurrentCulture { get; private set; }

        /// <summary>
        ///     Returns culture. If it is not implemented or invalid - return default culture.
        /// </summary>
        public static string GetImplementedCulture(string name)
        {
            if (string.IsNullOrEmpty(name)) return GetDefaultCulture();

            return SupportedCultures.Any(culture =>
                name.Equals(culture.TwoLetterName, StringComparison.InvariantCultureIgnoreCase))
                ? name
                : GetDefaultCulture();
        }

        /// <summary>
        ///     Returns default culture name which is the first name declared.
        /// </summary>
        /// <returns>The short name of culture.</returns>
        public static string GetDefaultCulture()
        {
            return SupportedCultures.First().TwoLetterName;
        }

        /// <summary>
        ///     Sets culture.
        /// </summary>
        /// <param name="cultureCode">Culture code.</param>
        public static void SetCulture(string cultureCode)
        {
            CurrentCulture = SupportedCultures.FirstOrDefault(culture => culture.TwoLetterName == cultureCode) ??
                             throw new NotSupportedException();
            Thread.CurrentThread.CurrentCulture = new CultureInfo(cultureCode);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }
    }
}