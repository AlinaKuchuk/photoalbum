﻿using AutoMapper;
using PhotoAlbum.Models;
using PhotoAlbum.Models.User;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.MappingProfiles
{
    /// <summary>
    ///     Represents photo metadata mapping profile.
    /// </summary>
    public sealed class PhotoMetadataMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PhotoMetadataMappingProfile" /> class.
        /// </summary>
        public PhotoMetadataMappingProfile()
        {
            CreateMap<PhotoMetadataPreviewDto, PhotoMetadataPreviewViewModel>();
            CreateMap<PaginatedDataDto<PhotoMetadataPreviewDto>, PaginatedViewModel<PhotoMetadataPreviewViewModel>>();
        }
    }
}