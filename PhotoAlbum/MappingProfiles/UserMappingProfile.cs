﻿using AutoMapper;
using PhotoAlbum.Models;
using PhotoAlbum.Models.Account;
using PhotoAlbum.Models.User;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.MappingProfiles
{
    /// <summary>
    ///     Represents user mapping profile.
    /// </summary>
    public sealed class UserMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="UserMappingProfile" /> class.
        /// </summary>
        public UserMappingProfile()
        {
            CreateMap<RegisterUserViewModel, CreateUserDto>();
            CreateMap<LoginViewModel, UserCredentialsDto>();

            // User profile
            CreateMap<UserDto, EditUserProfileFormViewModel>();

            // Filters
            CreateMap<UsersFilterViewModel, GetUsersParametersDto>();

            // User list
            CreateMap<UserDto, UserViewModel>()
                .ForMember(
                    dest => dest.Roles,
                    opt => opt.MapFrom(src => string.Join(", ", src.Roles)));

            // Edit
            CreateMap<UserDto, EditUserViewModel>()
                .ReverseMap()
                .ForMember(dest => dest.ProfilePhotoId, opt => opt.Ignore())
                .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.SelectedRoles));

            // Full
            CreateMap<FullUserDto, FullUserViewModel>();

            CreateMap<EditUserProfileViewModel, EditUserProfileDto>()
                .ForMember(dest => dest.PhotoStream, opt => opt.MapFrom(src => src.ProfilePhoto.InputStream));

            CreateMap<PaginatedDataDto<UserDto>, PaginatedViewModel<UserViewModel>>();
        }
    }
}