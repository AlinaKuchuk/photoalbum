﻿using AutoMapper;
using PhotoAlbum.Models.Comment;
using PhotoAlbum.Models.Post;
using PhotoAlbum.Services.Models.Comments;

namespace PhotoAlbum.MappingProfiles
{
    /// <summary>
    ///     Represents comment mapping profile.
    /// </summary>
    public sealed class CommentMappingProfile : Profile 
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CommentMappingProfile" /> class.
        /// </summary>
        public CommentMappingProfile()
        {
            CreateMap<CreateCommentViewModel, CreateCommentDto>();

            CreateMap<CommentDto, CommentViewModel>()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.Author.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.Author.LastName))
                .ForMember(dest => dest.AuthorId, opt => opt.MapFrom(src => src.Author.Id));
        }
    }
}