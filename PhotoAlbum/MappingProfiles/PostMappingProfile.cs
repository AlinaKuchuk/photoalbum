﻿using System.Linq;
using AutoMapper;
using PhotoAlbum.Models;
using PhotoAlbum.Models.Post;
using PhotoAlbum.Models.Shared;
using PhotoAlbum.Models.User;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Comments;
using PhotoAlbum.Services.Models.Post;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.MappingProfiles
{
    /// <summary>
    ///     Represents post mapping profile.
    /// </summary>
    public sealed class PostMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PostMappingProfile" /> class.
        /// </summary>
        public PostMappingProfile()
        {
            CreateMap<PostPreviewDto, PostPreviewViewModel>();
            CreateMap<PaginatedDataDto<CommentDto>, PaginatedViewModel<CommentViewModel>>();
            CreateMap<PaginatedDataDto<PostPreviewDto>, PaginatedViewModel<PostPreviewViewModel>>();

            CreateMap<FullPostDto, FullPostViewModel>()
                .ForMember(dest => dest.Author,
                    opt => opt.MapFrom(src => src.Album.Owner));
            CreateMap<UserDto, AuthorViewModel>();

            CreateMap<CreatePostViewModel, CreatePostDto>()
                .ForMember(
                    dest => dest.LoadedImages,
                    opt => opt.MapFrom(src => src.Photos.Where(x => x != null).Select(x => new LoadedImageDto
                        {Extension = x.ContentType, ImageStream = x.InputStream})));

            CreateMap<GetFullPostQueryParameters, GetFullPostQueryParametersDto>()
                .ForPath(
                    dest => dest.PaginationParameters.PageNumber,
                    opt => opt.MapFrom(src => src.PageNumber))
                .ForPath(
                    dest => dest.PaginationParameters.PageSize,
                    opt => opt.MapFrom(src => src.PageSize));

            CreateMap<PostsStatisticsDto, PostStatisticViewModel>();

            CreateMap<PaginatedDataDto<PreviewPostAggregatedDto>, PaginatedViewModel<PreviewPostAggregatedViewModel>>();
            CreateMap<PreviewPostAggregatedDto, PreviewPostAggregatedViewModel>();
            CreateMap<PostPreviewDto, PreviewPostViewModel>();
        }
    }
}