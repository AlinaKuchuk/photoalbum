﻿using AutoMapper;
using PhotoAlbum.Models;
using PhotoAlbum.Models.Album;
using PhotoAlbum.Models.Shared;
using PhotoAlbum.Models.User;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Album;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.MappingProfiles
{
    /// <summary>
    ///     Represents album mapping profile.
    /// </summary>
    public sealed class AlbumMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AlbumMappingProfile" /> class.
        /// </summary>
        public AlbumMappingProfile()
        {
            CreateMap<AlbumDto, AlbumPreviewViewModel>()
                .ForMember(dest => dest.OwnerId, opt => opt.MapFrom(src => src.Owner.Id));

            CreateMap<CreateAlbumViewModel, CreateAlbumDto>();

            CreateMap<UserAlbumsDto, UserAlbumsViewModel>();
            CreateMap<UserDto, AuthorViewModel>();
            CreateMap<PaginatedDataDto<AlbumDto>, PaginatedViewModel<AlbumPreviewViewModel>>();

            CreateMap<FullAlbumDto, FullAlbumViewModel>();

            CreateMap<EditAlbumViewModel, EditAlbumDto>();
            CreateMap<AlbumDto, EditAlbumViewModel>();
        }
    }
}