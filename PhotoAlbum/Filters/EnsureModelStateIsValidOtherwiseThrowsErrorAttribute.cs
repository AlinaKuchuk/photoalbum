﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace PhotoAlbum.Filters
{
    /// <summary>
    ///     Represents action attribute that redirect request to error page when model stat is invalid.
    /// </summary>
    internal sealed class EnsureModelStateIsValidOtherwiseThrowsErrorAttribute : ActionFilterAttribute
    {
        /// <inheritdoc />
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.Controller.ViewData.ModelState.IsValid)
            {
                var error = string.Join(Environment.NewLine, filterContext.Controller.ViewData.ModelState.Values.SelectMany(err => err.Errors)
                    .Select(x => x.ErrorMessage));
                filterContext.Result = new RedirectToRouteResult("Error", new RouteValueDictionary(new { ErrorMessage = error}));
            }
        }
    }
}