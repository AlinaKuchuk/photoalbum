﻿using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;

namespace PhotoAlbum.Filters
{
    /// <summary>
    ///     Represents global exception filter for saving exception information.
    /// </summary>
    internal sealed class GlobalExceptionLoggerExceptionFilter : FilterAttribute, IExceptionFilter
    {
        /// <inheritdoc />
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            var exceptionDataService = DependencyResolver.Current.GetService<IExceptionDataService>();

            // Rise an forget
            exceptionDataService?.SaveExceptionDataAsync(new CreateExceptionDataDto
            {
                UserPrincipalId = filterContext.HttpContext?.User?.Identity?.GetUserId(),
                Url = filterContext.HttpContext?.Request.RawUrl,
                StackTrace = filterContext.Exception.StackTrace 
            });

            filterContext.Result = new RedirectToRouteResult( new RouteValueDictionary(new
            {
                action = "GetErrorOccursView",
                controller = "Error",
                ErrorMessage = filterContext.Exception.Message,
                RedirectUri = new UrlHelper(filterContext.RequestContext).Action("GetExploreView", "Explore")
            }));

            filterContext.ExceptionHandled = true;
        }
    }
}