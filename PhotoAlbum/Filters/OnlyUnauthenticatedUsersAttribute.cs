﻿using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;

namespace PhotoAlbum.Filters
{
    /// <summary>
    /// Represents authorization filter that allows access view only authenticated user. 
    /// </summary>
    public sealed class OnlyUnauthenticatedUsersAttribute : FilterAttribute, IAuthenticationFilter
    {
        /// <inheritdoc />
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        /// <inheritdoc />
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        { "controller", "Explore" },
                        { "action", "GetExploreView" }
                    });
            }
        }
    }
}