﻿using System.Web.Mvc;

namespace PhotoAlbum.Filters
{
    /// <summary>
    ///     Represents an action attribute performs checking models state on request.
    /// </summary>
    internal sealed class EnsureModelStateValidAttribute : ActionFilterAttribute
    {
        private readonly string _viewName;

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnsureModelStateValidAttribute" /> class.
        /// </summary>
        /// <param name="viewName">The name of resulting view model.</param>
        public EnsureModelStateValidAttribute(string viewName)
        {
            _viewName = viewName;
        }

        /// <inheritdoc />
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.Controller.ViewData.ModelState.IsValid)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = _viewName
                };
            }
        }
    }
}