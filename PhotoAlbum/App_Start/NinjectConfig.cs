﻿using System.Web.Mvc;
using Ninject;
using Ninject.Web.Mvc;
using PhotoAlbum.Infrastructure;
using PhotoAlbum.Services.Infrastructure;

namespace PhotoAlbum
{
    /// <summary>
    /// Provides ninject configuration.
    /// </summary>
    public static class NinjectConfig
    {
        /// <summary>
        /// Configure ninject container.
        /// </summary>
        internal static void Configure()
        {
            var ninjectKernel = new StandardKernel(new WebModule(), new ServicesModule());
            ninjectKernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(ninjectKernel));
        }
    }
}