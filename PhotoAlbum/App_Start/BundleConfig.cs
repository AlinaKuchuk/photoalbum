﻿using System.Web.Optimization;

namespace PhotoAlbum
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*",
                "~/Scripts/popper.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/popper.min.js"));

            bundles.Add(new StyleBundle("~/Content/css")
                .IncludeDirectory("~/Content/fontawesome/webfonts/", "*.eot")
                .IncludeDirectory("~/Content/fontawesome/webfonts/", "*.woff")
                .IncludeDirectory("~/Content/fontawesome/webfonts/", "*.ttf")
                .IncludeDirectory("~/Content/fontawesome/webfonts/", "*.svg")
                .Include(
                "~/Content/bootstrap.css",
                "~/Content/fontawesome/css/all.css",
                "~/Content/site.css"));

            // Add all images in the project to the single bundle
            bundles.Add(new Bundle("~/Content/imgs")
                .IncludeDirectory("~/Content/imgs/cultures", "*.png")
                .Include("~/Content/imgs/logo.png"));
        }
    }
}