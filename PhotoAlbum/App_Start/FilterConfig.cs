﻿using System.Web.Mvc;
using PhotoAlbum.Filters;

namespace PhotoAlbum
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new GlobalExceptionLoggerExceptionFilter());
        }
    }
}