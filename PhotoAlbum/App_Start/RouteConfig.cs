﻿using System.Web.Mvc;
using System.Web.Routing;

namespace PhotoAlbum
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Error",
                "Error",
                new
                {
                    controller = "Error",
                    action = "GetErrorOccursView",
                    id = UrlParameter.Optional
                });

            routes.MapRoute(
                "Explore",
                "Explore/{action}/{id}",
                new
                {
                    controller = "Explore", action = "GetExploreView", id = UrlParameter.Optional
                });
            

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new
                {
                    controller = "Explore", action = "GetExploreView", id = UrlParameter.Optional
                });
        }
    }
}