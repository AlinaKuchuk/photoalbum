﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Models.Post;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models.Enums;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents statistics controller.
    /// </summary>
    public sealed class StatisticController : BaseController
    {
        private readonly IStatisticService _statisticService;
        private readonly IMapper _mapper;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StatisticController" /> class.
        /// </summary>
        /// <param name="statisticService">Statistic service.</param>
        /// <param name="mapper">Mapper.</param>
        public StatisticController(IStatisticService statisticService, IMapper mapper)
        {
            _statisticService = statisticService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Gets user posts statistic.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize(Roles = nameof(RolesDto.Business))]
        public async Task<ActionResult> GetPostsStatisticsAsync()
        {
            return View(
                "PostsStatisticsView",
                _mapper.Map<PostStatisticViewModel>(
                    await _statisticService.GetPostsStatisticAsync(User.Identity.GetUserId())));
        }
    }
}