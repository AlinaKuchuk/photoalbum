﻿using System;
using System.Web.Mvc;
using PhotoAlbum.Helpers;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents base controller.
    /// </summary>
    public abstract class BaseController : Controller
    {
        /// <summary>
        ///     Begins to invoke the action in the current controller context.
        /// </summary>
        /// <param name="callback">The callback.</param>
        /// <param name="state">The state.</param>
        /// <returns>Returns an IAsyncController instance.</returns>
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            var requestedCulture = Request.Cookies["_culture"];
            string cultureName;

            if (requestedCulture != null)
            {
                cultureName = requestedCulture.Value;
            }
            else
            {
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0
                    ? Request.UserLanguages[0]
                    : null;
            }

            cultureName = CultureManager.GetImplementedCulture(cultureName);

            CultureManager.SetCulture(cultureName);

            return base.BeginExecuteCore(callback, state);
        }
    }
}