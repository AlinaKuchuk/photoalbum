﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using PhotoAlbum.Models;
using PhotoAlbum.Resources;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents error controller.
    /// </summary>
    public sealed class ErrorController : BaseController
    {
        /// <summary>
        ///     Returns successfully completed view.
        /// </summary>
        /// <param name="successfullyCompletedModel">Successfully completed view successfullyCompletedModel.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        public Task<ActionResult> GetSuccessfullyCompletedView(
            [Required] SuccessfullyCompletedViewModel successfullyCompletedModel)
        {
            return Task.FromResult<ActionResult>(View("SuccessfullyCompletedView", successfullyCompletedModel));
        }

        /// <summary>
        ///     Returns error occurs view.
        /// </summary>
        /// <param name="errorModel">Error view successfullyCompletedModel.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        public Task<ActionResult> GetErrorOccursView([Required] ErrorViewModel errorModel)
        {
            return Task.FromResult<ActionResult>(View("ErrorOccursView", errorModel));
        }

        /// <summary>
        ///     Returns error occurs view.
        /// </summary>
        /// <param name="errorModel">Error view successfullyCompletedModel.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        public Task<ActionResult> NotFound([Required] ErrorViewModel errorModel)
        {
            errorModel.ErrorMessage = ErrorMessagesResources.NotFound;
            Response.StatusCode = 404;
            return Task.FromResult<ActionResult>(View("ErrorOccursView", errorModel));
        }

        /// <summary>
        ///     Returns error occurs view.
        /// </summary>
        /// <param name="errorModel">Error view successfullyCompletedModel.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        public Task<ActionResult> ServerError([Required] ErrorViewModel errorModel)
        {
            errorModel.ErrorMessage = ErrorMessagesResources.ServerError;
            Response.StatusCode = 500;
            return Task.FromResult<ActionResult>(View("ErrorOccursView", errorModel));
        }
    }
}