﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Filters;
using PhotoAlbum.Infrastructure.Helpers;
using PhotoAlbum.Models;
using PhotoAlbum.Models.Album;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Album;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents album controller.
    /// </summary>
    [Authorize]
    public sealed class AlbumController : BaseController
    {
        private readonly IAlbumService _albumService;
        private readonly IMapper _mapper;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AlbumController" /> class.
        /// </summary>
        /// <param name="albumService">Album service.</param>
        /// <param name="mapper">Mapper.</param>
        public AlbumController(IAlbumService albumService, IMapper mapper)
        {
            _albumService = albumService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Returns form for creating album.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        public Task<ActionResult> GetCreateAlbumFormView()
        {
            return Task.FromResult<ActionResult>(View("CreateAlbumFormView"));
        }

        /// <summary>
        ///     Performs album creating.
        /// </summary>
        /// <param name="createAlbumViewModel">Creating album view model.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [EnsureModelStateValid("CreateAlbumFormView")]
        public async Task<ActionResult> CreateAlbum([Required] CreateAlbumViewModel createAlbumViewModel)
        {
            var addedAlbum = await _albumService.CreateAlbumAsync(
                _mapper.Map<CreateAlbumDto>(createAlbumViewModel),
                User.Identity.GetUserId());

            return RedirectToAction("GetAlbumView", new {AlbumId = addedAlbum.Id});
        }

        /// <summary>
        ///     Gets all user albums.
        /// </summary>
        /// <param name="queryParameters">Query parameters to get user albums.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetUserAlbumsView(
            [Required] GetUserAlbumsQueryParameters queryParameters)
        {
            return View(
                "UserAlbumsView",
                _mapper.Map<UserAlbumsViewModel>(
                    await _albumService.GetUserAlbumsAsync(
                        new GetUserAlbumsQueryParametersDto
                        {
                            RequestingUserId = User.Identity.GetUserId(),
                            UserId = queryParameters.UserId,
                            RequestingUserRoles = User.Identity.GetClaim(ClaimNamesConstants.RolesClaimName).Split(','),
                            AlbumsPaginationQueryParameters = new GetPaginationQueryParametersDto
                            {
                                PageNumber = queryParameters.PageNumber.Value,
                                PageSize = queryParameters.PageSize.Value
                            }
                        })
                ));
        }

        /// <summary>
        ///     Gets an album photos.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetAlbumView(
            [Required] GetAlbumPhotosQueryParameters queryParameters)
        {
            return View(
                "AlbumView",
                _mapper.Map<FullAlbumViewModel>(
                    await _albumService.GetFullAlbumAsync(
                        new GetAlbumQueryParametersDto
                        {
                            RequestingUserId = User.Identity.GetUserId(),
                            AlbumId = queryParameters.AlbumId.Value,
                            RequestingUserRoles = User.Identity.GetClaim(ClaimNamesConstants.RolesClaimName).Split(','),
                            PhotosPaginationQueryParameters = new GetPaginationQueryParametersDto
                            {
                                PageNumber = queryParameters.PageNumber.Value,
                                PageSize = queryParameters.PageSize.Value
                            }
                        })
                ));
        }

        /// <summary>
        ///     Returns edit album form.
        /// </summary>
        /// <param name="albumId">Album identifier to edit.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> GetEditAlbumFormView([Required] int? albumId)
        {
            var album = await _albumService.GetAlbumAsync(albumId.Value);

            return View(
                "EditAlbumFormView",
                _mapper.Map<EditAlbumViewModel>(album));
        }

        /// <summary>
        ///     Edits album.
        /// </summary>
        /// <param name="editAlbum">Album to edit.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPut]
        [Authorize]
        public async Task<ActionResult> EditAlbum(EditAlbumViewModel editAlbum)
        {
            if (!ModelState.IsValid)
            {
                var albumToEdit = await _albumService.GetAlbumAsync(editAlbum.Id.Value);

                return View(
                    "EditAlbumFormView",
                    _mapper.Map<EditAlbumViewModel>(albumToEdit));
            }

            await _albumService.EditAlbumAsync(_mapper.Map<EditAlbumDto>(editAlbum), User.Identity.GetUserId());

            return RedirectToAction("GetAlbumView", new {AlbumId = editAlbum.Id});
        }
    }
}