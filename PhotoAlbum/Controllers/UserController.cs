﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Filters;
using PhotoAlbum.Infrastructure.Helpers;
using PhotoAlbum.Models;
using PhotoAlbum.Models.User;
using PhotoAlbum.Resources;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Enums;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents user controller.
    /// </summary>
    public sealed class UserController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="UserController" /> class.
        /// </summary>
        /// <param name="userService">User service.</param>
        /// <param name="mapper">Mapper.</param>
        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Retrieves users.
        /// </summary>
        /// <param name="filter">User filter.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize(Roles = nameof(RolesDto.Administrator))]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetUserManagementListView([Required] UsersFilterViewModel filter)
        {
            return View(
                "UserManagementListView",
                new UserListAggregatedViewModel
                {
                    Filter = filter,
                    Users = _mapper
                        .Map<IEnumerable<UserViewModel>>(
                            await _userService.GetUsersAsync(_mapper.Map<GetUsersParametersDto>(filter))).ToArray()
                });
        }

        /// <summary>
        ///     Returns user edit form.
        /// </summary>
        /// <param name="id">Identifier of user to edit.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize(Roles = nameof(RolesDto.Administrator))]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetUserEditFormView([Required] string id)
        {
            var user = await _userService.GetUserAsync(id);

            var editUserAggregateViewModel = new EditUserAggregatedViewModel
            {
                EditUserModel = _mapper.Map<EditUserViewModel>(user),
                AvailableRoles = BuildRolesSelectListForUser(user.Roles.ToArray()).ToArray()
            };

            return View("UserEditFormView", editUserAggregateViewModel);
        }

        /// <summary>
        ///     Edit user.
        /// </summary>
        /// <param name="model">Edit user aggregate view model.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPut]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(RolesDto.Administrator))]
        public async Task<ActionResult> EditUser([Required] EditUserAggregatedViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.AvailableRoles =
                    BuildRolesSelectListForUser(model.EditUserModel.SelectedRoles.ToArray()).ToArray();
                return View("UserEditFormView", model);
            }

            await _userService.UpdateUserAsync(_mapper.Map<UserDto>(model.EditUserModel));

            return RedirectToAction("GetSuccessfullyCompletedView", "Error", new SuccessfullyCompletedViewModel
            {
                Title = null,
                Description = UserListResources.UserIsUpdated,
                ReturnUri = Url.Action("GetUserManagementListView", "User")
            });
        }

        /// <summary>
        ///     Returns edit user form.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> GetEditUserProfileFormView()
        {
            var user = await _userService.GetUserAsync(User.Identity.GetUserId());

            return View(
                "EditUserProfileFormView",
                _mapper.Map<EditUserProfileFormViewModel>(user));
        }

        /// <summary>
        ///     Edits user profile.
        /// </summary>
        /// <param name="editUserProfile">User profile to edit.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> EditUserProfile(EditUserProfileViewModel editUserProfile)
        {
            if (!ModelState.IsValid)
            {
                var user = await _userService.GetUserAsync(User.Identity.GetUserId());

                return View(
                    "EditUserProfileFormView",
                    _mapper.Map<EditUserProfileFormViewModel>(user));
            }

            await _userService.EditProfileAsync(_mapper.Map<EditUserProfileDto>(editUserProfile));

            return RedirectToAction("GetDetailedUserView", new {UserId = editUserProfile.Id});
        }

        /// <summary>
        ///     Subscribes a user to another user.
        /// </summary>
        /// <param name="userId">User who subscribes.</param>
        /// <param name="redirectUri">Url to redirect.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        [HttpPost]
        [Authorize]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> StartFollow([Required(AllowEmptyStrings = false)] string userId,
            string redirectUri)
        {
            await _userService.StartFollowAsync(User.Identity.GetUserId(), userId);
            return Redirect(redirectUri);
        }

        /// <summary>
        ///     Unsubscribes a user from another user.
        /// </summary>
        /// <param name="userId">User to unsubscribes.</param>
        /// <param name="redirectUri">Url to redirect.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        [HttpPost]
        [Authorize]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> StopFollow([Required(AllowEmptyStrings = false)] string userId,
            string redirectUri)
        {
            await _userService.StopFollowAsync(User.Identity.GetUserId(), userId);
            return Redirect(redirectUri);
        }


        /// <summary>
        ///     Retrieves user details.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [AllowAnonymous]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetDetailedUserView(
            [Required] GetFullUserQueryParameters queryParameters)
        {
            return View(
                "UserDetailedView",
                _mapper.Map<FullUserViewModel>(
                    await _userService.GetFullUserAsync(
                        new GetFullUserQueryParametersDto
                        {
                            RequestingUserId = User.Identity.GetUserId(),
                            UserId = queryParameters.UserId,
                            RequestingUserRoles = User.Identity.GetClaim(ClaimNamesConstants.RolesClaimName).Split(','),
                            PhotosPaginationQueryParameters = new GetPaginationQueryParametersDto
                            {
                                PageNumber = queryParameters.PageNumber.Value,
                                PageSize = queryParameters.PageSize.Value
                            }
                        })
                ));
        }

        /// <summary>
        ///     Performs updating user, change user role.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPut]
        [Authorize]
        public async Task<ActionResult> MakeUserBusiness()
        {
            await _userService.MakeUserBusinessAsync(User.Identity.GetUserId());

            return RedirectToAction("GetLoginView", "Account");
        }

        /// <summary>
        ///     Performs updating user, make him private.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPut]
        [Authorize]
        public async Task<ActionResult> MakeUserPrivate()
        {
            await _userService.MakeUserPrivateAsync(User.Identity.GetUserId());

            return RedirectToAction(
                "GetSuccessfullyCompletedView",
                "Error",
                new
                {
                    Description = SettingsResources.ResultUpdatingToPrivate,
                    ReturnUri = Url.Action("GetExploreView", "Explore")
                });
        }

        /// <summary>
        ///     Performs updating user, make him not private.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPut]
        [Authorize]
        public async Task<ActionResult> MakeUserNotPrivate()
        {
            await _userService.MakeUserNotPrivateAsync(User.Identity.GetUserId());

            return RedirectToAction(
                "GetSuccessfullyCompletedView",
                "Error",
                new
                {
                    Description = SettingsResources.ResultUpdatingToNotPrivate,
                    ReturnUri = Url.Action("GetExploreView", "Explore")
                });
        }

        /// <summary>
        ///     Returns followers view.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetFollowersView(
            [Required] GetFollowersOrFollowingQueryParameters queryParameters)
        {
            var followers = await _userService.GetFollowersAsync(new GetFollowersOrFollowingQueryParametersDto
            {
                RequestingUserId = User.Identity.GetUserId(),
                UserId = queryParameters.UserId,
                RequestingUserRoles = User.Identity.GetClaim(ClaimNamesConstants.RolesClaimName).Split(','),
                PaginationQueryParameters = new GetPaginationQueryParametersDto
                {
                    PageSize = queryParameters.PageSize.Value,
                    PageNumber = queryParameters.PageNumber.Value
                }
            });

            return View("FollowersView", _mapper.Map<PaginatedViewModel<UserViewModel>>(followers));
        }


        /// <summary>
        ///     Returns followings view.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetFollowingsView(
            [Required] GetFollowersOrFollowingQueryParameters queryParameters)
        {
            var followers = await _userService.GetFollowingAsync(new GetFollowersOrFollowingQueryParametersDto
            {
                RequestingUserId = User.Identity.GetUserId(),
                UserId = queryParameters.UserId,
                RequestingUserRoles = User.Identity.GetClaim(ClaimNamesConstants.RolesClaimName).Split(','),
                PaginationQueryParameters = new GetPaginationQueryParametersDto
                {
                    PageSize = queryParameters.PageSize.Value,
                    PageNumber = queryParameters.PageNumber.Value
                }
            });

            return View("FollowingsView", _mapper.Map<PaginatedViewModel<UserViewModel>>(followers));
        }

        /// <summary>
        ///     Returns settings view.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize]
        public async  Task<ActionResult> GetSettingsView()
        {
            var user = _mapper.Map<UserViewModel>(await _userService.GetUserAsync(User.Identity.GetUserId()));

            return View("SettingsView", user);
        }

        private static IEnumerable<SelectListItem> BuildRolesSelectListForUser(string[] userRoles = null)
        {
            foreach (var role in ModelConstants.UserRoles)
            {
                var isRoleSelected = userRoles?.Contains(role);
                yield return new SelectListItem
                {
                    Selected = isRoleSelected ?? false,
                    Text = role,
                    Value = role
                };
            }
        }
    }
}