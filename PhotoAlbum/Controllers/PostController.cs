﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Filters;
using PhotoAlbum.Infrastructure.Helpers;
using PhotoAlbum.Models;
using PhotoAlbum.Models.Post;
using PhotoAlbum.Models.User;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents post controller.
    /// </summary>
    [Authorize]
    public sealed class PostController : BaseController
    {
        private readonly IAlbumService _albumService;
        private readonly IMapper _mapper;
        private readonly IPostService _postService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PostController" /> class.
        /// </summary>
        /// <param name="albumService">Album service.</param>
        /// <param name="postService">Post service.</param>
        /// <param name="mapper">Mapper.</param>
        public PostController(
            IAlbumService albumService,
            IPostService postService,
            IMapper mapper)
        {
            _albumService = albumService;
            _postService = postService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Returns form for creating post.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        public async Task<ActionResult> GetCreatePostFormView()
        {
            var createPostAggregatedViewModel = new CreatePostAggregatedViewModel
            {
                Albums = _mapper.Map<AlbumPreviewViewModel[]>(
                    await _albumService.GetUserAlbumsAsync(User))
            };

            return View("CreatePostFormView", createPostAggregatedViewModel);
        }

        /// <summary>
        ///     Performs post creating.
        /// </summary>
        /// <param name="createPostViewModel">Creating post view model.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPost]
        public async Task<ActionResult> CreatePost([Required] CreatePostViewModel createPostViewModel)
        {
            if (!ModelState.IsValid)
            {
                var createPostAggregatedViewModel = new CreatePostAggregatedViewModel
                {
                    Albums = _mapper.Map<AlbumPreviewViewModel[]>(
                        await _albumService.GetUserAlbumsAsync(User))
                };

                return View("CreatePostFormView", createPostAggregatedViewModel);
            }

            var createdPost = await _postService.CreatePostAsync(
                _mapper.Map<CreatePostDto>(createPostViewModel),
                User.Identity.GetUserId());

            return RedirectToAction("GetDetailedPostView", new {PostId = createdPost.Id});
        }

        /// <summary>
        ///     Returns detailed post information by view.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetDetailedPostView(
            [Required] GetFullPostQueryParameters queryParameters)
        {
            return View("PostDetailedView", _mapper.Map<FullPostViewModel>(
                await _postService.GetFullPostAsync(
                    new GetFullPostQueryParametersDto
                    {
                        RequestingUserId = User.Identity.GetUserId(),
                        PostId = queryParameters.PostId.Value,
                        RequestingUserRoles = User.Identity.GetClaim(ClaimNamesConstants.RolesClaimName).Split(','),
                        PaginationParameters = new GetPaginationQueryParametersDto
                        {
                            PageSize = queryParameters.PageSize.Value,
                            PageNumber = queryParameters.PageNumber.Value 
                        }
                    })));
        }
    }
}