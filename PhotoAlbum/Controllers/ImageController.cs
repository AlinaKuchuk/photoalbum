﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Filters;
using PhotoAlbum.Services.Interfaces;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents image controller.
    /// </summary>
    [Authorize]
    public sealed class ImageController : BaseController
    {
        private readonly IFileStorage _fileStorage;
        private readonly IImageAccessAuthorizationService _imageAccessAuthorizationService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ImageController" /> class.
        /// </summary>
        /// <param name="fileStorage">File storage.</param>
        /// <param name="imageAccessAuthorizationService">Image accessing authorization service.</param>
        public ImageController(
            IFileStorage fileStorage,
            IImageAccessAuthorizationService imageAccessAuthorizationService)
        {
            _fileStorage = fileStorage;
            _imageAccessAuthorizationService = imageAccessAuthorizationService;
        }

        /// <summary>
        ///     Gets image.
        /// </summary>
        /// <param name="id">Image identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [OutputCache(Duration = 120)]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> GetImage([Required] Guid? id)
        {
            _imageAccessAuthorizationService.CheckPermissions(id.Value, User);
            return File(await _fileStorage.GetFileStringAsync(id.Value), "image/jpeg");
        }
    }
}