﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using PhotoAlbum.Filters;
using PhotoAlbum.Models.Account;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents account controller.
    /// </summary>
    [AllowAnonymous]
    public sealed class AccountController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AccountController" /> class.
        /// </summary>
        /// <param name="userService">User service.</param>
        /// <param name="mapper">Mapper.</param>
        public AccountController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Returns register view.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [OnlyUnauthenticatedUsers]
        public Task<ViewResult> GetRegisterView()
        {
            return Task.FromResult(View("RegisterView"));
        }

        /// <summary>
        ///     Performs registration.
        /// </summary>
        /// <param name="registerUserViewModel">Registration view model.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPost]
        [OnlyUnauthenticatedUsers]
        [ValidateAntiForgeryToken]
        [EnsureModelStateValid("RegisterView")]
        public async Task<ActionResult> RegisterUser([Required] RegisterUserViewModel registerUserViewModel)
        {
            await _userService.CreateUserAsync(_mapper.Map<CreateUserDto>(registerUserViewModel));

            return RedirectToAction("GetExploreView", "Explore");
        }

        /// <summary>
        ///     Returns login view.
        /// </summary>
        /// <param name="returnUri">Redirect url.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [OnlyUnauthenticatedUsers]
        public Task<ViewResult> GetLoginView(Uri returnUri)
        {
            ViewBag.ReturnUri = returnUri;
            return Task.FromResult(View("LoginView"));
        }

        /// <summary>
        ///     Performs login.
        /// </summary>
        /// <param name="loginViewModel">Login view model.</param>
        /// <param name="returnUri">Redirect url.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPost]
        [OnlyUnauthenticatedUsers]
        [EnsureModelStateValid("LoginView")]
        public async Task<ActionResult> LoginUser(LoginViewModel loginViewModel, Uri returnUri)
        {
            await _userService.LoginUserAsync(_mapper.Map<UserCredentialsDto>(loginViewModel));

            if (returnUri == null)
            {
                return RedirectToAction("GetExploreView", "Explore");
            }

            return Redirect(returnUri.ToString());
        }

        /// <summary>
        ///     Performs log out.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [Authorize]
        public ActionResult Logout()
        {
            _userService.Logout();

            return RedirectToAction("GetExploreView", "Explore");
        }
    }
} 