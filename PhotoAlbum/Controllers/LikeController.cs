﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Filters;
using PhotoAlbum.Services.Interfaces;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents like controller.
    /// </summary>
    [Authorize]
    public sealed class LikeController : BaseController
    {
        private readonly IPostService _postService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="LikeController" /> class.
        /// </summary>
        /// <param name="postService">Post service.</param>
        public LikeController(IPostService postService)
        {
            _postService = postService;
        }

        /// <summary>
        ///     Add a like to the post.
        /// </summary>
        /// <param name="postId">Post identifier.</param>
        /// <param name="redirectUri">Url for redirect.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> LikePost([Required] int? postId, string redirectUri)
        {
            await _postService.LikePostByUserAsync(User.Identity.GetUserId(), postId.Value);
            return Redirect(redirectUri);
        }

        /// <summary>
        ///     Delete a like from the post.
        /// </summary>
        /// <param name="postId">Post identifier.</param>
        /// <param name="redirectUri">Url for redirect.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public async Task<ActionResult> UnlikePost([Required] int? postId, string redirectUri)
        {
            // ReSharper disable once PossibleInvalidOperationException
            await _postService.UnlikePostByUserAsync(User.Identity.GetUserId(), postId.Value);
            return Redirect(redirectUri);
        }
    }
}