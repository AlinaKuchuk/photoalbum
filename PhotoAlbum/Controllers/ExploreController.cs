﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Filters;
using PhotoAlbum.Models;
using PhotoAlbum.Models.Post;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents explore controller.
    /// </summary>
    [Authorize]
    public sealed class ExploreController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExploreController" /> class.
        /// </summary>
        /// <param name="postService">Post service.</param>
        /// <param name="mapper">Mapper.</param>
        public ExploreController(IPostService postService, IMapper mapper)
        {
            _postService = postService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Returns explore view.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpGet]
        public async Task<ActionResult> GetExploreView(GetPostsListQueryParameters queryParameters)
        {
            var resultPosts = await _postService.GetExplorePostsAsync(new GetFollowingPostsQueryParametersDto
            {
                SearchText = queryParameters.SearchText,
                Sort = queryParameters.SortOrder,
                RequestingUserId = User.Identity.GetUserId(),
                PaginationQueryParameters = new GetPaginationQueryParametersDto
                {
                    PageSize = queryParameters.PageSize.Value,
                    PageNumber = queryParameters.PageNumber.Value
                }
            });

            return View("PostListView", new PreviewPostListAggregatedModel
            {
                Posts = _mapper.Map<PaginatedViewModel<PreviewPostAggregatedViewModel>>(resultPosts),
                QueryParameters = queryParameters
            });
        }

        /// <summary>
        ///     Returns following updates view.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task"/> representing asynchronous unit test.</returns>
        [HttpGet]
        [EnsureModelStateValid("FollowingPostListView")]
        public async Task<ActionResult> GetFollowingUpdatesAsync([Required] GetPostsListQueryParameters queryParameters)
        {
            var posts = await _postService.GetFollowingUpdatesAsync(new GetFollowingPostsQueryParametersDto
            {
                SearchText = queryParameters.SearchText,
                RequestingUserId = User.Identity.GetUserId(),
                Sort = queryParameters.SortOrder,
                PaginationQueryParameters = new GetPaginationQueryParametersDto
                {
                    PageSize = queryParameters.PageSize.Value,
                    PageNumber = queryParameters.PageNumber.Value 
                }
            });

            return View("FollowingPostListView", new PreviewPostListAggregatedModel
            {
                Posts = _mapper.Map<PaginatedViewModel<PreviewPostAggregatedViewModel>>(posts),
                QueryParameters = queryParameters
            });
        }
    }
}