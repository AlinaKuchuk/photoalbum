﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using PhotoAlbum.Filters;
using PhotoAlbum.Helpers;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents culture controller.
    /// </summary>
    public sealed class CultureController : BaseController
    {
        /// <summary>
        ///     Changes the culture.
        /// </summary>
        /// <param name="cultureCode"></param>
        /// <param name="redirectUri"></param>
        /// <returns></returns>
        [HttpGet]
        [EnsureModelStateIsValidOtherwiseThrowsError]
        public ActionResult SetCulture(
            [Required(AllowEmptyStrings = false)] [MaxLength(2)] [MinLength(2)]
            string cultureCode,
            [Required(AllowEmptyStrings = false)] Uri redirectUri)
        {
            var culture = CultureManager.GetImplementedCulture(cultureCode);

            var cultureCookie = Request.Cookies[CultureManager.CultureCookieName];

            if (cultureCookie != null)
            {
                cultureCookie.Value = culture;
            }
            else
            {
                cultureCookie = new HttpCookie(CultureManager.CultureCookieName)
                {
                    Value = culture,
                    Expires = DateTime.UtcNow.AddYears(1)
                };
            }

            Response.Cookies.Add(cultureCookie);

            return Redirect(redirectUri.ToString());
        }
    }
}