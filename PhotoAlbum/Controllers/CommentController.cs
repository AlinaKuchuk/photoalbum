﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PhotoAlbum.Models.Comment;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models.Comments;

namespace PhotoAlbum.Controllers
{
    /// <summary>
    ///     Represents comment controller.
    /// </summary>
    [Authorize]
    public sealed class CommentController : BaseController
    {
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CommentController" /> class.
        /// </summary>
        /// <param name="commentService">Album service.</param>
        /// <param name="mapper">Mapper.</param>
        public CommentController(ICommentService commentService, IMapper mapper)
        {
            _commentService = commentService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Creates comment.
        /// </summary>
        /// <param name="createComment">Comment to create.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous action result.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateComment([Required] CreateCommentViewModel createComment)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction(
                    "GetErrorOccursView",
                    "Error",
                    new
                    {
                        ErrorMessages = "Invalid entered data.",
                        ReturnUri =
                            createComment.PostId.HasValue
                                ? Url.Action("GetDetailedPostView", "Post", new {createComment.PostId})
                                : Url.Action("GetExploreView", "Explore")
                    });
            }

            await _commentService.CreateCommentAsync(
                _mapper.Map<CreateCommentDto>(createComment),
                User.Identity.GetUserId());

            return RedirectToAction("GetDetailedPostView", "Post", new {createComment.PostId});
        }
    }
}