﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhotoAlbum.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CreatePostResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CreatePostResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PhotoAlbum.Resources.CreatePostResources", typeof(CreatePostResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Album missing..
        /// </summary>
        public static string AlbumIdRequired {
            get {
                return ResourceManager.GetString("AlbumIdRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create.
        /// </summary>
        public static string CreatingFormOkButton {
            get {
                return ResourceManager.GetString("CreatingFormOkButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description must be smaller then {1}..
        /// </summary>
        public static string DescriptionMaximumLength {
            get {
                return ResourceManager.GetString("DescriptionMaximumLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to upload 1 of 3.
        /// </summary>
        public static string FirstPhoto {
            get {
                return ResourceManager.GetString("FirstPhoto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location must be smaller then {1}..
        /// </summary>
        public static string LocationMaximumLength {
            get {
                return ResourceManager.GetString("LocationMaximumLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string NewPostDescription {
            get {
                return ResourceManager.GetString("NewPostDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location.
        /// </summary>
        public static string NewPostLocation {
            get {
                return ResourceManager.GetString("NewPostLocation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Photo missing..
        /// </summary>
        public static string PhotoRequired {
            get {
                return ResourceManager.GetString("PhotoRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to upload 2 of 3.
        /// </summary>
        public static string SecondPhoto {
            get {
                return ResourceManager.GetString("SecondPhoto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to upload 3 of 3.
        /// </summary>
        public static string ThirdPhoto {
            get {
                return ResourceManager.GetString("ThirdPhoto", resourceCulture);
            }
        }
    }
}
