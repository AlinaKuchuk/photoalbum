﻿using System.Security.Principal;
using Antlr.Runtime;
using Microsoft.AspNet.Identity;

namespace PhotoAlbum.Infrastructure.Helpers
{
    /// <summary>
    ///     Represents principal extensions.
    /// </summary>
    public static class PrincipalExtension
    {
        /// <summary>
        ///     Checks if allowed new post.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="userId">User id.</param>
        /// <returns>True if post is allowed, otherwise false.</returns>
        public static bool IsAllowedNewPost(this IPrincipal principal, string userId)
        {
            return IsIdentifiersEquals(principal, userId);
        }

        /// <summary>
        ///     Checks if allowed new album.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="userId">User id.</param>
        /// <returns>True if album is allowed, otherwise false.</returns>
        public static bool IsAllowedNewAlbum(this IPrincipal principal, string userId)
        {
            return IsIdentifiersEquals(principal, userId);
        }

        /// <summary>
        ///     Checks if allowed edit album.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="userId">User id.</param>
        /// <returns>True if album is allowed, otherwise false.</returns>
        public static bool IsAllowedEditAlbum(this IPrincipal principal, string userId)
        {
            return IsIdentifiersEquals(principal, userId);
        }

        /// <summary>
        ///     Checks if identifiers are equals. 
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="userId">User id.</param>
        /// <returns>True if identifiers are equals, otherwise false.</returns>
        private static bool IsIdentifiersEquals(IPrincipal principal, string userId)
        {
            return principal.Identity.GetUserId() == userId;
        }
    }
}