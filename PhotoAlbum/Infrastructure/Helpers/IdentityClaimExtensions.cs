﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace PhotoAlbum.Infrastructure.Helpers
{
    /// <summary>
    ///     Represents identity claim extensions.
    /// </summary>
    public static class IdentityClaimExtensions
    {
        /// <summary>
        ///     Gets claims.
        /// </summary>
        /// <param name="identity">Identity.</param>
        /// <param name="claimName">Claims name.</param>
        /// <returns>Claims value.</returns>
        public static string GetClaim(this IIdentity identity, string claimName)
        {
            if (!(identity is ClaimsIdentity claimsIdentity))
            {
                return string.Empty;
            }

            return claimsIdentity.Claims.FirstOrDefault(claim => claim.Type == claimName)?.Value;
        }
    }
}