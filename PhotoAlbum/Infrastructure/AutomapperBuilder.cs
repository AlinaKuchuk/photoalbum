﻿using AutoMapper;
using PhotoAlbum.Services.Infrastructure.MappingProfiles;
using AlbumMappingProfile = PhotoAlbum.MappingProfiles.AlbumMappingProfile;
using CommentMappingProfile = PhotoAlbum.MappingProfiles.CommentMappingProfile;
using PhotoMetadataMappingProfile = PhotoAlbum.MappingProfiles.PhotoMetadataMappingProfile;
using PostMappingProfile = PhotoAlbum.MappingProfiles.PostMappingProfile;
using ServiceUserMappingProfile = PhotoAlbum.Services.Infrastructure.MappingProfiles.UserMappingProfile;
using ServiceAlbumMappingProfile = PhotoAlbum.Services.Infrastructure.MappingProfiles.AlbumMappingProfile;
using ServicePhotoMetadataMappingProfile =
    PhotoAlbum.Services.Infrastructure.MappingProfiles.PhotoMetadataMappingProfile;
using ServicePostMetadataMappingProfile = PhotoAlbum.Services.Infrastructure.MappingProfiles.PostMappingProfile;
using ServiceCommentMappingProfile = PhotoAlbum.Services.Infrastructure.MappingProfiles.CommentMappingProfile;
using UserMappingProfile = PhotoAlbum.MappingProfiles.UserMappingProfile;

namespace PhotoAlbum.Infrastructure
{
    /// <summary>
    ///     Represents automapper builder.
    /// </summary>
    public static class AutomapperBuilder
    {
        /// <summary>
        ///     Creates an instance of the automapper.
        /// </summary>
        /// <returns></returns>
        public static IMapper BuildMapper()
        {
            var configurationExpression = new MapperConfiguration(config =>
            {
                config.AddProfile<UserMappingProfile>();
                config.AddProfile<ServiceUserMappingProfile>();

                config.AddProfile<AlbumMappingProfile>();
                config.AddProfile<PhotoMetadataMappingProfile>();
                config.AddProfile<PostMappingProfile>();
                config.AddProfile<CommentMappingProfile>();
                config.AddProfile<ExceptionDataMappingProfile>();

                config.AddProfile<ServiceAlbumMappingProfile>();
                config.AddProfile<ServicePhotoMetadataMappingProfile>();
                config.AddProfile<ServicePostMetadataMappingProfile>();
                config.AddProfile<ServiceCommentMappingProfile>();
            });

            return configurationExpression.CreateMapper();
        }
    }
}