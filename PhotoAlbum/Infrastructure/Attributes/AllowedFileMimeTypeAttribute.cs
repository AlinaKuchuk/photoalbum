﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhotoAlbum.Infrastructure.Attributes
{
    /// <summary>
    ///     Represents custom validation attribute for allowed file mime type.
    /// </summary>
    public class AllowedFileMimeTypeAttribute : ValidationAttribute
    {
        private readonly string[] _allowed;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AllowedFileMimeTypeAttribute" /> class.
        /// </summary>
        /// <param name="allowed">Allowed mime types.</param>
        public AllowedFileMimeTypeAttribute(string[] allowed)
        {
            _allowed = allowed;
        }

        /// <summary>
        ///     Check if object has allowed mime type.
        /// </summary>
        /// <param name="value">Object to be checked.</param>
        /// <returns>True if object is not null, otherwise false.</returns>
        public override bool IsValid(object value)
        {
            if (value is null)
            {
                return true;
            }

            if (!(value is HttpPostedFileBase httpPostedFileBase))
            {
                return false;
            }

            var mimeType = httpPostedFileBase.ContentType;

            return _allowed.Contains(mimeType);
        }
    }
}