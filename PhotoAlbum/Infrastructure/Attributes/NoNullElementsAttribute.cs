﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PhotoAlbum.Infrastructure.Attributes
{
    /// <summary>
    ///     Represents custom validation filter that object not null or enumeration has one or more elements.
    /// </summary>
    public sealed class NoNullElementsAttribute : ValidationAttribute
    {
        /// <summary>
        ///     Check if object not null or enumeration has one or more elements
        /// </summary>
        /// <param name="value">Object to be checked.</param>
        /// <returns>True if object is not null, otherwise false.</returns>
        public override bool IsValid(object value)
        {
            if (!(value is IEnumerable enumerable))
            {
                return true;
            }

            return enumerable.Cast<object>().Any(enumerableItem => enumerableItem != null);
        }
    }
}