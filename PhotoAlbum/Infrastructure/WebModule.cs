﻿using System.Web;
using AutoMapper;
using Microsoft.Owin.Security;
using Ninject.Modules;
using Ninject.Web.Common;

namespace PhotoAlbum.Infrastructure
{
    /// <summary>
    ///     Represents web module.
    /// </summary>
    public sealed class WebModule : NinjectModule
    {
        /// <summary>Loads the module into the kernel.</summary>
        public override void Load()
        {
            Bind<IMapper>()
                .ToMethod(context => AutomapperBuilder.BuildMapper())
                .InSingletonScope();

            Bind<IAuthenticationManager>().ToMethod(
                context =>
                    HttpContext.Current.GetOwinContext().Authentication).InRequestScope();
        }
    }
}