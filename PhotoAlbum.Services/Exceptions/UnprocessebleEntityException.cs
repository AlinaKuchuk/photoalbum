﻿using System;
using System.Runtime.Serialization;

namespace PhotoAlbum.Services.Exceptions
{
    /// <summary>
    ///     Represents an error is unprocessable.
    /// </summary>
    [Serializable]
    public class UnprocessableEntityException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="UnprocessableEntityException" /> class.
        /// </summary>
        public UnprocessableEntityException()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UnprocessableEntityException" /> class.
        /// </summary>
        /// <param name="message">Descriptive error message.</param>
        public UnprocessableEntityException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UnprocessableEntityException" /> class.
        /// </summary>
        /// <param name="message">Descriptive message.</param>
        /// <param name="inner">Inner exception.</param>
        public UnprocessableEntityException(string message, Exception inner) : base(message, inner)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UnprocessableEntityException" /> class.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The streaming context.</param>
        protected UnprocessableEntityException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}