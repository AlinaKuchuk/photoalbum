﻿using System;
using System.Runtime.Serialization;

namespace PhotoAlbum.Services.Exceptions
{
    /// <summary>
    ///     Represents an exceptions the error resource already exists.
    /// </summary>
    [Serializable]
    internal class EntityAlreadyExistsException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityAlreadyExistsException" /> class.
        /// </summary>
        internal EntityAlreadyExistsException()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityAlreadyExistsException" /> class.
        /// </summary>
        /// <param name="message">Descriptive error message.</param>
        internal EntityAlreadyExistsException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityAlreadyExistsException" /> class.
        /// </summary>
        /// <param name="message">The exception descriptive message.</param>
        /// <param name="innerException">The inner exception.</param>
        internal EntityAlreadyExistsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityAlreadyExistsException" /> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected EntityAlreadyExistsException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}