﻿using System;
using System.Runtime.Serialization;

namespace PhotoAlbum.Services.Exceptions
{
    /// <summary>
    ///     Represents an error occurs by invalid credentials.
    /// </summary>
    [Serializable]
    public class InvalidCredentialsException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InvalidCredentialsException" /> class.
        /// </summary>
        public InvalidCredentialsException()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="InvalidCredentialsException" /> class.
        /// </summary>
        /// <param name="message">Descriptive error message.</param>
        public InvalidCredentialsException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="InvalidCredentialsException" /> class.
        /// </summary>
        /// <param name="message">The exception descriptive message.</param>
        /// <param name="inner">The inner exception.</param>
        public InvalidCredentialsException(string message, Exception inner) : base(message, inner)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="InvalidCredentialsException" /> class.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The streaming context.</param>
        protected InvalidCredentialsException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}