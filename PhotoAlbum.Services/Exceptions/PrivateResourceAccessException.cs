﻿using System;
using System.Runtime.Serialization;

namespace PhotoAlbum.Services.Exceptions
{
    /// <summary>
    ///     Represents exception private resource accessing.
    /// </summary>
    [Serializable]
    public class PrivateResourceAccessingException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PrivateResourceAccessingException" /> class.
        /// </summary>
        public PrivateResourceAccessingException()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PrivateResourceAccessingException" /> class.
        /// </summary>
        /// <param name="message">Descriptive message.</param>
        public PrivateResourceAccessingException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PrivateResourceAccessingException" /> class.
        /// </summary>
        /// <param name="message">Descriptive error message.</param>
        /// <param name="inner">Inner exception.</param>
        public PrivateResourceAccessingException(string message, Exception inner) : base(message, inner)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PrivateResourceAccessingException" /> class.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Streaming context.</param>
        protected PrivateResourceAccessingException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}