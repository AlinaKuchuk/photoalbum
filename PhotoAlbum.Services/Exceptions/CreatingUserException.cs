﻿using System;
using System.Runtime.Serialization;

namespace PhotoAlbum.Services.Exceptions
{
    /// <summary>
    ///     Represents an error occurs on user creating.
    /// </summary>
    [Serializable]
    public class CreatingUserException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CreatingUserException" /> class.
        /// </summary>
        public CreatingUserException()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CreatingUserException" /> class.
        /// </summary>
        /// <param name="message">Descriptive error message.</param>
        public CreatingUserException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CreatingUserException" /> class.
        /// </summary>
        /// <param name="message">Descriptive message.</param>
        /// <param name="inner">Inner exception.</param>
        public CreatingUserException(string message, Exception inner) : base(message, inner)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CreatingUserException" /> class.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The streaming context.</param>
        protected CreatingUserException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}