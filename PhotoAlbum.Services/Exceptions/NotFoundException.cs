﻿using System;
using System.Runtime.Serialization;

namespace PhotoAlbum.Services.Exceptions
{
    /// <summary>
    ///     Represents not found exception.
    /// </summary>
    [Serializable]
    public class NotFoundException : Exception
    {
        /// <summary>
        ///     Initializing an instance of the <see cref="NotFoundException" /> class.
        /// </summary>
        public NotFoundException()
        {
        }

        /// <summary>
        ///     Initializing an instance of the <see cref="NotFoundException" /> class.
        /// </summary>
        /// <param name="message">Descriptive error message.</param>
        public NotFoundException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Initializing an instance of the <see cref="NotFoundException" /> class.
        /// </summary>
        /// <param name="message">Descriptive error message.</param>
        /// <param name="inner">Inner exception.</param>
        public NotFoundException(string message, Exception inner) : base(message, inner)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NotFoundException" /> class.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Streaming context.</param>
        protected NotFoundException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}