﻿using System;
using System.Runtime.Serialization;

namespace PhotoAlbum.Services.Exceptions
{
    /// <summary>
    ///     Represents an error occurs on updating item.
    /// </summary>
    [Serializable]
    public class UpdatingItemException : Exception
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="UpdatingItemException" /> class.
        /// </summary>
        public UpdatingItemException()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UpdatingItemException" /> class.
        /// </summary>
        /// <param name="message">Descriptive error message.</param>
        public UpdatingItemException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UpdatingItemException" /> class.
        /// </summary>
        /// <param name="message">Descriptive message.</param>
        /// <param name="inner">Inner exception.</param>
        public UpdatingItemException(string message, Exception inner) : base(message, inner)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UpdatingItemException" /> class.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The streaming context.</param>
        protected UpdatingItemException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
