﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using PhotoAlbum.Services.Interfaces;

namespace PhotoAlbum.Services.Filters
{
    /// <summary>
    ///     Represents a filter pipeline builder.
    /// </summary>
    /// <typeparam name="TEntity">Type of entity.</typeparam>
    public class FilterPipelineBuilder<TEntity>
    {
        protected ICollection<IFilterPipe<TEntity>> FilterPipes = new List<IFilterPipe<TEntity>>();

        /// <summary>
        ///     Collects filters expression using <cod>AND</cod> operator.
        /// </summary>
        /// <returns>A filter expression.</returns>
        public virtual Expression<Func<TEntity, bool>> Build()
        {
            return FilterPipes
                .Select(x => x.BuildFilterExpression())
                .Aggregate(
                    (previous, next) => previous.And(next));
        }

        /// <summary>
        ///     Adds a filter pipe to filter pipeline.
        /// </summary>
        /// <param name="pipe">A filter pipe to adding.</param>
        /// <returns>The filter pipeline filter pipe was added to.</returns>
        public virtual FilterPipelineBuilder<TEntity> WithPipe(IFilterPipe<TEntity> pipe)
        {
            if (pipe != null)
            {
                FilterPipes.Add(pipe);
            }

            return this;
        }
    }
}