﻿using System;
using System.Linq.Expressions;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.Services.Interfaces;

namespace PhotoAlbum.Services.Filters.Users
{
    /// <summary>
    ///     Represents user name filter pipe.
    /// </summary>
    public class UserNameFilterPipe : IFilterPipe<User>
    {
        private readonly string _userName;

        /// <summary>
        ///     Initializes a new instance of the <see cref="UserNameFilterPipe" /> class.
        /// </summary>
        /// <param name="userName">Filter user name.</param>
        public UserNameFilterPipe(string userName)
        {
            _userName = userName;
        }

        /// <inheritdoc />
        public Expression<Func<User, bool>> BuildFilterExpression()
        {
            if (string.IsNullOrEmpty(_userName))
            {
                return user => true;
            }

            return user => user.Email.Contains(_userName);
        }
    }
}