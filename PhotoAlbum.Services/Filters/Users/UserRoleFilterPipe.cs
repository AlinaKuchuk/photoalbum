﻿using System;
using System.Linq;
using System.Linq.Expressions;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.Services.Interfaces;

namespace PhotoAlbum.Services.Filters.Users
{
    /// <summary>
    /// Represents user role filter pipeline.
    /// </summary>
    public sealed class UserRoleFilterPipe : IFilterPipe<User>
    {
        private readonly string _userRole;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRoleFilterPipe"/> class.
        /// </summary>
        /// <param name="userRole">User role.</param>
        public UserRoleFilterPipe(string userRole)
        {
            _userRole = userRole;
        }

        /// <inheritdoc />
        public Expression<Func<User, bool>> BuildFilterExpression()
        {
            if (string.IsNullOrEmpty(_userRole))
            {
                return user => true;
            }

            return user => user.Roles.Any(role => role.RoleId == _userRole);
        }
    }
}