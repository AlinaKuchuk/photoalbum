﻿using System;
using System.Linq.Expressions;

namespace PhotoAlbum.Services.Filters
{
    /// <summary>
    ///     Contains extensions methods to the <see cref="Expression{TDelegate}" /> class.
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        ///     Apply and operator to expression trees.
        /// </summary>
        /// <typeparam name="TEntity">Type of entity.</typeparam>
        /// <param name="left">Left expression.</param>
        /// <param name="right">Right expression.</param>
        /// <returns>Chained expression.</returns>
        public static Expression<Func<TEntity, bool>> And<TEntity>(this Expression<Func<TEntity, bool>> left,
            Expression<Func<TEntity, bool>> right)
        {
            return CombineLambdas(left, right, ExpressionType.AndAlso);
        }

        /// <summary>
        ///     Applies or operator to expression trees.
        /// </summary>
        /// <typeparam name="TEntity">Type of entity.</typeparam>
        /// <param name="left">Left expression.</param>
        /// <param name="right">Right expression.</param>
        /// <returns>Combined expression.</returns>
        public static Expression<Func<TEntity, bool>> Or<TEntity>(this Expression<Func<TEntity, bool>> left,
            Expression<Func<TEntity, bool>> right)
        {
            return CombineLambdas(left, right, ExpressionType.OrElse);
        }

        private static Expression<Func<TEntity, bool>> CombineLambdas<TEntity>(
            this Expression<Func<TEntity, bool>> left,
            Expression<Func<TEntity, bool>> right, ExpressionType expressionType)
        {
            if (IsExpressionBodyConstant(left))
            {
                return right;
            }

            var leftParameter = left.Parameters[0];

            var visitor = new SubstituteParameterVisitor {Sub = {[right.Parameters[0]] = leftParameter}};

            Expression body = Expression.MakeBinary(expressionType, left.Body, visitor.Visit(right.Body));
            return Expression.Lambda<Func<TEntity, bool>>(body, leftParameter);
        }

        private static bool IsExpressionBodyConstant<TEntity>(Expression<Func<TEntity, bool>> left)
        {
            return left.Body.NodeType == ExpressionType.Constant;
        }
    }
}