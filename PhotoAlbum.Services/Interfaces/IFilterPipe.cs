﻿using System;
using System.Linq.Expressions;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides an interface for the filtering pipe.
    /// </summary>
    public interface IFilterPipe<TEntity>
    {
        /// <summary>
        ///     Builds filter expression.
        /// </summary>
        Expression<Func<TEntity, bool>> BuildFilterExpression();
    }
}