﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;
using PhotoAlbum.Services.Models.Album;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides an interface for album service.
    /// </summary>
    public interface IAlbumService
    {
        /// <summary>
        ///     Creates album.
        /// </summary>
        /// <param name="createAlbumDto">The album to create.</param>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<AlbumDto> CreateAlbumAsync(CreateAlbumDto createAlbumDto, string ownerId);

        /// <summary>
        ///     Gets user albums.
        /// </summary>
        /// <param name="requestingUserPrincipal"></param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<IEnumerable<AlbumDto>> GetUserAlbumsAsync(IPrincipal requestingUserPrincipal);

        /// <summary>
        ///     Gets user albums.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<UserAlbumsDto> GetUserAlbumsAsync(GetUserAlbumsQueryParametersDto queryParameters);

        /// <summary>
        ///     Gets album photos.
        /// </summary>
        /// <param name="queryParameters">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<FullAlbumDto> GetFullAlbumAsync(GetAlbumQueryParametersDto queryParameters);

        /// <summary>
        ///     Gets album by identifier.
        /// </summary>
        /// <param name="albumId">Album identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<AlbumDto> GetAlbumAsync(int albumId);

        /// <summary>
        ///     Edit album.
        /// </summary>
        /// <param name="albumInformation">New album information.</param>
        /// <param name="requestingUserId">Requesting user identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task EditAlbumAsync(EditAlbumDto albumInformation, string requestingUserId);
    }
}