﻿using System.Threading.Tasks;
using PhotoAlbum.Services.Models.Comments;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides interface for comment services.
    /// </summary>
    public interface ICommentService
    {
        /// <summary>
        ///     Creates comments.
        /// </summary>
        /// <param name="createComment">Comment to create.</param>
        /// <param name="authorId">Author identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<CommentDto> CreateCommentAsync(CreateCommentDto createComment, string authorId);
    }
}