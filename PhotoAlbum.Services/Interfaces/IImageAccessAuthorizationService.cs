﻿using System;
using System.Security.Principal;
using System.Threading.Tasks;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides an interface for the image access authorization service. 
    /// </summary>
    public interface IImageAccessAuthorizationService
    {
        /// <summary>
        /// Preforms checking permission for downloading image.
        /// </summary>
        /// <param name="imageId">Image identifier.</param>
        /// <param name="requestingUserPrincipal">Requesting user principal.</param>
        /// <returns>A <see cref="Task"/> representing asynchronous unit test.</returns>
        void CheckPermissions(Guid imageId, IPrincipal requestingUserPrincipal);
    }
}