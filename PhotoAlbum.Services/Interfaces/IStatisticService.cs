﻿using System.Threading.Tasks;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides interface for statistic service.
    /// </summary>
    public interface IStatisticService
    {
        /// <summary>
        ///     Gets posts statistic.
        /// </summary>
        /// <param name="requestingUserId">User identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<PostsStatisticsDto> GetPostsStatisticAsync(string requestingUserId);
    }
}