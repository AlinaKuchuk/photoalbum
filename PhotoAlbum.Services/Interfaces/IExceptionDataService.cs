﻿using System.Threading.Tasks;
using PhotoAlbum.Services.Models;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides an interface for exception logger.
    /// </summary>
    public interface IExceptionDataService
    {
        /// <summary>
        ///     Creates exception data.
        /// </summary>
        /// <param name="createExceptionData">Create exception data.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task SaveExceptionDataAsync(CreateExceptionDataDto createExceptionData);
    }
}