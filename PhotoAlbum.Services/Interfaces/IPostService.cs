﻿using System.Threading.Tasks;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides an interface for post service
    /// </summary>
    public interface IPostService
    {
        /// <summary>
        ///     Creates post.
        /// </summary>
        /// <param name="createPost">Post to be created.</param>
        /// <param name="ownerId">Owner identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<FullPostDto> CreatePostAsync(CreatePostDto createPost, string ownerId);

        /// <summary>
        ///     Gets full post.
        /// </summary>
        /// <param name="queryParameters">Query post parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<FullPostDto> GetFullPostAsync(GetFullPostQueryParametersDto queryParameters);

        /// <summary>
        ///     Retrieves all posts that available for accessing in the application.
        /// </summary>
        /// <param name="queryParametersDto">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<PaginatedDataDto<PreviewPostAggregatedDto>> GetExplorePostsAsync(
            GetFollowingPostsQueryParametersDto queryParametersDto);

        /// <summary>
        ///     Gets posts of users that users follows, applying query parameters.
        /// </summary>
        /// <param name="queryParametersDto">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<PaginatedDataDto<PreviewPostAggregatedDto>> GetFollowingUpdatesAsync(
            GetFollowingPostsQueryParametersDto queryParametersDto);

        /// <summary>
        ///     Add a like on the post by user identifier.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="postId">Post identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task LikePostByUserAsync(string userId, int postId);

        /// <summary>
        ///     Delete a like from the post by user identifier.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="postId">Post identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task UnlikePostByUserAsync(string userId, int postId);
    }
}