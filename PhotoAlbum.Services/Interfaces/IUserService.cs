﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides an interface for the user service.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        ///     Creates user.
        /// </summary>
        /// <param name="createUser">The user to create.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task CreateUserAsync(CreateUserDto createUser);

        /// <summary>
        ///     Logins user.
        /// </summary>
        /// <param name="credentials">User credentials.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task LoginUserAsync(UserCredentialsDto credentials);

        /// <summary>
        ///     Retrieves users by parameters.
        /// </summary>
        /// <param name="parameters">User getting parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<IEnumerable<UserDto>> GetUsersAsync(GetUsersParametersDto parameters);

        /// <summary>
        ///     Retrieves user by identifier.
        /// </summary>
        /// <param name="id">User identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<UserDto> GetUserAsync(string id);

        /// <summary>
        ///     Updates user.
        /// </summary>
        /// <param name="user">User to update.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task UpdateUserAsync(UserDto user);

        /// <summary>
        ///     Retrieves full information of user by parameters.
        /// </summary>
        /// <param name="queryParameters">Get full user query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<FullUserDto> GetFullUserAsync(GetFullUserQueryParametersDto queryParameters);

        /// <summary>
        ///     Subscribes a user to another user.
        /// </summary>
        /// <param name="userId">User who subscribes.</param>
        /// <param name="followingUserId">User to following.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task StartFollowAsync(string userId, string followingUserId);

        /// <summary>
        ///     Unsubscribes a user from another user.
        /// </summary>
        /// <param name="userId">User who unsubscribes.</param>
        /// <param name="followingUserId">User to unsubscribes.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task StopFollowAsync(string userId, string followingUserId);

        /// <summary>
        ///     Edits user profile.
        /// </summary>
        /// <param name="profile">Profile to edit.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task EditProfileAsync(EditUserProfileDto profile);

        /// <summary>
        ///     Log out of user account.
        /// </summary>
        void Logout();

        /// <summary>
        ///     Performs updating user role, set it to business.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task MakeUserBusinessAsync(string userId);

        /// <summary>
        ///     Performs updating user, set it private.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task MakeUserPrivateAsync(string userId);

        /// <summary>
        ///     Performs updating user, set it not private.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task MakeUserNotPrivateAsync(string userId);

        /// <summary>
        ///     Gets user followers.
        /// </summary>
        /// <param name="queryParametersDto">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<PaginatedDataDto<UserDto>> GetFollowersAsync(GetFollowersOrFollowingQueryParametersDto queryParametersDto);

        /// <summary>
        ///     Gets user following.
        /// </summary>
        /// <param name="queryParametersDto">Query parameters.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<PaginatedDataDto<UserDto>> GetFollowingAsync(GetFollowersOrFollowingQueryParametersDto queryParametersDto);
    }
}