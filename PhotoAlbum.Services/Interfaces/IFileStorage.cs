﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace PhotoAlbum.Services.Interfaces
{
    /// <summary>
    ///     Provides interface for file storage.
    /// </summary>
    public interface IFileStorage
    {
        /// <summary>
        ///     Saves file.
        /// </summary>
        /// <param name="stream">Stream.</param>
        /// <param name="mimeType">Mime type.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<Guid> SaveFileAsync(Stream stream, string mimeType);

        /// <summary>
        ///     Gets file string.
        /// </summary>
        /// <param name="id">File identifier.</param>
        /// <returns>A <see cref="Task" /> representing asynchronous operation.</returns>
        Task<byte[]> GetFileStringAsync(Guid id);
    }
}