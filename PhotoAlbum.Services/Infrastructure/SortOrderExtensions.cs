﻿using System.Linq;
using PhotoAlbum.DataAccess.Models.Enums;

namespace PhotoAlbum.Services.Infrastructure
{
    /// <summary>
    ///     Contains extensions methods to the <see cref="string" />.
    /// </summary>
    public static class SortOrderExtensions
    {
        /// <summary>
        ///     Converts string parameter to tuple sort info.
        /// </summary>
        /// <param name="sortParameter"></param>
        /// <returns></returns>
        public static (string field, SortOrder order) ToSortOrder(this string sortParameter)
        {
            var parametersParts = sortParameter.Split(':');
            var field = parametersParts.First();

            switch (parametersParts.Last())
            {
                case "desc":
                    return (field, SortOrder.Desc);
                default:
                    return (field, SortOrder.Asc);
            }
        }
    }
}