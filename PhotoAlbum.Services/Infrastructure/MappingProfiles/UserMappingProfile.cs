﻿using System.Linq;
using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.QueryParameters;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.Services.Infrastructure.MappingProfiles
{
    /// <summary>
    ///     Represents user mapping profile.
    /// </summary>
    public class UserMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="UserMappingProfile" /> class.
        /// </summary>
        public UserMappingProfile()
        {
            CreateMap<User, UserDto>()
                .ForMember(dest => dest.Roles, opt => opt.Ignore())
                .ReverseMap()
                .ForMember(dest => dest.ProfilePhotoId, opt => opt.Ignore())
                .ForMember(dest => dest.Roles, opt => opt.Ignore());
            CreateMap<User, CreateUserDto>();
            CreateMap<Role, RoleDto>();

            // Full User
            CreateMap<FullUserAggregatedModel, FullUserDto>()
                .ForMember(
                    dest => dest.Id,
                    opt => opt.MapFrom(src => src.User.Id))
                .ForMember(
                    dest => dest.FirstName,
                    opt => opt.MapFrom(src => src.User.FirstName))
                .ForMember(
                    dest => dest.LastName,
                    opt => opt.MapFrom(src => src.User.LastName))
                .ForMember(
                    dest => dest.IsPrivate,
                    opt => opt.MapFrom(src => src.User.IsPrivate ))
                .ForMember(
                    dest => dest.Email,
                    opt => opt.MapFrom(src => src.User.Email))
                .ForMember(
                    dest => dest.Albums,
                    opt => opt.MapFrom(src => src.Albums))
                .ForMember(
                    dest => dest.ProfilePhotoId,
                    opt => opt.MapFrom(src => src.User.ProfilePhotoId))
                .ForMember(
                    dest => dest.Biography,
                    opt => opt.MapFrom(src => src.User.Biography));
            
            CreateMap<GetFullUserQueryParametersDto, DbGetFullUserQueryParameters>();


            CreateMap<UserWithRolesAggregatedModel, UserDto>()
                .ForMember(dest => dest.Roles, 
                    opt => opt.MapFrom( src => src.Roles.Select(role => role.Name)))
                .IncludeMembers(user => user.User);

            // Flattening
            CreateMap<User, UserDto>(MemberList.None);
            CreateMap<GetFollowersOrFollowingQueryParametersDto, DbGetFollowersOrFollowingQueryParameters>();
            CreateMap<DbPaginationData<User>, PaginatedDataDto<UserDto>>();
        }
    }
}