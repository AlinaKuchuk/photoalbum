﻿using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.Services.Models;

namespace PhotoAlbum.Services.Infrastructure.MappingProfiles
{
    /// <summary>
    ///     Represents exception data mapping profile.
    /// </summary>
    public sealed class ExceptionDataMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionDataMappingProfile" /> class.
        /// </summary>
        public ExceptionDataMappingProfile()
        {
            CreateMap<CreateExceptionDataDto, ExceptionData>();
        }
    }
}