﻿using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Services.Infrastructure.MappingProfiles
{
    /// <summary>
    ///     Represents album mapping profile.
    /// </summary>
    public sealed class PhotoMetadataMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PhotoMetadataMappingProfile" /> class.
        /// </summary>
        public PhotoMetadataMappingProfile()
        {
            CreateMap<PhotoMetadata, PhotoMetadataPreviewDto>();
            CreateMap<DbPaginationData<PhotoMetadata>, PaginatedDataDto<PhotoMetadataPreviewDto>>();
        }
    }
}