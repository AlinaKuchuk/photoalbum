﻿using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.QueryParameters;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Album;

namespace PhotoAlbum.Services.Infrastructure.MappingProfiles
{
    /// <summary>
    ///     Represents album mapping profile.
    /// </summary>
    public sealed class AlbumMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AlbumMappingProfile" /> class.
        /// </summary>
        public AlbumMappingProfile()
        {
            CreateMap<Album, AlbumDto>();
            CreateMap<CreateAlbumDto, Album>();
            CreateMap<UserAlbumsAggregatedModel, UserAlbumsDto>();
            CreateMap<DbPaginationData<PreviewAlbumAggregatedModel>, PaginatedDataDto<AlbumDto>>();
            CreateMap<PreviewAlbumAggregatedModel, AlbumDto>()
                .ForMember(dest => dest.Id,
                    opt => opt.MapFrom(src => src.Album.Id))
                .ForMember(dest => dest.Owner,
                    opt => opt.MapFrom(src => src.Album.Owner))
                .ForMember(dest => dest.IsPrivate,
                    opt => opt.MapFrom(src => src.Album.IsPrivate))
                .ForMember(dest => dest.Title,
                    opt => opt.MapFrom(src => src.Album.Title))
                .ForMember(dest => dest.Description,
                    opt => opt.MapFrom(src => src.Album.Description))
                .ForMember(dest => dest.PreviewPhotoId,
                    opt => opt.MapFrom(src => src.AlbumPreviewPhotoId));

            CreateMap<GetUserAlbumsQueryParametersDto, DbGetUserAlbumsQueryParameters>();
            CreateMap<FullAlbumAggregatedModel, FullAlbumDto>();
            CreateMap<GetAlbumQueryParametersDto, DbGetAlbumPhotosQueryParameters>();
        }
    }
}