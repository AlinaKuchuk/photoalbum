﻿using System.Collections.Generic;
using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Models.QueryParameters;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Comments;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Services.Infrastructure.MappingProfiles
{
    /// <summary>
    ///     Represents album mapping profile.
    /// </summary>
    public sealed class PostMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PostMappingProfile" /> class.
        /// </summary>
        public PostMappingProfile()
        {
            CreateMap<GetPaginationQueryParametersDto, DbPaginationQueryParameters>();
            CreateMap<Post, PostPreviewDto>();

            CreateMap<DbPaginationData<Post>, PaginatedDataDto<PostPreviewDto>>();

            CreateMap<Post, FullPostDto>()
                .ForMember(dest => dest.Comments, opt => opt.Ignore());
            CreateMap<CreatePostDto, Post>()
                .ForMember(dest => dest.Photos, opt => opt.MapFrom(src => new List<PhotoMetadata>()));

            CreateMap<DbPaginationData<Comment>, PaginatedDataDto<CommentDto>>();

            CreateMap<FullPostAggregatedModel, FullPostDto>()
                .ForMember(
                    dest => dest.Comments,
                    opt => opt.MapFrom(src => src.Comments))
                .ForMember(dest => dest.LikeCount,
                    opt => opt.MapFrom(src => src.LikesCount))
                .ForMember(dest => dest.Album,
                    opt => opt.MapFrom(src => src.Post.Album))
                .ForMember(dest => dest.Id,
                    opt => opt.MapFrom(src => src.Post.Id))
                .ForMember(dest => dest.Photos,
                    opt => opt.MapFrom(src => src.Post.Photos))
                .ForMember(dest => dest.Author,
                    opt => opt.MapFrom(src => src.Post.Album.Owner))
                .ForMember(dest => dest.Location,
                    opt => opt.MapFrom(src => src.Post.Location))
                .ForMember(dest => dest.IsLiked,
                    opt => opt.MapFrom(src => src.IsLiked))
                .ForMember(dest => dest.Description,
                    opt => opt.MapFrom(src => src.Post.Description));

            CreateMap<GetPaginationQueryParametersDto, DbPaginationQueryParameters>();
            CreateMap<PreviewPostAggregatedModel, PreviewPostAggregatedDto>();
            CreateMap<PostsStatistic, PostsStatisticsDto>();
            CreateMap<DbPaginationData<PreviewPostAggregatedModel>, PaginatedDataDto<PreviewPostAggregatedDto>>();
        }
    }
}