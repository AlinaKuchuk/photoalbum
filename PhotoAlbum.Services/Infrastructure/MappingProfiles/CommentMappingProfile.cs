﻿using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.Services.Models.Comments;

namespace PhotoAlbum.Services.Infrastructure.MappingProfiles
{
    /// <summary>
    ///     Represents comment mapping profile.
    /// </summary>
    public sealed class CommentMappingProfile : Profile
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CommentMappingProfile" /> class.
        /// </summary>
        public CommentMappingProfile()
        {
            CreateMap<Comment, CommentDto>();
            CreateMap<CreateCommentDto, Comment>();
        }
    }
}