﻿using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using PhotoAlbum.DataAccess;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Repositories;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Services;

namespace PhotoAlbum.Services.Infrastructure
{
    /// <summary>
    ///     Represents service DI module.
    /// </summary>
    public sealed class ServicesModule : NinjectModule
    {
        /// <summary>Loads the module into the kernel.</summary>
        public override void Load()
        {
            Bind(typeof(IGenericRepository<,>))
                .To(typeof(GenericRepository<,>));

            Bind<IRoleRepository>()
                .ToMethod(context =>
                    new RoleRepository(
                        new RoleStore<IdentityRole>((DbContext) context.Kernel.Get<IPhotoAlbumContext>())));

            Bind<IUserRepository>()
                .ToMethod(context =>
                    new UserRepository(
                        new UserStore<User>((DbContext) context.Kernel.Get<IPhotoAlbumContext>()),
                        context.Kernel.Get<IPhotoAlbumContext>()));

            Bind<IRoleStore<IdentityRole, string>>()
                .To<RoleStore<IdentityRole>>();

            Bind<IExceptionDataService>()
                .To<ExceptionDataService>();

            Bind<IPostRepository>()
                .To<PostRepository>();

            Bind<IAlbumRepository>()
                .To<AlbumRepository>();

            Bind<IPhotoAlbumContext>()
                .To<PhotoAlbumContext>()
                .InRequestScope();

            Bind<IUnitOfWork>()
                .To<UnitOfWork>();

            Bind<IUserService>()
                .To<UserService>();

            Bind<IAlbumService>()
                .To<AlbumService>();

            Bind<ICommentService>()
                .To<CommentService>();

            Bind<IStatisticService>()
                .To<StatisticService>();

            Bind<IImageAccessAuthorizationService>()
                .To<ImageAccessAuthorizationService>();

            Bind<IFileStorage>()
                .ToMethod(_ =>
                    new LocalDirectoryFileStorage(
                        @"C:\Users\alina\source\repos\PhotoAlbum\PhotoAlbum\App_Data\Photos"));

            Bind<IPostService>()
                .To<PostService>();
        }
    }
}