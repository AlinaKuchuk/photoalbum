﻿using System.Threading.Tasks;
using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.Services.Exceptions;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models.Comments;
using PhotoAlbum.Services.Resources;

namespace PhotoAlbum.Services.Services
{
    /// <summary>
    ///     Represents comments service.
    /// </summary>
    public sealed class CommentService : ICommentService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CommentService" /> class.
        /// </summary>
        /// <param name="unitOfWork">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        public CommentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<CommentDto> CreateCommentAsync(CreateCommentDto createComment, string authorId)
        {
            var requestedPost = await _unitOfWork.PostRepository.GetByIdAsync(
                createComment.PostId,
                post => post.Album,
                post => post.Album.Owner);

            if (requestedPost == null)
            {
                throw new NotFoundException(ErrorMessagesResources.PostNotFound);
            }

            if (requestedPost.Album.IsPrivate && requestedPost.Album.Owner.Id != authorId)
            {               
                throw new PrivateResourceAccessingException(ErrorMessagesResources.PostIsPrivate);
            }

            if (requestedPost.Album.Owner.IsPrivate && requestedPost.Album.Owner.Id != authorId)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.AuthorAccountIsPrivate);
            }

            var commentToCreate = _mapper.Map<Comment>(createComment);
            commentToCreate.AuthorId = authorId;

            var createdComment = _unitOfWork.CommentRepository.Add(commentToCreate);
            await _unitOfWork.CommitAsync();

            return _mapper.Map<CommentDto>(createdComment);
        }
    }
}