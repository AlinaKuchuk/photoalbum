﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models.QueryParameters;
using PhotoAlbum.Services.Exceptions;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models.Album;
using PhotoAlbum.Services.Models.Enums;
using PhotoAlbum.Services.Resources;

namespace PhotoAlbum.Services.Services
{
    /// <summary>
    ///     Represents album service.
    /// </summary>
    public sealed class AlbumService : IAlbumService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AlbumService" /> class.
        /// </summary>
        /// <param name="unitOfWork">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        public AlbumService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<AlbumDto> CreateAlbumAsync(CreateAlbumDto createAlbumDto, string ownerId)
        {
            var album = _mapper.Map<Album>(createAlbumDto);
            album.OwnerId = ownerId;

            var createdAlbum = _unitOfWork.AlbumRepository.Add(album);
            await _unitOfWork.CommitAsync();

            return _mapper.Map<AlbumDto>(createdAlbum);
        }

        /// <inheritdoc />
        public Task<AlbumDto> GetAlbumAsync(int albumId)
        {
            var targetAlbum = _unitOfWork.AlbumRepository.GetFiltered(album => album.Id == albumId && !album.IsDeleted)
                .FirstOrDefault();

            if (targetAlbum == null)
            {
                throw new NotFoundException(ErrorMessagesResources.AlbumNotFound);
            }

            return Task.FromResult(_mapper.Map<AlbumDto>(targetAlbum));
        }

        // For getting on create post form
        /// <inheritdoc />
        public Task<IEnumerable<AlbumDto>> GetUserAlbumsAsync(IPrincipal requestingUserPrincipal)
        {
            var requestingUserId = requestingUserPrincipal.Identity.GetUserId();
            var albums = _unitOfWork.AlbumRepository
                .GetFiltered(album =>
                    album.OwnerId == requestingUserId &&
                    !album.IsDeleted).ToArray();

            if (albums == null || !albums.Any())
            {
                throw new NotFoundException(ErrorMessagesResources.AlbumNotFound);
            }

            return Task.FromResult(_mapper.Map<IEnumerable<AlbumDto>>(albums));
        }

        // User albums
        /// <inheritdoc />
        public async Task<UserAlbumsDto> GetUserAlbumsAsync(GetUserAlbumsQueryParametersDto queryParameters)
        {
            var userAlbums =
                await _unitOfWork.AlbumRepository.GetUserAlbumsAsync(
                    _mapper.Map<DbGetUserAlbumsQueryParameters>(queryParameters));

            if (!queryParameters.RequestingUserRoles.Contains(nameof(RolesDto.Administrator)) &&
                queryParameters.RequestingUserId != queryParameters.UserId && userAlbums.Owner.IsPrivate)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.AuthorAccountIsPrivate);
            }

            return _mapper.Map<UserAlbumsDto>(userAlbums);
        }

        /// <inheritdoc />
        public async Task<FullAlbumDto> GetFullAlbumAsync(GetAlbumQueryParametersDto queryParameters)
        {
            var albumAggregatedModel =
                await _unitOfWork.AlbumRepository.GetAlbumPhotosAsync(
                    _mapper.Map<DbGetAlbumPhotosQueryParameters>(queryParameters));

            if (albumAggregatedModel == null)
            {
                throw new NotFoundException(ErrorMessagesResources.AlbumNotFound);
            }

            if (!queryParameters.RequestingUserRoles.Contains(nameof(RolesDto.Administrator)) &&
                queryParameters.RequestingUserId != albumAggregatedModel.Album.OwnerId && albumAggregatedModel.Album.Owner.IsPrivate)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.AuthorAccountIsPrivate);
            }

            return _mapper.Map<FullAlbumDto>(albumAggregatedModel);
        }

        /// <inheritdoc />
        public async Task EditAlbumAsync(EditAlbumDto albumInformation, string requestingUserId)
        {
            var albumToUpdate = _unitOfWork.AlbumRepository
                .GetFiltered(album =>
                    album.Id == albumInformation.Id)
                .FirstOrDefault();

            if (albumToUpdate == null)
            {
                throw new UpdatingItemException(ErrorMessagesResources.AlbumNotFound);
            }

            if (albumToUpdate.OwnerId != requestingUserId)
            {
                throw new UnauthorizedAccessException(ErrorMessagesResources.UserIsNotOwner);
            }

            albumToUpdate.Title = albumInformation.Title;
            albumToUpdate.Description = albumInformation.Description;
            albumToUpdate.IsPrivate = albumInformation.IsPrivate;

            _unitOfWork.AlbumRepository.Update(albumToUpdate);
            await _unitOfWork.CommitAsync();
        }
    }
}