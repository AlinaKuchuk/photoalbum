﻿using System;
using System.Linq;
using System.Security.Principal;
using Microsoft.AspNet.Identity;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.Services.Exceptions;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models.Enums;
using PhotoAlbum.Services.Resources;

namespace PhotoAlbum.Services.Services
{
    /// <summary>
    ///     Represents image access authorization service.
    /// </summary>
    public sealed class ImageAccessAuthorizationService : IImageAccessAuthorizationService
    {
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializing a new instance of the <see cref="ImageAccessAuthorizationService" /> class.
        /// </summary>
        /// <param name="unitOfWork">Unit of work.</param>
        public ImageAccessAuthorizationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <inheritdoc />
        public void CheckPermissions(Guid imageId, IPrincipal requestingUserPrincipal)
        {
            var requestingImage = _unitOfWork.PhotoMetadataRepository
                .GetFiltered(
                    metadata => metadata.FileId == imageId,
                    image => image.Post.Album,
                    image => image.Owner)
                .FirstOrDefault();

            if (requestingImage == null)
            {
                return;
            }

            var requestingUserId = requestingUserPrincipal.Identity.GetUserId();

            if (!requestingUserPrincipal.IsInRole(nameof(RolesDto.Administrator)) 
                && requestingImage.Owner.IsPrivate
                && requestingImage.OwnerId != requestingUserId)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.PhotoBelongsToPrivateAccount);
            }

            if (!requestingUserPrincipal.IsInRole(nameof(RolesDto.Administrator)) 
                && requestingImage.Post.Album.IsPrivate 
                && requestingImage.Post.Album.OwnerId != requestingUserId)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.PhotoBelongToPprivateAlbum);
            }
        }
    }
}