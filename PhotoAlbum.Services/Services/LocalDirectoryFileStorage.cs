﻿using System;
using System.IO;
using System.Threading.Tasks;
using PhotoAlbum.Services.Interfaces;

namespace PhotoAlbum.Services.Services
{
    /// <summary>
    /// Represents local file storage.
    /// </summary>
    public sealed class LocalDirectoryFileStorage : IFileStorage
    {
        private readonly string _localDirectoryStoragePath;

        // Prohibit create class by default constructor
        private LocalDirectoryFileStorage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalDirectoryFileStorage" /> class.
        /// </summary>
        /// <param name="localDirectoryStoragePath"></param>
        public LocalDirectoryFileStorage(string localDirectoryStoragePath)
        {
            if (!Directory.Exists(localDirectoryStoragePath))
            {
                throw new DirectoryNotFoundException();
            }

            _localDirectoryStoragePath = localDirectoryStoragePath;
        }

        /// <inheritdoc />
        public async Task<Guid> SaveFileAsync(Stream stream, string mimeType)
        {
            using (stream)
            {
                var fileName = Guid.NewGuid();
                using (var outputFileStream = new FileStream(Path.Combine(_localDirectoryStoragePath, $"{fileName}.{mimeType}"), FileMode.Create))
                {
                    await stream.CopyToAsync(outputFileStream);
                    return fileName;
                }
            }
        }

        /// <inheritdoc />
        public Task<byte[]> GetFileStringAsync(Guid id)
        {
            return Task.FromResult(File.ReadAllBytes(Path.Combine(_localDirectoryStoragePath, $"{id}.jpeg")));
        }
    }
}