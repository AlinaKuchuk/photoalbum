﻿using System.Threading.Tasks;
using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;

namespace PhotoAlbum.Services.Services
{
    /// <summary>
    ///     Represents exception data service.
    /// </summary>
    public sealed class ExceptionDataService : IExceptionDataService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionDataService" /> class.
        /// </summary>
        /// <param name="mapper">Mapper.</param>
        /// <param name="unitOfWork">Unit of work.</param>
        public ExceptionDataService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        /// <inheritdoc />
        public async Task SaveExceptionDataAsync(CreateExceptionDataDto createExceptionData)
        { 
            _unitOfWork.ExceptionDataRepository.Add(_mapper.Map<ExceptionData>(createExceptionData));
            await _unitOfWork.CommitAsync();
        }
    }
}