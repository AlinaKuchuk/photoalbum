﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Services.Services
{
    /// <summary>
    ///     Represents statistic service.
    /// </summary>
    public sealed class StatisticService : IStatisticService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StatisticService" /> class.
        /// </summary>
        /// <param name="unitOfWork">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        public StatisticService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<PostsStatisticsDto> GetPostsStatisticAsync(string requestingUserId)
        {
            var userPosts = _unitOfWork.PostRepository.GetFiltered(post => post.Album.OwnerId == requestingUserId);

            if (!userPosts.Any())
            {
                return new PostsStatisticsDto
                {
                    LikesCount = default,
                    ViewsCount = default,
                };
            }

            return _mapper.Map<PostsStatisticsDto>(
                await _unitOfWork.PostRepository.GetPostsStatisticAsync(requestingUserId));
        }
    }
}