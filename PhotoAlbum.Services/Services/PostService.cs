﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models.QueryParameters;
using PhotoAlbum.Services.Exceptions;
using PhotoAlbum.Services.Infrastructure;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Comments;
using PhotoAlbum.Services.Models.Post;
using PhotoAlbum.Services.Resources;

namespace PhotoAlbum.Services.Services
{
    /// <summary>
    ///     Represents post service.
    /// </summary>
    public sealed class PostService : IPostService
    {
        private readonly IFileStorage _fileStorage;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PostService" /> class.
        /// </summary>
        /// <param name="fileStorage">File storage.</param>
        /// <param name="unitOfWork">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        public PostService(IFileStorage fileStorage, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _fileStorage = fileStorage;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<FullPostDto> CreatePostAsync(CreatePostDto createPost, string ownerId)
        {
            var postToCreate = _mapper.Map<Post>(createPost);
            foreach (var postImageStream in createPost.LoadedImages)
            {
                var savedFileId = await _fileStorage.SaveFileAsync(postImageStream.ImageStream,
                    postImageStream.Extension.Split('/').LastOrDefault() ?? "jpeg");
                var filePhotoMetadata = new PhotoMetadata
                {
                    FileId = savedFileId,
                    OwnerId = ownerId
                };

                postToCreate.Photos.Add(filePhotoMetadata);
            }

            var createdPost = _unitOfWork.PostRepository.Add(postToCreate);
            await _unitOfWork.CommitAsync();

            return _mapper.Map<FullPostDto>(createdPost);
        }

        /// <inheritdoc />
        public async Task<FullPostDto> GetFullPostAsync(
            GetFullPostQueryParametersDto queryParameters)
        {
            var resultPost = await _unitOfWork.PostRepository.GetFullPostAsync(
                queryParameters.PostId,
                queryParameters.RequestingUserId,
                _mapper.Map<DbPaginationQueryParameters>(queryParameters.PaginationParameters),
                queryParameters.RequestingUserRoles);

            if (resultPost == null)
            {
                throw new NotFoundException();
            }

            var postDto = _mapper.Map<FullPostDto>(resultPost.Post);
            postDto.Comments = _mapper.Map<PaginatedDataDto<CommentDto>>(resultPost.Comments);
            postDto.LikeCount = resultPost.LikesCount;
            postDto.IsLiked = resultPost.IsLiked;

            return postDto;
        }

        /// <inheritdoc />
        public async Task<PaginatedDataDto<PreviewPostAggregatedDto>> GetExplorePostsAsync(
            GetFollowingPostsQueryParametersDto queryParametersDto)
        {
            var (field, order) = queryParametersDto.Sort.ToSortOrder();
            var explorePosts = await _unitOfWork.PostRepository.GetExplorePostsAsync(
                queryParametersDto.SearchText,
                queryParametersDto.RequestingUserId,
                _mapper.Map<DbPaginationQueryParameters>(queryParametersDto.PaginationQueryParameters),
                field,
                order);

            return _mapper.Map<PaginatedDataDto<PreviewPostAggregatedDto>>(explorePosts);
        }

        /// <inheritdoc />
        public async Task<PaginatedDataDto<PreviewPostAggregatedDto>> GetFollowingUpdatesAsync(GetFollowingPostsQueryParametersDto queryParametersDto)
        {
            var (field, order) = queryParametersDto.Sort.ToSortOrder();
            var followingPosts = await _unitOfWork.PostRepository.GetFollowingUpdatesAsync(
                queryParametersDto.SearchText,
                queryParametersDto.RequestingUserId,
                _mapper.Map<DbPaginationQueryParameters>(queryParametersDto.PaginationQueryParameters),
                field,
                order);

            return _mapper.Map<PaginatedDataDto<PreviewPostAggregatedDto>>(followingPosts);
        }

        /// <inheritdoc />
        public async Task LikePostByUserAsync(string userId, int postId)
        {
            var postToLike = await _unitOfWork.PostRepository.GetByIdAsync(
                postId,
                post => post.Album,
                post => post.Likes,
                post => post.Album.Owner);

            if (postToLike == null)
            {               
                throw new NotFoundException(ErrorMessagesResources.PostNotFound);
            }

            if (postToLike.Album.IsPrivate && userId != postToLike.Album.OwnerId)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.PostIsPrivate);
            }

            if (postToLike.Album.Owner.IsPrivate && userId != postToLike.Album.OwnerId)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.AuthorAccountIsPrivate);
            }

            if (postToLike.Likes != null &&
                postToLike.Likes.Any(like => !like.IsDeleted && like.UserId == userId && like.PostId == postId))
            {
                throw new UnprocessableEntityException(ErrorMessagesResources.PostIsAlreadyLiked);
            }

            _unitOfWork.LikeRepository.Add(new Like
            {
                UserId = userId,
                PostId = postId
            });

            await _unitOfWork.CommitAsync();
        }

        /// <inheritdoc />
        public async Task UnlikePostByUserAsync(string userId, int postId)
        {
            var resultLike = _unitOfWork.LikeRepository
                .GetFiltered(
                    like => like.UserId == userId && like.PostId == postId,
                    like => like.Post.Album,
                    like => like.User)
                .FirstOrDefault();

            if (resultLike == null)
            {
                throw new NotFoundException(ErrorMessagesResources.PostNotFound);
            }

            if (resultLike.Post?.Album?.IsPrivate != null && resultLike.Post.Album.IsPrivate &&
                userId != resultLike.Post.Album.OwnerId)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.PhotoBelongToPprivateAlbum);
            }

            if (resultLike.User?.IsPrivate != null && resultLike.User.IsPrivate &&
                userId != resultLike.Post?.Album?.OwnerId)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.AuthorAccountIsPrivate);
            }

            _unitOfWork.LikeRepository.Delete(resultLike);

            await _unitOfWork.CommitAsync();
        }
    }
}