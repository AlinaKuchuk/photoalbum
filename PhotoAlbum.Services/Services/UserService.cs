﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using PhotoAlbum.DataAccess.Entities;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models.Enums;
using PhotoAlbum.DataAccess.Models.QueryParameters;
using PhotoAlbum.Services.Exceptions;
using PhotoAlbum.Services.Filters;
using PhotoAlbum.Services.Filters.Users;
using PhotoAlbum.Services.Interfaces;
using PhotoAlbum.Services.Models;
using PhotoAlbum.Services.Models.Enums;
using PhotoAlbum.Services.Models.User;
using PhotoAlbum.Services.Resources;

namespace PhotoAlbum.Services.Services
{
    /// <summary>
    ///     Represents user service.
    /// </summary>
    public sealed class UserService : IUserService
    {
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IFileStorage _fileStorage;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        /// <param name="unitOfWork">Unit of work.</param>
        /// <param name="mapper">Mapper.</param>
        /// <param name="authenticationManager">Authentication manager.</param>
        /// <param name="fileStorage">File storage.</param>
        public UserService(IUnitOfWork unitOfWork, IMapper mapper, IAuthenticationManager authenticationManager,
            IFileStorage fileStorage)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _authenticationManager = authenticationManager;
            _fileStorage = fileStorage;
        }

        /// <inheritdoc />
        public async Task CreateUserAsync(CreateUserDto createUser)
        {
            var checkingUserExist = await _unitOfWork.UserRepository.FindByNameAsync(createUser.Email);

            if (checkingUserExist != null)
            {
                throw new EntityAlreadyExistsException(ErrorMessagesResources.UserAlreadyExists);
            }

            var userToCreate = new User
            {
                Email = createUser.Email,
                UserName = createUser.Email,
                FirstName = createUser.FirstName,
                LastName = createUser.LastName
            };

            var creationResult = await _unitOfWork.UserRepository.CreateAsync(userToCreate, createUser.Password);

            if (creationResult.Errors.Any())
            {
                throw new CreatingUserException(creationResult.Errors?.FirstOrDefault());
            }

            await _unitOfWork.UserRepository.AddToRoleAsync(userToCreate.Id, Roles.User.ToString());
            await _unitOfWork.CommitAsync();
        }

        /// <inheritdoc />
        public async Task LoginUserAsync(UserCredentialsDto credentials)
        {
            var checkUserCredentials =
                await _unitOfWork.UserRepository.FindAsync(credentials.Email, credentials.Password);

            if (checkUserCredentials == null)
            {
                throw new InvalidCredentialsException(ErrorMessagesResources.InvalidCredentials);
            }

            var userClaim = await _unitOfWork.UserRepository.CreateIdentityAsync(
                checkUserCredentials,
                DefaultAuthenticationTypes.ApplicationCookie);

            _authenticationManager.SignOut();
            _authenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = true
            }, userClaim);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<UserDto>> GetUsersAsync(GetUsersParametersDto parameters)
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _unitOfWork.UserRepository.GetFilteredAsync(
                new FilterPipelineBuilder<User>()
                    .WithPipe(new UserNameFilterPipe(parameters.UserName))
                    .Build()));
        }

        /// <inheritdoc />
        public async Task<UserDto> GetUserAsync(string id)
        {
            var roles = await _unitOfWork.RoleRepository.GetRolesAsync();
            var user = await _unitOfWork.UserRepository.FindByIdAsync(id);

            return MapRoles(_mapper.Map<UserDto>(user), roles, user);
        }

        /// <inheritdoc />
        public async Task UpdateUserAsync(UserDto user)
        {
            var userToUpdate = await _unitOfWork.UserRepository.FindByIdAsync(user.Id);

            if (userToUpdate == null)
            {
                throw new UnprocessableEntityException(ErrorMessagesResources.UserIsNotExist);
            }

            var bio = userToUpdate.Biography;
            _mapper.Map(user, userToUpdate);
            userToUpdate.Biography = bio;
            userToUpdate.Roles.Clear();
            await _unitOfWork.UserRepository.UpdateAsync(userToUpdate);
            foreach (var userRole in user.Roles)
            {
                await _unitOfWork.UserRepository.AddToRoleAsync(user.Id, userRole);
            }

            await _unitOfWork.CommitAsync();
        }

        /// <inheritdoc />
        public async Task<FullUserDto> GetFullUserAsync(GetFullUserQueryParametersDto queryParameters)
        {
            var fullAggregatedUser =
                await _unitOfWork.UserRepository.GetFullUserAsync(
                    _mapper.Map<DbGetFullUserQueryParameters>(queryParameters));

            if (!queryParameters.RequestingUserRoles.Contains(nameof(RolesDto.Administrator))
                && queryParameters.UserId != queryParameters.RequestingUserId
                && fullAggregatedUser.User.IsPrivate)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.UserIsPrivate);
            }

            return _mapper.Map<FullUserDto>(fullAggregatedUser);
        }

        /// <inheritdoc />
        public async Task StartFollowAsync(string userId, string followingUserId)
        {
            var userFollows = await _unitOfWork.UserRepository
                .FindByIdWithIncludesAsync(
                    userId,
                    user => user.Following,
                    user => user.Followers);

            var userFollowTo = await _unitOfWork.UserRepository
                .FindByIdWithIncludesAsync(
                    followingUserId,
                    user => user.Following,
                    user => user.Followers);

            if (userFollows.Following == null)
            {
                userFollows.Following = new List<User>();
            }

            userFollows.Following.Add(userFollowTo);
            await _unitOfWork.UserRepository.UpdateAsync(userFollows);
            await _unitOfWork.CommitAsync();
        }

        /// <inheritdoc />
        public async Task StopFollowAsync(string userId, string followingUserId)
        {
            var userFollows = await _unitOfWork.UserRepository.FindByIdWithIncludesAsync(
                userId,
                user => user.Following,
                user => user.Followers);

            if (userFollows.Following == null)
            {
                return;
            }

            var userFollowTo = await _unitOfWork.UserRepository
                .FindByIdWithIncludesAsync(
                    followingUserId,
                    user => user.Following,
                    user => user.Followers);

            if (userFollows.Following.Remove(userFollowTo))
            {
                await _unitOfWork.UserRepository.UpdateAsync(userFollows);
                await _unitOfWork.CommitAsync();
            }
        }

        /// <inheritdoc />
        public async Task EditProfileAsync(EditUserProfileDto profile)
        {
            var user = await _unitOfWork.UserRepository.FindByIdAsync(profile.Id);
            user.FirstName = profile.FirstName;
            user.LastName = profile.LastName;
            user.Biography = profile.Biography;

            if (profile.PhotoStream != null)
            {
                var photoId = await _fileStorage.SaveFileAsync(profile.PhotoStream, "jpeg");
                user.ProfilePhotoId = photoId;
            }

            await _unitOfWork.UserRepository.UpdateAsync(user);
            await _unitOfWork.CommitAsync();
        }

        /// <inheritdoc />
        public void Logout()
        {
            _authenticationManager.SignOut();
        }

        /// <inheritdoc />
        public async Task MakeUserBusinessAsync(string userId)
        {
            await _unitOfWork.UserRepository.AddToRoleAsync(userId, nameof(RolesDto.Business));
            await _unitOfWork.CommitAsync();

            _authenticationManager.SignOut();
        }

        /// <inheritdoc />
        public async Task MakeUserPrivateAsync(string userId)
        {
            var userToUpdate = await _unitOfWork.UserRepository.FindByIdAsync(userId);

            if (userToUpdate == null)
            {
                throw new NotFoundException(ErrorMessagesResources.UserIsNotExist);
            }

            userToUpdate.IsPrivate = true;

            await _unitOfWork.UserRepository.UpdateAsync(userToUpdate);
            await _unitOfWork.CommitAsync();
        }

        /// <inheritdoc />
        public async Task MakeUserNotPrivateAsync(string userId)
        {
            var userToUpdate = await _unitOfWork.UserRepository.FindByIdAsync(userId);

            if (userToUpdate == null)
            {
                throw new NotFoundException(ErrorMessagesResources.UserIsNotExist);
            }

            userToUpdate.IsPrivate = false;

            await _unitOfWork.UserRepository.UpdateAsync(userToUpdate);
            await _unitOfWork.CommitAsync();
        }

        /// <inheritdoc />
        public async Task<PaginatedDataDto<UserDto>> GetFollowersAsync(GetFollowersOrFollowingQueryParametersDto queryParametersDto)
        {
            var followers =
                await _unitOfWork.UserRepository.GetFollowersAsync(
                    _mapper.Map<DbGetFollowersOrFollowingQueryParameters>(queryParametersDto));

            if (!queryParametersDto.RequestingUserRoles.Contains(nameof(RolesDto.Administrator))
                && queryParametersDto.UserId != queryParametersDto.RequestingUserId
                && followers.User.IsPrivate)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.UserIsPrivate);
            }

            return _mapper.Map<PaginatedDataDto<UserDto>>(followers.Users);
        }

        /// <inheritdoc />
        public async Task<PaginatedDataDto<UserDto>> GetFollowingAsync(GetFollowersOrFollowingQueryParametersDto queryParametersDto)
        {
            var following =
                await _unitOfWork.UserRepository.GetFollowingAsync(
                    _mapper.Map<DbGetFollowersOrFollowingQueryParameters>(queryParametersDto));

            if (!queryParametersDto.RequestingUserRoles.Contains(nameof(RolesDto.Administrator))
                && queryParametersDto.UserId != queryParametersDto.RequestingUserId
                && following.User.IsPrivate)
            {
                throw new PrivateResourceAccessingException(ErrorMessagesResources.UserIsPrivate);
            }

            return _mapper.Map<PaginatedDataDto<UserDto>>(following.Users);
        }

        private static UserDto MapRoles(UserDto userDto, IEnumerable<IdentityRole> roles, User user)
        {
            userDto.Roles = roles.Where(role => user.Roles.Any(innerRole => innerRole.RoleId == role.Id))
                .Select(role => role.Name).ToList();

            return userDto;
        }
    }
}