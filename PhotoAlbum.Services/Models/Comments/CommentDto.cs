﻿using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.Services.Models.Comments
{
    /// <summary>
    /// Represents comment data-transfer object.
    /// </summary>
    public class CommentDto
    {
        /// <summary>
        ///     Gets or sets content.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        ///     Gets or sets author.
        /// </summary>
        public UserDto Author { get; set; }
    }
}