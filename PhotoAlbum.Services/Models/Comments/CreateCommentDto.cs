﻿namespace PhotoAlbum.Services.Models.Comments
{
    /// <summary>
    ///     Represents create comment data-transfer object.
    /// </summary>
    public sealed class CreateCommentDto
    {
        /// <summary>
        ///     Gets or sets content.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        public int PostId { get; set; }
    }
}