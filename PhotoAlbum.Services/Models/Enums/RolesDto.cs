﻿namespace PhotoAlbum.Services.Models.Enums
{
    /// <summary>
    ///     User roles.
    /// </summary>
    public enum RolesDto
    {
        /// <summary>
        /// Common user role.
        /// </summary>
        User,

        /// <summary>
        /// Business account role.
        /// </summary>
        Business,

        /// <summary>
        /// Administrator role.
        /// </summary>
        Administrator
    }
}