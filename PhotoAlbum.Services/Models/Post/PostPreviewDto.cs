﻿using System;
using System.Collections.Generic;

namespace PhotoAlbum.Services.Models.Post
{
    /// <summary>
    ///     Represents post data-transfer object.
    /// </summary>
    public sealed class PostPreviewDto
    {
        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public IEnumerable<PhotoMetadataPreviewDto> Photos { get; set; }

        /// <summary>
        ///     Gets or sets created date-time.
        /// </summary>
        public DateTime CreateDateTime { get; set; }
    }
}