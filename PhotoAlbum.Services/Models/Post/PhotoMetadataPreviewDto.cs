﻿using System;

namespace PhotoAlbum.Services.Models.Post
{
    /// <summary>
    /// Represents photo metadata preview data-transfer object.
    /// </summary>
    public sealed class PhotoMetadataPreviewDto
    {
        /// <summary>
        /// Gets or sets file identifier.
        /// </summary>
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets post identifier.
        /// </summary>
        public int PostId { get; set; }

        /// <summary>
        /// Gets or sets created date-time.
        /// </summary>
        public DateTime CreatedDateTime { get; set; }
    }
}