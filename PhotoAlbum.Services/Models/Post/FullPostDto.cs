﻿using System;
using System.Collections.Generic;
using PhotoAlbum.Services.Models.Album;
using PhotoAlbum.Services.Models.Comments;
using PhotoAlbum.Services.Models.User;


namespace PhotoAlbum.Services.Models.Post
{
    /// <summary>
    ///     Represents post data-transfer object.
    /// </summary>
    public class FullPostDto
    {
        /// <summary>
        ///     Gets or sets identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets likes.
        /// </summary>
        public int LikeCount { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public ICollection<PhotoMetadataPreviewDto> Photos { get; set; }

        /// <summary>
        ///     Gets or sets author.
        /// </summary>
        public UserDto Author { get; set; }

        /// <summary>
        ///     Gets or sets comments.
        /// </summary>
        public PaginatedDataDto<CommentDto> Comments { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        public AlbumDto Album { get; set; }

        /// <summary>
        ///     Gets or sets post liked or not.
        /// </summary>
        public bool IsLiked { get; set; }

        /// <summary>
        ///     Gets or sets created date-time.
        /// </summary>
        public DateTime CreateDateTime { get; set; }
    }
}