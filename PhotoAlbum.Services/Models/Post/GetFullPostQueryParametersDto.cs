﻿namespace PhotoAlbum.Services.Models.Post
{
    /// <summary>
    ///     Represents post query parameter data-transfer object.
    /// </summary>
    public sealed class GetFullPostQueryParametersDto : BaseQueryParametersDto
    {
        /// <summary>
        ///     Gets or sets post identifier.
        /// </summary>
        public int PostId { get; set; }

        /// <summary>
        ///     Gets or sets requesting user roles.
        /// </summary>
        public string[] RequestingUserRoles { get; set; }

        /// <summary>
        ///     Gets or sets pagination parameters.
        /// </summary>
        public GetPaginationQueryParametersDto PaginationParameters { get; set; }
    }
}