﻿namespace PhotoAlbum.Services.Models.Post
{
    /// <summary>
    ///     Gets or sets post statistics.
    /// </summary>
    public sealed class PostsStatisticsDto
    {
        /// <summary>
        ///     Gets or sets views count.
        /// </summary>
        public int ViewsCount { get; set; }

        /// <summary>
        ///     Gets or sets likes count.
        /// </summary>
        public int LikesCount { get; set; }
    }
}