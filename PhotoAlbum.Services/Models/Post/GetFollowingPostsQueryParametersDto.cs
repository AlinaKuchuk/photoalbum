﻿namespace PhotoAlbum.Services.Models.Post
{
    /// <summary>
    ///     Represents following post query parameters data-transfer object.
    /// </summary>
    public sealed class GetFollowingPostsQueryParametersDto : BaseQueryParametersDto
    {
        /// <summary>
        ///     Gets or sets sort.
        /// </summary>
        public string Sort { get; set; }

        /// <summary>
        ///     Gets or sets test to search.
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        ///     Gets or sets pagination query parameters.
        /// </summary>
        public GetPaginationQueryParametersDto PaginationQueryParameters { get; set; }
    }
}