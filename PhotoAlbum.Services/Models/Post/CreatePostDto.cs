﻿using System.Collections.Generic;
using System.IO;

namespace PhotoAlbum.Services.Models.Post
{
    /// <summary>
    ///     Represents new post data-transfer object.
    /// </summary>
    public sealed class CreatePostDto
    {
        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        ///     Gets or sets album identifier.
        /// </summary>
        public int AlbumId { get; set; }

        /// <summary>
        ///     Gets or sets loaded images.
        /// </summary>
        public ICollection<LoadedImageDto> LoadedImages { get; set; }
    }
}