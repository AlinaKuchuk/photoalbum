﻿using System.Collections.Generic;
using PhotoAlbum.Services.Models.Album;
using PhotoAlbum.Services.Models.Comments;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.Services.Models.Post
{
    /// <summary>
    ///     Represents preview post aggregated data-transfer object.
    /// </summary>
    public sealed class PreviewPostAggregatedDto
    {
        /// <summary>
        ///     Gets or sets author.
        /// </summary>
        public UserDto Author { get; set; }

        /// <summary>
        ///     Gets or sets post.
        /// </summary>
        public PostPreviewDto Post { get; set; }

        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        public AlbumDto Album { get; set; }

        /// <summary>
        ///     Gets or set number of likes.
        /// </summary>
        public int LikesCount { get; set; }

        /// <summary>
        ///     Gets or set comments.
        /// </summary>
        public IEnumerable<CommentDto> Comments { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is post liked by requesting user or not.
        /// </summary>
        public bool IsLiked { get; set; }
    }
}