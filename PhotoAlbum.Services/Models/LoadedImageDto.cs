﻿using System.IO;

namespace PhotoAlbum.Services.Models
{
    /// <summary>
    ///     Represents loaded image data-transfer object.
    /// </summary>
    public sealed class LoadedImageDto
    {
        /// <summary>
        ///     Gets or sets image stream.
        /// </summary>
        public Stream ImageStream { get; set; }

        /// <summary>
        ///     Gets or sets extension.
        /// </summary>
        public string Extension { get; set; }
    }
}