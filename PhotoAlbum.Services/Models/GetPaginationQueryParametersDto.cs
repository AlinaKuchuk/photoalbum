﻿namespace PhotoAlbum.Services.Models
{
    /// <summary>
    ///     Represents pagination query parameters.
    /// </summary>
    public sealed class GetPaginationQueryParametersDto
    {
        /// <summary>
        ///     Gets or sets page size.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        ///     Gets or sets page number.
        /// </summary>
        public int PageNumber { get; set; }
    }
}