﻿using System;
using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.Services.Models.Album
{
    /// <summary>
    ///     Represents album data-transfer object.
    /// </summary>
    public sealed class AlbumDto
    {
        /// <summary>
        ///     Gets or sets identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets author.
        /// </summary>
        public UserDto Owner { get; set; }

        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Gets or sets preview photo identifier.
        /// </summary>
        public Guid? PreviewPhotoId { get; set; }

        /// <summary>
        ///     Get or sets a value indicating is album private or not.
        /// </summary>
        public bool IsPrivate { get; set; }
    }
}