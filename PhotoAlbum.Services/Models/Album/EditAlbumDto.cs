﻿namespace PhotoAlbum.Services.Models.Album
{
    /// <summary>
    ///     Represents Edit album data-transfer object.
    /// </summary>
    public sealed class EditAlbumDto
    {
        /// <summary>
        ///     Gets or sets album identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is album private or not.
        /// </summary>
        public bool IsPrivate { get; set; }
    }
}
