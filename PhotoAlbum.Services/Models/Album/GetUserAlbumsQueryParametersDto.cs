﻿namespace PhotoAlbum.Services.Models.Album
{
    /// <summary>
    ///     Represents user album query parameters data-transfer object.
    /// </summary>
    public class GetUserAlbumsQueryParametersDto : BaseQueryParametersDto
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets requesting user roles.
        /// </summary>
        public string[] RequestingUserRoles  { get; set; }

        /// <summary>
        ///     Get or sets photos pagination query parameters.
        /// </summary>
        public GetPaginationQueryParametersDto AlbumsPaginationQueryParameters { get; set; }
    }
}
