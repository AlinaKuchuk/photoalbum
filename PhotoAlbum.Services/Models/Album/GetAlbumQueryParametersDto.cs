﻿namespace PhotoAlbum.Services.Models.Album
{
    /// <summary>
    ///     Represents album photos query parameters.
    /// </summary>
    public sealed class GetAlbumQueryParametersDto : BaseQueryParametersDto
    {
        /// <summary>
        ///     Gets or sets album identifier.
        /// </summary>
        public int AlbumId { get; set; }

        /// <summary>
        ///     Gets or sets requesting user roles.
        /// </summary>
        public string[] RequestingUserRoles { get; set; }

        /// <summary>
        ///     Gets or sets photos pagination query parameters.
        /// </summary>
        public GetPaginationQueryParametersDto PhotosPaginationQueryParameters { get; set; }
    }
}