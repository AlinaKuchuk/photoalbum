﻿using PhotoAlbum.Services.Models.User;

namespace PhotoAlbum.Services.Models.Album
{
    /// <summary>
    ///     Represents all user albums data-transfer object.
    /// </summary>
    public sealed class UserAlbumsDto
    {
        /// <summary>
        ///     Gets or sets owner.
        /// </summary>
        public UserDto Owner { get; set; }

        /// <summary>
        ///     Gets or sets albums.
        /// </summary>
        public PaginatedDataDto<AlbumDto> Albums { get; set; }
    }
}
