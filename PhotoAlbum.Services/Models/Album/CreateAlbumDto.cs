﻿using System;

namespace PhotoAlbum.Services.Models.Album
{
    /// <summary>
    ///     Represents creating album data-transfer object.
    /// </summary>
    public sealed class CreateAlbumDto
    {
        /// <summary>
        ///     Gets or sets description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Get or sets a value indicating is album private or not.
        /// </summary>
        public bool IsPrivate { get; set; }
    }
}