﻿using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Services.Models.Album
{
    /// <summary>
    ///     Represents album photos data-transfer object.
    /// </summary>
    public sealed class FullAlbumDto
    {
        /// <summary>
        ///     Gets or sets album.
        /// </summary>
        public AlbumDto Album { get; set; }

        /// <summary>
        ///     Gets or sets number of posts in the album.
        /// </summary>
        public int PostCount { get; set; }

        /// <summary>
        ///     Gets or sets number of photos in the album.
        /// </summary>
        public int PhotosCount { get; set; }

        /// <summary>
        ///     Gets or sets photo pagination data-transfer object.
        /// </summary>
        public PaginatedDataDto<PhotoMetadataPreviewDto> Photos { get; set; }
    }
}
