﻿namespace PhotoAlbum.Services.Models.User
{
    /// <summary>
    /// Represents role data-transfer object.
    /// </summary>
    public sealed class RoleDto
    {
        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string Name { get; set; }
    }
}