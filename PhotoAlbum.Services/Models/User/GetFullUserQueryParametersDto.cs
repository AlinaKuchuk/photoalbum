﻿namespace PhotoAlbum.Services.Models.User
{
    /// <summary>
    ///     Represents get full user query parameters.
    /// </summary>
    public sealed class GetFullUserQueryParametersDto : BaseQueryParametersDto
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets requesting  user roles.
        /// </summary>
        public string[] RequestingUserRoles { get; set; }

        /// <summary>
        ///     Get or sets photos pagination query parameters.
        /// </summary>
        public GetPaginationQueryParametersDto PhotosPaginationQueryParameters { get; set; }

        /// <summary>
        ///     Gets or sets maximum album counts.
        /// </summary>
        public int MaximumAlbumCounts { get; set; } = 4;
    }
}