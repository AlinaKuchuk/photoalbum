﻿using System.IO;

namespace PhotoAlbum.Services.Models.User
{
    /// <summary>
    ///     Represents edit user profile.
    /// </summary>
    public sealed class EditUserProfileDto
    {
        /// <summary>
        ///     Gets or sets user id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets user first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets user last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets user biography.
        /// </summary>
        public string Biography { get; set; }

        /// <summary>
        ///     Gets or sets photo stream.
        /// </summary>
        public Stream PhotoStream { get; set; }
    }
}