﻿namespace PhotoAlbum.Services.Models.User
{
    /// <summary>
    ///     Represents get followers or following query parameters.
    /// </summary>
    public sealed class GetFollowersOrFollowingQueryParametersDto : BaseQueryParametersDto
    {
        /// <summary>
        ///     Gets or sets user identifier.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Gets or sets requesting user roles.
        /// </summary>
        public string[] RequestingUserRoles { get; set; }

        /// <summary>
        ///     Gets or sets pagination query parameters.
        /// </summary>
        public GetPaginationQueryParametersDto PaginationQueryParameters { get; set; }
    }
}