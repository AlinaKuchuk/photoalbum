﻿using System;
using System.Collections.Generic;

namespace PhotoAlbum.Services.Models.User
{
    /// <summary>
    ///     Represents user data transfer object.
    /// </summary>
    public sealed class UserDto
    {
        /// <summary>
        ///     Gets or sets identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets profile photo identifier.
        /// </summary>
        public Guid? ProfilePhotoId { get; set; }

        /// <summary>
        ///     Gets or sets biography.
        /// </summary>
        public string Biography { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is user account private or not.
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        ///     Gets or sets roles.
        /// </summary>
        public IEnumerable<string> Roles { get; set; }
    }
}