﻿namespace PhotoAlbum.Services.Models.User
{
    /// <summary>
    ///     Represents login data-transfer object.
    /// </summary>
    public sealed class UserCredentialsDto
    {
        /// <summary>
        ///     Gets or sets email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets password.
        /// </summary>
        public string Password { get; set; }
    }
}