﻿using System;
using System.Collections.Generic;
using PhotoAlbum.Services.Models.Album;
using PhotoAlbum.Services.Models.Post;

namespace PhotoAlbum.Services.Models.User
{
    /// <summary>
    ///     Represents full user data-transfer object.
    /// </summary>
    public sealed class FullUserDto
    {
        /// <summary>
        ///     Gets or sets identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets followers count.
        /// </summary>
        public int FollowersCount { get; set; }

        /// <summary>
        ///     Gets or sets following count.
        /// </summary>
        public int FollowingCount { get; set; }

        /// <summary>
        ///     Gets or sets post count.
        /// </summary>
        public int PostsCount { get; set; }

        /// <summary>
        ///     Gets or sets biography.
        /// </summary>
        public string Biography { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating is user account private or not.
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        ///     Gets or sets profile photo identifier.
        /// </summary>
        public Guid? ProfilePhotoId { get; set; }

        /// <summary>
        ///     Gets or sets albums.
        /// </summary>
        public IEnumerable<AlbumDto> Albums { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating requesting user follows requested user.
        /// </summary>
        public bool IsFollowed { get; set; }

        /// <summary>
        ///     Gets or sets photos.
        /// </summary>
        public PaginatedDataDto<PhotoMetadataPreviewDto> Photos { get; set; }
    }
}