﻿namespace PhotoAlbum.Services.Models.User
{
    /// <summary>
    ///     Represents getting users parameters.
    /// </summary>
    public sealed class GetUsersParametersDto
    {
        /// <summary>
        ///     Gets or sets user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        ///     Gets or sets role.
        /// </summary>
        public string Role { get; set; }
    }
}