﻿using System.Collections.Generic;

namespace PhotoAlbum.Services.Models
{
    public sealed class PaginatedDataDto<TEntity>
    {
        public IEnumerable<TEntity> Items { get; set; }

        public int TotalPages { get; set; }

        public int CurrentPage { get; set; }
    }
}