﻿using System;

namespace PhotoAlbum.Services.Models
{
    /// <summary>
    ///     Represents base query parameters.
    /// </summary>
    public abstract class BaseQueryParametersDto
    {
        /// <summary>
        ///     Gets or sets requesting user identifier.
        /// </summary>
        public string RequestingUserId { get; set; }
    }
}