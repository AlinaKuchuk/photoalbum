﻿namespace PhotoAlbum.Services.Models
{
    /// <summary>
    ///     Represents create exception data data-transfer object.
    /// </summary>
    public sealed class CreateExceptionDataDto
    {
        /// <summary>
        ///     Gets or sets user principal identifier.
        /// </summary>
        public string UserPrincipalId { get; set; }

        /// <summary>
        ///     Gets or sets url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        ///     Gets or sets stack trace.
        /// </summary>
        public string StackTrace { get; set; }
    }
}